// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const eq = function (a, b, epsilon) {
  if ((epsilon == null)) {
    epsilon = 1e-8
  }

  const absA = Math.abs(a)
  const absB = Math.abs(b)
  const diff = Math.abs(a - b)

  if (a === b) {
    return true
  } else if ((a === 0) || (b === 0) || (diff < Number.MIN_VALUE)) {
    // a or b is zero or both are extremely close to it
    // relative error is less meaningful here
    return diff < epsilon
  } else {
    // use relative error
    return (diff / (absA + absB)) < epsilon
  }
}

const leq = (a, b, epsilon) => (a < b) || eq(a, b, epsilon)

const geq = (a, b, epsilon) => (a > b) || eq(a, b, epsilon)

module.exports = [eq, geq, leq]

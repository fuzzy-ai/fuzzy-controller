const add = function (...addends) {
  return addends.reduce((sum, addend) => sum + addend, 0)
}

const sub = function (minuend, ...subtrahends) {
  return subtrahends.reduce((difference, subtrahend) =>
    difference - subtrahend, minuend)
}

const mul = function (multiplicand, ...multipliers) {
  return multipliers.reduce((product, multiplier) =>
    product * multiplier, multiplicand)
}

const div = function (dividend, ...divisors) {
  return divisors.reduce((quotient, divisor) => quotient / divisor, dividend)
}

module.exports = {add, sub, mul, div}

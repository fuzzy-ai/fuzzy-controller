// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const debug = require('debug')('fuzzy-controller:polygon')

const segment = require('./segment')
const [eq] = Array.from(require('./equalish'))

class CircularError extends Error {
  constructor (cur, already, seen) {
    super(`Circular: [${cur.x}, ${cur.y}] is equal to [${already.x}, ${already.y}] in ${_.map(seen, item => `[${item.x}, ${item.y}]`).join(', ')}`)
    this.cur = cur
    this.already = already
    this.seen = seen
    this.name = 'CircularError'
  }
}

let isPointlike = pt => _.isArray(pt) && (pt.length === 2) && _.isFinite(pt[0]) && _.isFinite(pt[1])

const isSegmentlike = seg => _.isArray(seg) && (seg.length === 2) && isPointlike(seg[0]) && isPointlike(seg[1])

const addTo = function (arr, pt) {
  if (!_.find(arr, other => other.equals(pt))) {
    return arr.push(pt)
  }
}

const pins = (pt, seg) => _.some(seg, ptlk => pt.equals(ptlk))

class Point {
  constructor (x, y) {
    this.x = x
    this.y = y
    this.snext = null
    this.cnext = null
    this.inside = null
    this.intersection = false
  }
  equals (other) {
    if (other instanceof Point) {
      return eq(this.x, other.x) && eq(this.y, other.y)
    } else if (isPointlike(other)) {
      return eq(this.x, other[0]) && eq(this.y, other[1])
    } else {
      throw new Error(`Can't compare ${JSON.stringify(this)} with ${JSON.stringify(other)}`)
    }
  }
}

const isInside = function (pt, polygon) {
  assert(_.isObject(pt), `${pt} is not an object`)
  assert(pt instanceof Point, `${pt} is not a Point`)
  assert(_.isArray(polygon), `${polygon} is not an object`)
  _.each(polygon, (other) => {
    assert(_.isObject(other), `${other} is not an object`)
    assert(other instanceof Point, `${other} is not a Point`)
  })

  let rm

  _.each(polygon, (pt) => {
    if ((rm == null) || (pt.x > rm)) {
      rm = pt.x
    }
  })

  const ray = [[pt.x, pt.y], [rm + 1, pt.y]]

  let ints = 0

  _.each(polygon, (vt, i) => {
    let next
    if (i === (polygon.length - 1)) {
      [next] = polygon
    } else {
      next = polygon[i + 1]
    }
    const seg = [[vt.x, vt.y], [next.x, next.y]]
    const int = segment.intersect(ray, seg)
    // What if the intersection is a line segment?!
    if (isPointlike(int)) {
      ints++
    } else if (isSegmentlike(int)) {
      // Kind of a noop but why not
      ints += 2
    }
  })

  return (ints % 2) !== 0
}

exports.union = function (subject, clip) {
  let un
  debug('Union')
  debug(subject)
  debug(clip)

  isPointlike = pt => _.isArray(pt) && (pt.length === 2) && _.isFinite(pt[0]) && _.isFinite(pt[1])

  const isArrayOfPointlike = val => _.isArray(val) && _.every(val, isPointlike)

  const noDuplicates = function (pts) {
    let dupes = 0
    _.each(pts, (pt, i) => {
      const next = (i < (pts.length - 1)) ? pts[i + 1] : pts[0]
      if (eq(pt[0], next[0]) && eq(pt[1], next[1])) {
        return dupes++
      }
    })
    return dupes === 0
  }

  // Arguments are arrays of 2-element arrays

  assert(isArrayOfPointlike(subject), `${subject} doesn't look like an array of points`)
  assert(isArrayOfPointlike(clip), `${clip} doesn't look like an array of points`)

  assert(noDuplicates(subject), `${subject} has duplicate points`)
  assert(noDuplicates(clip), `${clip} has duplicate points`)

  const toPoint = function (arr, i) {
    assert(_.isArray(arr), `${arr} is not an array.`)
    assert.equal(arr.length, 2, `${arr} is not a 2-element array.`)
    return new Point(arr[0], arr[1])
  }

  const setNext = prop =>
    function (pt, i, lst) {
      assert(_.isObject(pt), `${pt} is not an object`)
      assert(pt instanceof Point, `${pt} is not a Point`)
      assert(_.isArray(lst), `${lst} is not an array`)
      assert(_.isFinite(i), `${i} is not a number`)
      if (i === (lst.length - 1)) {
        [pt[prop]] = lst
      } else {
        pt[prop] = lst[i + 1]
      }
      assert((pt[prop] != null), `${pt} doesn't have a '${prop}' member (${i}, ${lst})`)
      return pt[prop]
    }

  const isIntersection = ipt => ipt.intersection

  const isEntering = ipt => isIntersection(ipt) && (ipt.snext != null ? ipt.snext.inside : undefined)

  const isLeaving = ipt => isIntersection(ipt) && !(ipt.snext != null ? ipt.snext.inside : undefined)

  const walk = function (start, prop, iter) {
    let cur = start
    let seen = []
    while (true) {
      iter(cur)
      const prev = cur
      cur = prev[prop]
      if (cur.equals(start)) { break }
      seen.push(prev)
      const already = _.find(seen, item => cur.equals(item))
      if (already != null) {
        if (already === start) {
          break
        } else {
          throw new CircularError(cur, already, seen)
        }
      }
    }
    seen = null
    return cur
  }

  const reduceSegments = function (shape) {
    assert(isArrayOfPointlike(shape), `${shape} is not an array of point-like arrays.`)
    assert(shape.length >= 3, `${shape} is not of length 3 or more.`)
    let i = 0
    while (true) {
      while (true) {
        const cur = shape[i]
        const next = shape[(i + 1) % shape.length]
        const nextnext = shape[(i + 2) % shape.length]
        const m1 = segment.slope([cur, next])
        const m2 = segment.slope([next, nextnext])
        if (eq(m1, m2)) {
          shape = _.filter(shape, pt => pt !== next)
        } else {
          break
        }
        if (i >= shape.length) { break }
      }
      i++
      if (i >= shape.length) { break }
    }
    return shape
  }

  const rotateArray = function (arr, count) {
    const len = arr.length >>> 0
    count = count >> 0
    count = ((count % len) + len) % len
    Array.prototype.push.apply(arr, arr.splice(0, count))
    return arr
  }

  const startWithLeftmost = function (shape) {
    let lm = null
    let lmx = Infinity
    let lmi = null
    _.each(shape, (pt, i) => {
      if (pt[0] < lmx) {
        lm = pt
        // eslint-disable-next-line prefer-destructuring
        lmx = pt[0]
        lmi = i
      }
    })
    if (lm != null) {
      rotateArray(shape, lmi)
    }
    return shape
  }

  const ints = []

  const sp = _.map(subject, toPoint)

  // Keep point identity

  const cp = _.map(clip, (ptlk) => {
    const spt = _.find(sp, spt => spt.equals(ptlk))
    if (spt) {
      spt.intersection = true
      ints.push(spt)
      return spt
    } else {
      return toPoint(ptlk)
    }
  })

  _.each(sp, setNext('snext'))
  _.each(cp, setNext('cnext'))

  for (const pt of Array.from(sp)) {
    pt.inside = isInside(pt, cp)
  }

  for (const pt of Array.from(cp)) {
    pt.inside = isInside(pt, sp)
  }

  // Find intersections of all segments

  walk(cp[0], 'cnext', cpt =>
    walk(sp[0], 'snext', (spt) => {
      let ipt = null
      assert(cpt.cnext)
      assert(spt.snext)
      const cseg = [[cpt.x, cpt.y], [cpt.cnext.x, cpt.cnext.y]]
      const sseg = [[spt.x, spt.y], [spt.snext.x, spt.snext.y]]
      const int = segment.intersect(cseg, sseg)
      debug({cseg, sseg, int})
      if (int != null) {
        if (isPointlike(int)) {
          debug('int is pointlike')
          if (cpt.equals(int) || spt.equals(int)) {
            // skip if it's the current points; they
            // should be handled by previous segments
            ipt = null
          } else if (cpt.cnext.equals(spt.snext) && cpt.cnext.equals(int)) {
            ipt = cpt.cnext
            // No insertion needed
          } else if (cpt.cnext.equals(int)) {
            ipt = cpt.cnext
            ipt.snext = spt.snext
            spt.snext = ipt
          } else if (spt.snext.equals(int)) {
            ipt = spt.snext
            ipt.cnext = cpt.cnext
            cpt.cnext = ipt
          } else {
            ipt = new Point(int[0], int[1])
            ipt.cnext = cpt.cnext
            cpt.cnext = ipt
            ipt.snext = spt.snext
            spt.snext = ipt
          }
        } else if (isSegmentlike(int)) {
          debug('int is segmentlike')
          // NB: int is like [[x0, y0], [x1, y1]] where x0 <= x1
          if (cpt === spt) {
            cpt.intersection = true
            addTo(ints, cpt)
            if (cpt.cnext === spt.snext) {
              cpt.cnext.intersection = true
              addTo(ints, cpt.cnext)
            } else if (pins(cpt.cnext, int)) {
              addTo(ints, cpt.cnext)
              cpt.cnext.intersection = true
              cpt.cnext.snext = spt.snext
              spt.snext = cpt.cnext
            } else if (pins(spt.snext, int)) {
              addTo(ints, spt.snext)
              spt.snext.intersection = true
              spt.snext.cnext = cpt.cnext
              cpt.cnext = spt.snext
            }
          } else if (cpt.cnext === spt.snext) {
            cpt.cnext.intersection = true
            addTo(ints, cpt.cnext)
            if (pins(spt, int)) {
              addTo(ints, spt)
              spt.intersection = true
              spt.cnext = cpt.cnext
              cpt.cnext = spt
            } else if (pins(cpt, int)) {
              addTo(ints, cpt)
              cpt.intersection = true
              cpt.snext = spt.snext
              spt.snext = cpt
            }
          } else if (pins(cpt, int)) {
            cpt.intersection = true
            addTo(ints, cpt)
            if (pins(cpt.cnext, int)) {
              cpt.cnext.intersection = true
              addTo(ints, cpt.cnext)
              cpt.cnext.snext = spt.snext
              cpt.snext = cpt.cnext
              spt.snext = cpt
            } else if (pins(spt.snext, int)) {
              spt.snext.intersection = true
              addTo(ints, spt.snext)
              spt.snext.cnext = cpt.cnext
              cpt.cnext = spt.snext
              cpt.snext = spt.snext
              spt.snext = cpt
            }
          } else if (pins(spt, int)) {
            spt.intersection = true
            addTo(ints, spt)
            if (pins(cpt.cnext, int)) {
              cpt.cnext.intersection = true
              addTo(ints, cpt.cnext)
              cpt.cnext.snext = spt.snext
              spt.snext = cpt.cnext
              spt.cnext = cpt.cnext
              cpt.cnext = spt
            } else if (pins(spt.snext, int)) {
              spt.snext.intersection = true
              addTo(ints, spt.snext)
              spt.snext.cnext = cpt.cnext
              spt.cnext = spt.snext
              cpt.cnext = spt
            }
          }
        }
      }
      if (ipt != null) {
        ipt.intersection = true
        return addTo(ints, ipt)
      }
    })
  )

  // console.log "CLIPPING"
  //
  // walk cp[0], "cnext", (cpt) ->
  //   console.dir {x: cpt.x, y: cpt.y}
  //
  // console.log "SUBJECT"
  //
  // walk sp[0], "snext", (spt) ->
  //   console.dir {x: spt.x, y: spt.y}
  //
  // console.dir ints

  if (ints.length === 0) {
    if (_.every(sp, pt => pt.inside)) {
      un = [clip]
    } else if (_.every(cp, pt => pt.inside)) {
      un = [subject]
    } else {
      // XXX: Handle disconnected polygons
      un = [subject, clip]
    }
  } else if (ints.length === 1) {
    un = [subject, clip]
  } else {
    const leaving = _.filter(ints, isLeaving)
    un = []
    while (true) {
      let cur
      const start = (cur = leaving.shift())

      if ((cur == null)) { break }

      const shape = []
      un.push(shape)

      let prop = 'snext'
      let prev = null

      while (true) {
        shape.push([cur.x, cur.y])

        prev = cur
        cur = cur[prop]

        if (isEntering(cur)) {
          prop = 'cnext'
        } else if (isLeaving(cur)) {
          _.remove(leaving, pt => pt === cur)
          prop = 'snext'
        }

        assert.ok(cur, `Couldn't follow the link from ${prev}`)

        if (cur === start) { break }
      }
    }
  }

  un = _.map(un, (shape) => {
    assert(isArrayOfPointlike(shape), `${shape} is not an array of point-like arrays.`)
    shape = reduceSegments(shape)
    shape = startWithLeftmost(shape)
    return shape
  })

  return un
}

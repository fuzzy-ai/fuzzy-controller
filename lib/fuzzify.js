// fuzzify.coffee
//
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const _ = require('lodash')
const {sub, div} = require('./careful-math')

const [, geq] = Array.from(require('./equalish'))

const leftmostSlope = function (points, sets) {
  if (points.length !== 2) {
    return false
  }
  const [left] = points
  let lm = true
  _.each(sets, (points, setName) => {
    if ((points.length === 2) && (points[0] < left)) {
      lm = false
    }
  })
  return lm
}

const fuzzify = function (crisp, sets) {
  const fuzzified = {}
  _.each(sets, (points, setName) => {
    const lms = leftmostSlope(points, sets)
    fuzzified[setName] = fuzzyMembership(crisp, points, lms)
  })
  return fuzzified
}

const fuzzyMembership = function (crisp, points, lm) {
  let membership = null
  switch (points.length) {
    case 2: // slope
      if (lm) {
        if (crisp < points[0]) {
          membership = 1
        } else if (geq(crisp, points[0]) && (crisp < points[1])) {
          membership = sub(1, div(sub(crisp, points[0]), sub(points[1], points[0])))
        } else if (geq(crisp, points[1])) {
          membership = 0
        }
      } else {
        if (crisp < points[0]) {
          membership = 0
        } else if (geq(crisp, points[0]) && (crisp < points[1])) {
          membership = div(sub(crisp, points[0]), sub(points[1], points[0]))
        } else if (geq(crisp, points[1])) {
          membership = 1
        }
      }
      break
    case 3: // triangle
      if (crisp < points[0]) {
        membership = 0
      } else if (geq(crisp, points[0]) && (crisp < points[1])) {
        membership = div(sub(crisp, points[0]), sub(points[1], points[0]))
      } else if (geq(crisp, points[1]) && (crisp < points[2])) {
        membership = sub(1, div(sub(crisp, points[1]), sub(points[2], points[1])))
      } else if (geq(crisp, points[2])) {
        membership = 0
      }
      break
    case 4: // trapezoid
      if (crisp < points[0]) {
        membership = 0
      } else if (geq(crisp, points[0]) && (crisp < points[1])) {
        membership = div(sub(crisp, points[0]), sub(points[1], points[0]))
      } else if (geq(crisp, points[1]) && (crisp < points[2])) {
        membership = 1
      } else if (geq(crisp, points[2]) && (crisp < points[3])) {
        membership = sub(1, div(sub(crisp, points[2]), sub(points[3], points[2])))
      } else if (geq(crisp, points[3])) {
        membership = 0
      }
      break
  }
  return membership
}

module.exports = {fuzzify, fuzzyMembership}

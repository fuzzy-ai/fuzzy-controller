// Copyright 2014-2017 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const fs = require('fs')
const path = require('path')
const assert = require('assert')
const { EventEmitter } = require('events')
const debug = require('debug')('fuzzy-controller:fuzzy-controller')

const _ = require('lodash')

const FuzzyRuleParser = require('./fuzzy-rule-parser')
const FuzzyRuleParserError = require('./parser/fuzzy-rule-parser-error')
const FuzzyRule = require('./parser/fuzzy-rule')
const centroid = require('./centroid')
const polygon = require('./polygon')
const {fuzzify} = require('./fuzzify')
const {add, sub, mul, div} = require('./careful-math')

const [eq, geq, leq] = Array.from(require('./equalish'))

const GRANULARITY = 1e6

const js = JSON.stringify

class FuzzyController extends EventEmitter {
  constructor (inputs, outputs, rules, parsedRules) {
    super()

    let frules

    debug('Constructor started')

    // We also accept a single argument; an object with "inputs", "outputs",
    // "rules", and "parsed_rules"

    if (arguments.length === 1) {
      const params = inputs
      assert(_.isObject(params), 'Single argument must be an object.');
      ({inputs, outputs, rules} = params)
      parsedRules = params.parsed_rules
    }

    assert(_.isObject(inputs), 'No inputs defined.')
    assert(_.isObject(outputs), 'No outputs defined.')
    assert(_.isArray(rules) || _.isArray(parsedRules), 'No rules defined.')

    _.each(inputs, (input, name) => {
      assert(_.isObject(input), `Input ${name} is not an object`)
      return _.each(input, (def, setName) => {
        assert(_.isArray(def),
          `Definition of ${setName} for ${name} is not an array.`)
        assert(def.length >= 2 && def.length <= 4,
          `Definition of ${setName} for ${name} must be 2-4 numbers.`)
        _.each(def, (point, i) => {
          assert(_.isFinite(point),
            `Definition of ${setName} for ${name} must be 2-4 numbers.`)
          if (i > 0) {
            assert(geq(point, def[i - 1]),
              `Definition of ${setName} for ${name} is ${def}, ` +
              'must be 2-4 numbers in ascending order.'
            )
          }
        })
        assert((def[0] < def[def.length - 1]),
          `Definition of ${setName} for ${name} must not have zero width.`)
      })
    })

    _.each(outputs, (output, name) => {
      assert(_.isObject(output), `Output ${name} is not an object`)
      return _.each(output, (def, setName) => {
        assert(_.isArray(def),
          `Definition of ${setName} for ${name} is not an array.`)
        assert(def.length >= 2 && def.length <= 4,
          `Definition of ${setName} for ${name} must be 2-4 numbers.`)
        _.each(def, (point, i) => {
          assert(_.isFinite(point),
            `Definition of ${setName} for ${name} must be 2-4 numbers.`)
          if (i > 0) {
            assert(geq(point, def[i - 1]),
              `Definition of ${setName} for ${name} is ${def},` +
              ' must be 2-4 numbers in ascending order.'
            )
          }
        })
        assert((def[0] < def[def.length - 1]),
          `Definition of ${setName} for ${name} must not have zero width.`)
      })
    })

    if (rules != null) {
      _.each(rules, rule => assert(_.isString(rule), 'Each rule must be a string.'))
    }

    if (parsedRules != null) {
      _.each(parsedRules, parsedRule => assert(_.isObject(parsedRule), 'Each parsed rule must be an object.'))
    }

    if (parsedRules != null) {
      frules = _.map(parsedRules, (parsedRule) => {
        const frule = FuzzyRule.fromJSON(parsedRule)
        frule.validate(inputs, outputs)
        return frule
      })

      if ((rules == null)) {
        rules = _.map(frules, frule => frule.toString())
      }
    } else if (rules != null) {
      const parser = new FuzzyRuleParser()

      frules = _.map(rules, (rule) => {
        const frule = parser.parse(rule)
        frule.validate(inputs, outputs)
        return frule
      })
    }

    this.inputs = _.cloneDeep(inputs)
    this.outputs = _.cloneDeep(outputs)
    this.rules = _.cloneDeep(rules)
    this.frules = _.cloneDeep(frules)

    debug('Constructor finished')

    debug(js(this.inputs))
    debug(js(this.outputs))
    debug(js(this.rules))
    debug(js(this.frules))
  }

  leftmostSlope (points, sets) {
    if (points.length !== 2) {
      return false
    }
    const [left] = points
    let lm = true
    _.each(sets, (points, setName) => {
      if ((points.length === 2) && (points[0] < left)) {
        lm = false
      }
    })
    return lm
  }

  fuzzifyArgs (args) {
    assert(_.isObject(args), `${args} is not an object.`)

    _.each(args, (arg, name) => {
      assert(_.has(this.inputs, name), `Unrecognized argument ${name}.`)
      const msg = `${name} argument is not a number or null.`
      assert(_.isFinite(arg) || _.isNull(arg), msg)
    })

    const fuzzified = {}

    _.each(this.inputs, (sets, inputName) => {
      // If the input is missing, all set memberships are 0
      if (!_.has(args, inputName) || _.isNull(args[inputName])) {
        fuzzified[inputName] = {}
        _.each(sets, (points, setName) => { fuzzified[inputName][setName] = 0 })
      } else {
        fuzzified[inputName] = fuzzify(args[inputName], sets)
      }
    })

    // Validate our fuzzified values

    _.each(fuzzified, (dimension, inputName) => {
      assert(_.isString(inputName), `${inputName} is not a string.`)
      assert(_.has(this.inputs, inputName), `Unrecognized argument ${inputName}.`)
      assert(_.isObject(dimension), `${inputName} is not an object.`)
      return _.each(dimension, (value, setName) => {
        assert(_.isString(setName),
          `${setName} of ${inputName} is not a string.`)
        assert(_.has(this.inputs[inputName], setName),
          `Unrecognized fuzzy value ${setName} for ${inputName}.`)
        assert(_.isFinite(value),
          `${setName} of ${inputName} is not a number ` +
          `(argument = ${args[inputName]}).`
        )
        assert(value >= 0 && value <= 1,
          `${setName} of ${inputName} (${value}) is not a fuzzy set membership.`)
      })
    })

    return fuzzified
  }

  infer (fuzzified) {
    const results = {}

    // Initialize all to 0

    _.each(this.outputs, (sets, outputName) => {
      results[outputName] = {}
      _.each(sets, (points, setName) => { results[outputName][setName] = 0 })
    })

    _.each(this.frules, (frule, i) => {
      const result = frule.apply(fuzzified)
      return _.each(result, (membership, dimension) => {
        return _.each(membership, (value, set) => {
          if (value > 0) {
            this.emit('rule', i, this.rules[i])
          }
          assert(_.has(this.outputs, dimension),
            `Rule refers to unknown output name '${dimension}': ${this.rules[i]}`)
          assert(_.has(this.outputs[dimension], set),
            `Rule refers to unknown set name '${set}' ` +
            `for output '${dimension}': ${this.rules[i]}`
          )
          // ADD accumulation
          results[dimension][set] = add(results[dimension][set], value)
        })
      })
    })

    return results
  }

  defuzzify (results) {
    const crisp = {}

    _.each(this.outputs, (sets, outputName) => {
      crisp[outputName] = this.defuzzifyDim(results[outputName], sets, outputName)
    })

    return crisp
  }

  clipShape (shape, clipTo, sets) {
    let clipped
    debug(`clipShape(${JSON.stringify(shape)}, ` +
      `${JSON.stringify(clipTo)}, ` +
      `${JSON.stringify(sets)})`)

    if (eq(clipTo, 0)) {
      clipped = null
    } else if (geq(clipTo, 1)) {
      switch (shape.length) {
        case 2: // slope
          if (this.leftmostSlope(shape, sets)) { // Downward slope
            clipped = [
              [sub(shape[0], sub(shape[1] - shape[0])),
                0
              ],
              [shape[0], clipTo],
              [shape[1], 0]
            ]
          } else { // upward slope
            clipped = [
              [shape[0], 0],
              [shape[1], clipTo],
              [add(shape[1], sub(shape[1], shape[0])), 0]
            ]
          }
          break
        case 3: // triangle
          clipped = [[shape[0], 0], [shape[1], clipTo], [shape[2], 0]]
          break
        case 4: // trapezoid
          clipped = [
            [shape[0], 0],
            [shape[1], clipTo],
            [shape[2], clipTo],
            [shape[3], 0]
          ]
          break
      }
    } else {
      let lvx, rvx
      switch (shape.length) {
        case 2: // slope
          if (this.leftmostSlope(shape, sets)) { // Downward slope
            lvx = add(sub(shape[0], sub(shape[1], shape[0])),
              mul(sub(shape[1], shape[0]), clipTo))
            rvx = sub(shape[1], mul(sub(shape[1], shape[0]), clipTo))
            clipped = [
              [sub(shape[0], sub(shape[1], shape[0])), 0],
              [lvx, clipTo],
              [rvx, clipTo],
              [shape[1], 0]
            ]
          } else { // upward slope
            lvx = add(shape[0], mul(sub(shape[1], shape[0]), clipTo))
            rvx = sub(add(shape[1], sub(shape[1], shape[0])),
              mul(sub(shape[1], shape[0]), clipTo))
            clipped = [
              [shape[0], 0],
              [lvx, clipTo],
              [rvx, clipTo],
              [add(shape[1], sub(shape[1], shape[0])), 0]
            ]
          }
          break
        case 3: // triangle
          lvx = add(shape[0], mul(sub(shape[1], shape[0]), clipTo))
          rvx = sub(shape[2], mul(sub(shape[2], shape[1]), clipTo))
          clipped = [[shape[0], 0], [lvx, clipTo], [rvx, clipTo], [shape[2], 0]]
          break
        case 4: // trapezoid
          lvx = add(shape[0], mul(sub(shape[1], shape[0]), clipTo))
          rvx = sub(shape[3], mul(sub(shape[3], shape[2]), clipTo))
          clipped = [[shape[0], 0], [lvx, clipTo], [rvx, clipTo], [shape[3], 0]]
          break
      }
    }

    debug(`clipped = ${JSON.stringify(clipped)}`)

    return clipped
  }

  assertProperShape (shape) {
    assert(_.isArray(shape), `${shape} is not an array`)
    assert(shape.length >= 2, `${shape} is not 2 or more points`)
    assert(eq(shape[0][1], 0), `Leftmost point of ${shape} is not on the X axis`)
    assert(eq(shape[shape.length - 1][1], 0),
      `Rightmost point of ${shape} is not on the X axis`)

    let lastX = Number.NEGATIVE_INFINITY

    return (() => {
      const result = []
      for (const pt of Array.from(shape)) {
        assert(_.isArray(pt), `${pt} is not an array`)
        assert.equal(pt.length, 2, `${pt} is not of length 2`)
        assert(geq(pt[0], lastX),
          `${pt} is not right of last X value ${lastX} in ${JSON.stringify(shape)}`)
        // eslint-disable-next-line prefer-destructuring
        lastX = pt[0]
        result.push(lastX)
      }
      return result
    })()
  }

  combineShapes (shapes) {
    debug(`combineShapes(${JSON.stringify(shapes)})`)

    assert(_.isArray(shapes), `${shapes} is not an array`)
    _.each(shapes, shape => {
      return this.assertProperShape(shape)
    })

    const leftmostX = function (shape) {
      let min = Infinity
      _.each(shape, (pt) => {
        if (pt[0] < min) {
          // eslint-disable-next-line prefer-destructuring
          min = pt[0]
        }
      })
      return min
    }

    shapes = shapes.sort((a, b) => {
      const lxa = leftmostX(a)
      const lxb = leftmostX(b)
      if (lxa < lxb) {
        return -1
      } else if (lxa > lxb) {
        return 1
      } else {
        return 0
      }
    })

    let [u] = shapes
    const disconnected = []

    _.each(shapes.slice(1), (shape) => {
      let un
      try {
        un = polygon.union(u, shape)
      } catch (err) {
        throw new Error(`Error unioning two shapes ${JSON.stringify(u)} ` +
          `and ${JSON.stringify(shape)}: ${err.message}`)
      }

      assert((un.length >= 1) && (un.length <= 2),
        `${JSON.stringify(un)} is not of length 1 or 2`)

      if (un.length === 1) {
        // eslint-disable-next-line prefer-destructuring
        u = un[0]
      } else if (un.length === 2) {
        disconnected.push(un[0])
        // eslint-disable-next-line prefer-destructuring
        u = un[1]
      }
    })

    this.assertProperShape(u)

    for (const d of Array.from(disconnected)) {
      this.assertProperShape(d)
    }

    // If they're all rooted on the positive x-axis, just smooshing them
    // all together should work

    if (disconnected.length > 0) {
      const last = u
      u = Array.prototype.concat.apply(disconnected[0], disconnected.slice(1))
      u = u.concat(last)
    }

    this.assertProperShape(u)

    debug(`combined = ${JSON.stringify(u)}`)

    return u
  }

  defuzzifyDim (res, sets, outputName) {
    const clipped = {}
    _.each(res, (membership, name) => {
      const shape = sets[name]
      const clippedShape = this.clipShape(shape, membership, sets)
      if (clippedShape != null) {
        clipped[name] = clippedShape
      }
    })
    this.emit('clipped', clipped, outputName)
    // No fuzzy set has any output; null output in this case
    if (_.keys(clipped).length === 0) {
      return null
    } else {
      const distribution = this.combineShapes(_.values(clipped))
      this.emit('combined', _.cloneDeep(distribution), outputName)
      const ctr = centroid(distribution)
      debug(`centroid for ${outputName} is ${JSON.stringify(ctr)}`)
      this.emit('centroid', _.cloneDeep(ctr), outputName)
      return ctr[0]
    }
  }

  evaluate (args) {
    assert(_.isObject(args, `${args} is not an object.`))

    _.each(args, (arg, name) => {
      assert(_.has(this.inputs, name), `Unrecognized argument ${name}.`)
      assert(_.isFinite(arg) || _.isNull(arg),
        `${name} argument is not a number.`)
    })

    const fuzzified = this.fuzzifyArgs(args)

    this.emit('fuzzified', _.cloneDeep(fuzzified), _.cloneDeep(args))

    const results = this.infer(fuzzified)

    this.emit('inferred', _.cloneDeep(results), _.cloneDeep(fuzzified),
      _.cloneDeep(args))

    const crisp = this.defuzzify(results)

    assert(_.isObject(crisp, `Output ${crisp} is not an object.`))

    _.each(crisp, (result, name) => {
      assert(_.has(this.outputs, name), `Unrecognized result ${name}.`)
      assert((_.isNull(result) || _.isFinite(result)),
        `${name} argument is not a number or null.`)
    })

    _.each(this.outputs, (output, name) => assert(_.has(crisp, name), `Missing output ${name}.`))

    return crisp
  }

  toJSON () {
    const result = {
      inputs: _.cloneDeep(this.inputs),
      outputs: _.cloneDeep(this.outputs),
      rules: _.cloneDeep(this.rules),
      parsed_rules: _.map(this.frules, frule => frule.toJSON())
    }
    return result
  }

  toArray () {
    const addSet = function (result, set) {
      switch (set.length) {
        case 2:
          result.push(set[0])
          result.push(sub(set[1], set[0]))
          break
        case 3:
          result.push(set[1])
          result.push(sub(set[1], set[0]))
          result.push(sub(set[2], set[1]))
          break
        case 4:
          result.push(add(set[1], div(sub(set[2], set[1]), 2)))
          result.push(sub(set[2], set[1]))
          result.push(sub(set[1], set[0]))
          result.push(sub(set[3], set[2]))
          break
      }
    }

    const addMap = function (result, map) {
      for (const name of Object.keys(map).sort()) {
        const dimension = map[name]
        for (const setName of Object.keys(dimension).sort()) {
          addSet(result, dimension[setName])
        }
      }
    }

    let result = []

    addMap(result, this.inputs)
    addMap(result, this.outputs)

    for (const frule of Array.from(this.frules)) {
      let weight
      if (frule.inputSetNames) {
        weight = []
        for (const name of Array.from(frule.inputSetNames)) {
          weight.push(frule.weightMap[name])
        }
        result = _.concat(result, weight)
      } else {
        result.push(frule.weight)
      }
    }

    return result
  }

  eps (dimension) {
    let min = Infinity
    let max = Number.NEGATIVE_INFINITY
    for (const setName in dimension) {
      const set = dimension[setName]
      for (const pt of Array.from(set)) {
        if (pt > max) {
          max = pt
        }
        if (pt < min) {
          min = pt
        }
      }
    }
    return div(sub(max, min), GRANULARITY)
  }

  scale (arr, lowest, highest) {
    const min = _.min(arr)
    const max = _.max(arr)
    if (geq(min, lowest) && leq(max, highest)) {
      return arr
    } else {
      const ratio = div(sub(highest, lowest), sub(max, min))
      return _.map(arr, val => _.clamp(add(mul(sub(val, min), ratio), lowest), lowest, highest))
    }
  }

  fromArray (arr) {
    assert(_.every(arr, _.isFinite),
      `fromArray() args must be finite numbers: ${js(arr)}`)

    let i = 0

    const updateSets = dimensions => {
      const names = _.keys(dimensions)
      names.sort()
      return (() => {
        const result = []
        for (let j = 0, end = names.length - 1, asc = end >= 0; asc ? j <= end : j >= end; asc ? j++ : j--) {
          const dimension = dimensions[names[j]]
          const epsilon = this.eps(dimension)
          const setNames = _.keys(dimension)
          setNames.sort()
          result.push((() => {
            const result1 = []
            for (const setName of Array.from(setNames)) {
              const set = dimension[setName]

              switch (set.length) {
                case 2:
                  set[0] = arr[i]
                  i++
                  set[1] = add(set[0], Math.max(arr[i], epsilon))
                  result1.push(i++)
                  break
                case 3:
                  set[1] = arr[i]
                  i++
                  set[0] = sub(set[1], Math.max(arr[i], epsilon))
                  i++
                  set[2] = add(set[1], Math.max(arr[i], epsilon))
                  result1.push(i++)
                  break
                case 4:
                  const center = arr[i]
                  i++
                  const width = Math.max(arr[i], epsilon)
                  i++
                  set[1] = sub(center, div(width, 2.0))
                  set[2] = add(center, div(width, 2.0))
                  set[0] = sub(set[1], Math.max(arr[i], epsilon))
                  i++
                  set[3] = add(set[2], Math.max(arr[i], epsilon))
                  result1.push(i++)
                  break
                default:
                  result1.push(undefined)
              }
            }
            return result1
          })())
        }
        return result
      })()
    }

    updateSets(this.inputs)
    updateSets(this.outputs)

    // Re-scale inputs between 0.0 and 1.0

    const weights = this.scale(arr.slice(i), 0.0, 1.0)

    let j = 0

    for (const frule of Array.from(this.frules)) {
      let weight
      if (frule.inputSetNames) {
        weight = weights.slice(j, j + frule.inputSetNames.length)
        frule.weight = weight
        frule.weightMap = _.zipObject(frule.inputSetNames, weight)
        j += weight.length
      } else {
        weight = weights[j]
        j++
        frule.weight = weight
      }
    }

    // Re-generate the text rules!

    this.rules = _.map(this.frules, frule => frule.toString())

    return true
  }

  // Determine the rough scale of a parameter

  epsAt (i) {
    let j, pts
    let asc, end
    let asc1, end1
    let n = 0

    // For inputs, the scale is roughly the difference between min and max values

    let names = _.keys(this.inputs)
    names.sort()

    for (j = 0, end = names.length - 1, asc = end >= 0; asc ? j <= end : j >= end; asc ? j++ : j--) {
      pts = _.flatten(_.values(this.inputs[names[j]]))
      if ((i >= n) && (i < (n + pts.length))) {
        return this.eps(this.inputs[names[j]])
      } else {
        n += pts.length
      }
    }

    // For outputs, the scale is roughly the diff between min and max values

    names = _.keys(this.outputs)
    names.sort()
    for (j = 0, end1 = names.length - 1, asc1 = end1 >= 0; asc1 ? j <= end1 : j >= end1; asc1 ? j++ : j--) {
      pts = _.flatten(_.values(this.outputs[names[j]]))
      if ((i >= n) && (i < (n + pts.length))) {
        return this.eps(this.outputs[names[j]])
      } else {
        n += pts.length
      }
    }

    // For rules, the scale is always 1.0

    return div(1.0, GRANULARITY)
  }

  clone () {
    const cinputs = _.cloneDeep(this.inputs)
    const coutputs = _.cloneDeep(this.outputs)
    const crules = _.cloneDeep(this.rules)
    return new FuzzyController(cinputs, coutputs, crules)
  }

  slopeAt (i, value, args) {
    let further
    debug(`slopeAt(${i}, ${value}, ${JSON.stringify(args)})`)

    const atDelta = delta => {
      const arr = _.cloneDeep(theta)
      arr[i] = add(arr[i], delta)
      const fc = this.clone()
      fc.fromArray(arr)
      const narr = fc.toArray()
      let fuzzified = null
      let inferred = null
      fc.once('fuzzified', results => { fuzzified = results })
      fc.once('inferred', results => { inferred = results })
      const result = fc.evaluate(args)
      const rv = {
        result,
        param: narr[i],
        fuzzified,
        inferred
      }
      return rv
    }

    const theta = this.toArray()

    const epsilon = this.epsAt(i)

    debug(`epsilon = ${epsilon}`)

    let lower = atDelta(mul(-1.0, epsilon))
    let upper = atDelta(mul(1.0, epsilon))
    const exact = atDelta(0.0)

    debug(`Initially lower = ${js(lower)}`)
    debug(`Initially upper = ${js(upper)}`)
    debug(`Initially exact = ${js(exact)}`)

    // Deal with cases where the results aren't defined either in the upper or
    // lower limit.

    if (_.some(lower.result, _.isNull)) {
      // Both bounds are undefined
      if (_.some(upper.result, _.isNull)) {
        // XXX try values closer to the mid-point
      // Value is defined at the mid-point, and is not the same as the upper
      // limit
      } else if (!_.some(exact.result, _.isNull) && (exact.param !== upper.param)) {
        lower = exact
      // Try further along the upper direction
      } else {
        further = atDelta(mul(2.0, epsilon))
        if (!_.some(further.result, _.isNull) && (further.param !== upper.param)) {
          lower = upper
          upper = further
        }
      }
    } else if (_.some(upper.result, _.isNull)) {
      assert(!_.some(lower.result, _.isNull))
      // Value is defined at the mid-point,
      // and is not the same as the lower limit
      if (!_.some(exact.result, _.isNull) && (exact.param !== lower.param)) {
        upper = exact
      } else {
        // Try further along the lower direction
        further = atDelta(mul(-2.0, epsilon))
        if (!_.some(further.result, _.isNull) && (further.param !== lower.param)) {
          upper = lower
          lower = further
        }
      }
    }

    debug(`Eventually lower = ${js(lower)}`)
    debug(`Eventually upper = ${js(upper)}`)
    debug(`Eventually exact = ${js(exact)}`)

    // Calculate the difference between upper and lower params

    const delta = sub(upper.param, lower.param)

    debug(`delta = ${delta}`)

    // Is the delta too small? We need a non-zero
    // delta for the division op below

    if (delta < div(epsilon, 2.0)) {
      const state = {
        epsilon,
        delta,
        upper: upper.param,
        lower: lower.param
      }
      throw new Error(`No difference between params: ${js(state)}`)
    }

    const slope = {}

    for (const outputName of Array.from(_.keys(this.outputs))) {
      const lres = lower.result[outputName]
      const ures = upper.result[outputName]
      if (_.isNull(lres)) {
        debug(`Lower result is null: (${js(lower)}), (${js(upper)})`)
        slope[outputName] = null
      } else if (_.isNull(ures)) {
        debug(`Upper result is null: (${js(lower)}), (${js(upper)})`)
        slope[outputName] = null
      } else {
        slope[outputName] =
          div(sub(ures, lres), delta)
        if (!_.isFinite(slope[outputName])) {
          throw new Error(`slope for ${outputName} is not a number: ` +
            `${slope[outputName]}`)
        }
      }
    }

    debug(slope)

    return slope
  }

  secondDerivative (i, j, firstValue, secondValue, args) {
    const theta = this.toArray()

    const epsilon = this.epsAt(i)

    const lower = _.cloneDeep(theta)
    lower[j] = sub(secondValue, div(epsilon, 2.0))
    const flower = this.clone()
    flower.fromArray(lower)

    const upper = _.cloneDeep(theta)
    upper[j] = add(secondValue, div(epsilon, 2.0))
    const fupper = this.clone()
    fupper.fromArray(upper)

    const slower = flower.slopeAt(i, firstValue, args)
    const supper = fupper.slopeAt(i, firstValue, args)

    const second = {}
    for (const outputName of Array.from(_.keys(this.outputs))) {
      const delta = sub(supper[outputName], slower[outputName])
      second[outputName] = div(delta, epsilon)
    }

    return second
  }

  addRule (rule) {
    assert(_.isString(rule), 'Each rule must be a string.')
    this.rules.push(rule)
    const parser = new FuzzyRuleParser()
    const frule = parser.parse(rule)
    frule.validate(this.inputs, this.outputs)
    return this.frules.push(frule)
  }

  inputSlope (inputName, outputName, inputs) {
    const islope = (ldelta, udelta) => {
      // Upper and lower values

      const uinputs = _.clone(inputs)
      uinputs[inputName] = inputs[inputName] + udelta

      debug(`uinputs = ${js(uinputs)}`)

      const uoutputs = this.evaluate(uinputs)

      debug(`uoutputs = ${js(uoutputs)}`)

      const linputs = _.clone(inputs)
      linputs[inputName] = inputs[inputName] - ldelta

      debug(`linputs = ${js(linputs)}`)

      const loutputs = this.evaluate(linputs)
      debug(`loutputs = ${js(loutputs)}`)

      if (_.isNumber(uoutputs[outputName]) && _.isNumber(loutputs[outputName])) {
        const odelta = (uoutputs[outputName] - loutputs[outputName])
        const idelta = (uinputs[inputName] - linputs[inputName])
        assert(idelta !== 0, 'input difference is zero')
        return odelta / idelta
      } else {
        return null
      }
    }

    assert(_.has(this.inputs, inputName), `Unknown input ${inputName}`)
    assert(_.has(this.outputs, outputName), `Unknown output ${outputName}`)
    for (let value = 0; value < inputs.length; value++) {
      const key = inputs[value]
      assert(_.isString(key), `${key} is not a string`)
      assert(_.has(this.inputs, key), `${key} in inputs is an unknown input`)
      assert(_.isNumber(value), `${value} is not a number`)
    }
    assert(_.has(inputs, inputName), `No value for ${inputName} in inputs`)

    // Generate an epsilon appropriate for this input variable

    const epsilon = this.eps(this.inputs[inputName])

    debug(`epsilon = ${epsilon}`)

    let slope = islope(epsilon, epsilon)

    if (_.isNull(slope)) {
      slope = islope(0, 2 * epsilon)
    }

    if (_.isNull(slope)) {
      slope = islope(2 * epsilon, 0)
    }

    return slope
  }

  static fuzzify (crisp, sets) {
    return fuzzify(crisp, sets)
  }
}

const packageDotJson = path.join(__dirname, '..', 'package.json')

if (fs.existsSync(packageDotJson)) {
  const pkg = fs.readFileSync(packageDotJson)
  const data = JSON.parse(pkg)
  FuzzyController.version = data.version
} else {
  FuzzyController.version = 'unknown'
}

// Include the parser error class so others can check against it

FuzzyController.FuzzyRuleParserError = FuzzyRuleParserError

module.exports = FuzzyController

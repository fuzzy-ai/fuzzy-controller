// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

let intercept, slope
const assert = require('assert')

const _ = require('lodash')
const debug = require('debug')('fuzzy-controller:segment')
const {add, sub, mul, div} = require('./careful-math')

const [eq, geq, leq] = Array.from(require('./equalish'))

exports.slope = (slope = function (e) {
  if (eq(e[1][0], e[0][0])) {
    return Infinity
  } else {
    return div(sub(e[1][1], e[0][1]), sub(e[1][0], e[0][0]))
  }
})

exports.intercept = (intercept = (slope, e) => add(mul(-1, slope, e[0][0]), e[0][1]))

const peq = (p1, p2) => eq(p1[0], p2[0]) && eq(p1[1], p2[1])

const trimTo = function (result, e1, e2) {
  let [x, y] = Array.from(result)
  // Trim to make sure we're within bounds
  x = Math.min(Math.max(x, Math.min(e1[0][0], e1[1][0]), Math.min(e2[0][0], e2[1][0])),
    Math.max(e1[0][0], e1[1][0]), Math.max(e2[0][0], e2[1][0]))
  y = Math.min(Math.max(y, Math.min(e1[0][1], e1[1][1]), Math.min(e2[0][1], e2[1][1])),
    Math.max(e1[0][1], e1[1][1]), Math.max(e2[0][1], e2[1][1]))
  return [x, y]
}

const bothOnSameSide = function (a, b) {
  const diff1 = sub(a[0][1], b)
  const diff2 = sub(a[1][1], b)

  return ((diff1 > 1e-8) && (diff2 > 1e-8)) || ((diff1 < -1e-8) && (diff2 < -1e-8))
}

const js = JSON.stringify

exports.intersect = function (e1, e2) {
  debug(`intersect(${js(e1)}, ${js(e2)})`)

  const isBetween = (a, b, c) => geq(a, Math.min(b, c)) && leq(a, Math.max(b, c))

  const m1 = slope(e1)
  const m2 = slope(e2)

  debug(`slope(${js(e1)}) is ${m1}`)
  debug(`slope(${js(e2)}) is ${m2}`)

  let result = null

  if (eq(m1, m2)) { // parallel
    let s1, s2
    debug('Segments are parallel')
    if (m1 === Infinity) { // vertical and parallel
      debug('Segments are vertical and parallel')
      if (!eq(e1[0][0], e2[0][0])) { // vertical and not collinear
        debug('Segments are vertical and parallel and not collinear')
        result = null
      } else { // vertical and collinear
        debug('Segments are vertical and parallel and collinear')
        const vertically = (a, b) => sub(a[1], b[1])
        s1 = _.clone(e1)
        s2 = _.clone(e2)
        s1.sort(vertically)
        s2.sort(vertically)
        assert(leq(s1[0][1], s1[1][1]))
        assert(leq(s2[0][1], s2[1][1]))
        if (leq(s1[0][1], s2[0][1]) && leq(s2[1][1], s1[1][1])) {
          result = s2
        } else if (leq(s2[0][1], s1[0][1]) && leq(s1[1][1], s2[1][1])) {
          result = s1
        } else if (leq(s1[0][1], s2[0][1]) && leq(s2[0][1], s1[1][1]) && leq(s1[1][1], s2[1][1])) {
          result = [s2[0], s1[1]]
        } else if (leq(s2[0][1], s1[0][1]) && (s1[0][1] < s2[1][1]) && leq(s2[1][1], s1[1][1])) {
          result = [s1[1], s2[1]]
        } else {
          result = null
        }
      }
    } else { // parallel, not vertical
      debug('Segments are parallel but not vertical')
      const b1 = intercept(m1, e1)
      const b2 = intercept(m2, e2)
      debug(`intercept(${js(m1)}, ${js(e1)}) = ${b1}`)
      debug(`intercept(${js(m2)}, ${js(e2)}) = ${b2}`)
      if (!eq(b1, b2)) { // not vertical, parallel but not collinear
        debug('not vertical, parallel, not collinear')
        result = null
      } else { // Not vertical, parallel, collinear; look for overlap
        debug('not vertical, parallel, collinear')
        const horizontally = (a, b) => sub(a[0], b[0])
        s1 = _.clone(e1)
        s2 = _.clone(e2)
        s1.sort(horizontally)
        s2.sort(horizontally)
        assert(leq(s1[0][0], s1[1][0]))
        assert(leq(s2[0][0], s2[1][0]))
        if (leq(s1[0][0], s2[0][0]) && leq(s2[1][0], s1[1][0])) {
          result = s2
        } else if (leq(s2[0][0], s1[0][0]) && leq(s1[1][0], s2[1][0])) {
          result = s1
        } else if (leq(s1[0][0], s2[0][0]) && leq(s2[0][0], s1[1][0]) && leq(s1[1][0], s2[1][0])) {
          result = [s2[0], s1[1]]
        } else if (leq(s2[0][0], s1[0][0]) && leq(s1[0][0], s2[1][0]) && leq(s2[1][0], s1[1][0])) {
          result = [s1[0], s2[1]]
        } else {
          result = null
        }
      }
    }
  } else { // not parallel
    let x, y
    debug('not parallel')
    // Short circuit for if they share an endpoint
    if (peq(e1[0], e2[0]) || peq(e1[0], e2[1])) {
      [result] = e1
    } else if (peq(e1[1], e2[0]) || peq(e1[1], e2[1])) {
      [, result] = e1
    } else if (m1 === Infinity) { // e1 is vertical, e2 is not
      debug('e1 is vertical, e2 is not')
      if (isBetween(e1[0][0], e2[0][0], e2[1][0])) {
        [[x]] = e1
        y = add(mul(m2, sub(x, e2[0][0])), e2[0][1])
        if (isBetween(y, e1[0][1], e1[1][1])) {
          result = [x, y]
        } else {
          result = null
        }
      }
    } else if (m2 === Infinity) { // e2 is vertical, e1 is not
      debug('e2 is vertical, e1 is not')
      if (isBetween(e2[0][0], e1[0][0], e1[1][0])) {
        [[x]] = e2
        y = add(mul(m1, sub(x, e1[0][0])), e1[0][1])
        if (isBetween(y, e2[0][1], e2[1][1])) {
          result = [x, y]
        } else {
          result = null
        }
      }
    } else if (eq(m1, 0) && bothOnSameSide(e2, e1[0][1])) {
      debug('e1 horizontal, both e2 points are on same side')
      result = null
    } else if (eq(m2, 0) && bothOnSameSide(e1, e2[0][1])) {
      debug('e2 horizontal, both e1 points are on same side')
      result = null
    } else { // neither e2 nor e1 are vertical, not parallel
      debug('neither vertical nor parallel')
      const [[x1, y1]] = e1
      const [[x2, y2]] = e2

      x = div(add(sub(mul(m1, x1), y1, mul(m2, x2)), y2), sub(m1, m2))

      debug({x})

      if (isBetween(x, x1, e1[1][0]) && isBetween(x, x2, e2[1][0])) {
        y = add(mul(m1, sub(x, x1)), y1)
        result = [x, y]
      } else {
        result = null
      }
    }
  }

  // This happens when collinear segments touch at one point
  // XXX: kind of sloppy to clean this up here but better than nothing I guess

  if (_.isArray(result) && _.isArray(result[0]) && _.isArray(result[1]) && peq(result[0], result[1])) {
    [result] = result
  }

  debug('Before trim')
  debug(result)

  // Trim to make sure points are inside the segments
  if (_.isArray(result) && _.isFinite(result[0]) && _.isFinite(result[1])) {
    result = trimTo(result, e1, e2)
  } else if (_.isArray(result) && _.isArray(result[0]) && _.isArray(result[1])) {
    result = [trimTo(result[0], e1, e2), trimTo(result[1], e1, e2)]
  }

  debug('After trim')
  debug(result)

  return result
}

// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')

class FuzzyExpression {
  evaluate (bindings) {
    return null
  }

  validate (variables) {
    return null
  }

  static fromJSON (json) {
    assert.ok(_.isString(json.type), 'FuzzyExpression json must have a type string')
    switch (json.type) {
      case 'is':
        return require('./is-expression').fromJSON(json)
      case 'and':
        return require('./and-expression').fromJSON(json)
      case 'or':
        return require('./or-expression').fromJSON(json)
      case 'not':
        return require('./not-expression').fromJSON(json)
      default:
        throw new Error(`Unrecognized expression type: ${json.type}`)
    }
  }

  wrap (identifier) {
    if (identifier.match(/\s+/)) {
      return `[${identifier}]`
    } else {
      return identifier
    }
  }
}

module.exports = FuzzyExpression

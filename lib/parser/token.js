// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const typeName = require('./type-name')

class Token {
  constructor (type, text) {
    this.type = type
    this.text = text
    if (!this.text) {
      this.text = typeName(this.type)
    }
  }

  toString () {
    return this.text
  }
}

module.exports = Token

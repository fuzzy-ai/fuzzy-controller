// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const {mul} = require('../careful-math')

const FuzzyExpression = require('./fuzzy-expression')

class AndExpression extends FuzzyExpression {
  constructor (left, right) {
    super()
    this.left = left
    this.right = right
  }

  evaluate (bindings) {
    const lv = this.left.evaluate(bindings)
    const rv = this.right.evaluate(bindings)
    const av = mul(lv, rv)

    return av
  }

  validate (variables) {
    this.left.validate(variables)
    return this.right.validate(variables)
  }

  toString () {
    return `(${this.left}) AND (${this.right})`
  }

  toJSON () {
    return {
      type: 'and',
      left: this.left.toJSON(),
      right: this.right.toJSON()
    }
  }

  static fromJSON (json) {
    assert.equal(json.type, 'and')
    assert(_.isObject(json.left), 'left is required to be an object')
    const left = FuzzyExpression.fromJSON(json.left)
    assert(_.isObject(json.right), 'right is required to be an object')
    const right = FuzzyExpression.fromJSON(json.right)
    return new (this)(left, right)
  }
}

module.exports = AndExpression

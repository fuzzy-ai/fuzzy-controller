// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

class BindingsError extends Error {
  constructor (bindings, dimension, set) {
    super('Error with bindings')
    this.bindings = bindings
    this.dimension = dimension
    this.set = set
  }
}

module.exports = BindingsError

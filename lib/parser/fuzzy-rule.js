// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')

// Superclass for all FuzzyRules

class FuzzyRule {
  apply (bindings) {
    return null
  }

  validate (inputs, outputs) {
    return null
  }

  toString () {
    return null
  }

  static fromJSON (json) {
    assert.ok(_.isString(json.type), 'FuzzyExpression json must have a type string')
    switch (json.type) {
      case 'if-then':
        return require('./if-then-fuzzy-rule').fromJSON(json)
      case 'increases':
        return require('./increases-fuzzy-rule').fromJSON(json)
      case 'decreases':
        return require('./decreases-fuzzy-rule').fromJSON(json)
      default:
        throw new Error(`Unrecognized rule type: ${json.type}`)
    }
  }
}

module.exports = FuzzyRule

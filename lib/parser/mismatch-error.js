// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const FuzzyRuleParserError = require('./fuzzy-rule-parser-error')

class MismatchError extends FuzzyRuleParserError {
  constructor (source) {
    super(`Mismatched parentheses`, source)
    this.name = 'MismatchError'
  }
}

module.exports = MismatchError

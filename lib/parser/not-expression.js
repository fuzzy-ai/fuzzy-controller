// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const {sub} = require('../careful-math')

const FuzzyExpression = require('./fuzzy-expression')

class NotExpression extends FuzzyExpression {
  constructor (expr) {
    super()
    this.expr = expr
  }

  evaluate (bindings) {
    const ev = this.expr.evaluate(bindings)
    const nv = sub(1.0, ev)

    return nv
  }

  validate (variables) {
    return this.expr.validate(variables)
  }

  toString () {
    return `NOT (${this.expr})`
  }

  toJSON () {
    return {
      type: 'not',
      expression: this.expr.toJSON()
    }
  }

  static fromJSON (json) {
    assert.equal(json.type, 'not')
    assert(_.isObject(json.expression), 'expression is required to be an object')
    const expression = FuzzyExpression.fromJSON(json.expression)
    return new (this)(expression)
  }
}

module.exports = NotExpression

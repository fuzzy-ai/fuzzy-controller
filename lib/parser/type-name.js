/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const _ = require('lodash')

const types = require('./types')

const names = _.invert(types)

const typeName = function (type) {
  if (type < types.MAX_KEYWORD) {
    return names[type]
  } else {
    return `<${names[type]}>`
  }
}

module.exports = typeName

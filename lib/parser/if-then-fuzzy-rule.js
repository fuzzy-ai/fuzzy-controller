// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const {mul} = require('../careful-math')

const FuzzyRule = require('./fuzzy-rule')

// IF ... THEN ...

class IfThenFuzzyRule extends FuzzyRule {
  constructor (antecedent, consequent, weight = 1.0) {
    super()
    this.antecedent = antecedent
    this.consequent = consequent
    this.weight = weight
  }

  apply (bindings) {
    const ant = this.antecedent.evaluate(bindings)

    const obj = {}
    const sub = {}
    sub[this.consequent.set] = mul(ant, this.weight)
    obj[this.consequent.dimension] = sub
    return obj
  }

  validate (inputs, outputs) {
    this.antecedent.validate(inputs)
    return this.consequent.validate(outputs)
  }

  toString () {
    if (this.weight === 1.0) {
      return `IF ${this.antecedent} THEN ${this.consequent}`
    } else {
      return `IF ${this.antecedent} THEN ${this.consequent} WITH ${this.weight}`
    }
  }

  toJSON () {
    const result = {
      type: 'if-then',
      antecedent: this.antecedent.toJSON(),
      consequent: this.consequent.toJSON()
    }
    if (this.weight !== 1.0) {
      result.weight = this.weight
    }
    return result
  }

  static fromJSON (json) {
    assert.equal(json.type, 'if-then')
    const FuzzyExpression = require('./fuzzy-expression')
    assert(_.isObject(json.antecedent), 'antecedent must be an object in fromJSON()')
    const antecedent = FuzzyExpression.fromJSON(json.antecedent)
    assert.ok(_.isObject(json.consequent), 'consequent must be an object in fromJSON()')
    const consequent = FuzzyExpression.fromJSON(json.consequent)
    assert(((json.weight == null) || _.isFinite(json.weight)), 'weight, if provided, must be a number')
    const { weight } = json
    return new (this)(antecedent, consequent, weight)
  }
}

module.exports = IfThenFuzzyRule

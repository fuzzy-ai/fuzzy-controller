// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const types = {
  IF: 100,
  THEN: 101,
  IS: 102,
  AND: 103,
  OR: 104,
  OPEN: 105,
  CLOSE: 106,
  WITH: 110,
  NOT: 111,
  INCREASES: 112,
  DECREASES: 113,
  MAX_KEYWORD: 199,
  IDENTIFIER: 200,
  NUMBER: 300,
  EOL: 400
}

module.exports = types

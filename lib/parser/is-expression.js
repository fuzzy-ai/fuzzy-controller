// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')

const FuzzyExpression = require('./fuzzy-expression')
const BindingsError = require('./bindings-error')

class IsExpression extends FuzzyExpression {
  constructor (dimension, set) {
    super()
    this.dimension = dimension
    this.set = set
    assert.ok(_.isString(this.dimension))
    assert.ok(_.isString(this.set))
  }

  evaluate (bindings) {
    assert.ok(_.isString(this.dimension))
    assert.ok(_.isString(this.set))

    if (_.has(bindings, this.dimension) && _.has(bindings[this.dimension], this.set)) {
      return bindings[this.dimension][this.set]
    } else {
      throw new BindingsError(bindings, this.dimension, this.set)
    }
  }

  validate (variables) {
    assert.ok(_.has(variables, this.dimension), `Rule mentions unknown variable ${this.dimension}.`)
    assert.ok(_.has(variables[this.dimension], this.set), `Rule mentions unknown set ${this.set} for variable ${this.dimension}.`)
    return null
  }

  toString () {
    return `${this.wrap(this.dimension)} IS ${this.wrap(this.set)}`
  }

  toJSON () {
    return {
      type: 'is',
      dimension: this.dimension,
      set: this.set
    }
  }

  static fromJSON (json) {
    assert.equal(json.type, 'is')
    assert(_.isString(json.dimension), 'dimension is required to be a string')
    assert(_.isString(json.set), 'set is required to be a string')
    return new (this)(json.dimension, json.set)
  }
}

module.exports = IsExpression

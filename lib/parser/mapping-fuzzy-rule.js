// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const {mul} = require('../careful-math')

const FuzzyRule = require('./fuzzy-rule')
const [, geq] = Array.from(require('../equalish'))

// Superclass for INCREASES and DECREASES rules

class MappingFuzzyRule extends FuzzyRule {
  constructor (input, output, weight) {
    super()
    this.input = input
    this.output = output
    if (weight == null) { weight = 1.0 }
    this.weight = weight
    assert.ok(_.isString(this.input))
    assert.ok(_.isString(this.output))
    assert.ok(_.isFinite(this.weight) || (_.isArray(this.weight) && _.every(this.weight, _.isFinite)))
    this.setMap = {}
    this.weightMap = {}
  }

  apply (bindings) {
    const obj = {}
    obj[this.output] = {}

    for (const inputSetName in this.setMap) {
      const outputSetName = this.setMap[inputSetName]
      if (_.has(bindings, this.input) && _.has(bindings[this.input], inputSetName)) {
        const ant = bindings[this.input][inputSetName]
        obj[this.output][outputSetName] = mul(ant, this.weightMap[inputSetName])
      }
    }

    return obj
  }

  validate (inputs, outputs) {
    assert.ok(_.has(inputs, this.input), `Rule mentions unknown input variable ${this.input}.`)
    assert.ok(_.has(outputs, this.output), `Rule mentions unknown output variable ${this.output}.`)
    const inputSetCount = _.keys(inputs[this.input]).length
    const outputSetCount = _.keys(outputs[this.output]).length
    assert.ok(inputSetCount === outputSetCount, `Increases rule with different number of input sets (${inputSetCount}) than output sets (${outputSetCount}).`)
    if (_.isArray(this.weight)) {
      assert.ok(inputSetCount === this.weight.length, `Increases rule with different number of input sets (${inputSetCount}) than weights (${this.weight.length}).`)
    }
    // XXX: this is sneaky; it would be better to do this in the constructor
    this.makeSetMap(inputs[this.input], outputs[this.output])
  }

  sortSets (sets) {
    const pairs = _.toPairs(sets)

    const leftmostSlope = points => _.every(pairs, pair => (pair[1].length !== 2) || geq(pair[1][0], points[0]))

    const leftmostPeak = function (pair) {
      const [, points] = pair
      switch (points.length) {
        case 2:
          if (leftmostSlope(points)) {
            return points[0]
          } else {
            return points[1]
          }
        case 3:
          return points[1]
        case 4:
          return points[1]
      }
    }

    pairs.sort((a, b) => {
      const almp = leftmostPeak(a)
      const blmp = leftmostPeak(b)

      if (almp < blmp) {
        return -1
      } else if (almp > blmp) {
        return 1
      } else {
        return 0
      }
    })

    const names = pairs.map(pair => pair[0])

    return names
  }

  wrap (identifier) {
    if (identifier.match(/\s+/)) {
      return `[${identifier}]`
    } else {
      return identifier
    }
  }
}

module.exports = MappingFuzzyRule

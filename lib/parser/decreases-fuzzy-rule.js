// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')

const MappingFuzzyRule = require('./mapping-fuzzy-rule')
const [eq] = Array.from(require('../equalish'))

assert(_.isFunction(eq))

// input DECREASES output

class DecreasesFuzzyRule extends MappingFuzzyRule {
  makeSetMap (inputSets, outputSets) {
    this.inputSetNames = this.sortSets(inputSets)
    this.outputSetNames = this.sortSets(outputSets)

    // This is what makes it decrease!

    this.outputSetNames.reverse()

    this.setMap = _.zipObject(this.inputSetNames, this.outputSetNames)

    if (_.isArray(this.weight)) {
      this.weightMap = _.zipObject(this.inputSetNames, this.weight)
    } else {
      Array.from(this.inputSetNames).map((name) =>
        (this.weightMap[name] = this.weight))
    }
  }

  toString () {
    if (_.isFinite(this.weight)) {
      if (this.weight === 1.0) {
        return `${this.wrap(this.input)} DECREASES ${this.wrap(this.output)}`
      } else {
        return `${this.wrap(this.input)} DECREASES ${this.wrap(this.output)} WITH ${this.weight}`
      }
    } else if (_.isArray(this.weight)) {
      if (_.every(this.weight.slice(1), x => eq(x, this.weight[0]))) {
        if (this.weight[0] === 1.0) {
          return `${this.wrap(this.input)} DECREASES ${this.wrap(this.output)}`
        } else {
          return `${this.wrap(this.input)} DECREASES ${this.wrap(this.output)} WITH ${this.weight[0]}`
        }
      } else {
        return `${this.wrap(this.input)} DECREASES ${this.wrap(this.output)} WITH ${this.weight.join(' ')}`
      }
    }
  }

  toJSON () {
    const results = {
      type: 'decreases',
      input: this.input,
      output: this.output
    }
    if (!_.isFinite(this.weight) || (this.weight !== 1.0)) {
      results.weight = this.weight
    }
    return results
  }

  static fromJSON (json) {
    assert.equal(json.type, 'decreases')
    assert(_.isString(json.input), 'input must be a string')
    assert(_.isString(json.output), 'output must be a string')
    assert((json.weight == null) || _.isFinite(json.weight) || (_.isArray(json.weight) && _.every(json.weight, _.isFinite)), 'Weight, if provided, must be a number or array of numbers')
    return new (this)(json.input, json.output, json.weight)
  }
}

module.exports = DecreasesFuzzyRule

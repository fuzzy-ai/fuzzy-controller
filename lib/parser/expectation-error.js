// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const FuzzyRuleParserError = require('./fuzzy-rule-parser-error')
const typeName = require('./type-name')

class ExpectationError extends FuzzyRuleParserError {
  constructor (expected, actual, source) {
    super(`Expected ${typeName(expected)}, got ${actual}`, source)
    this.expected = expected
    this.actual = actual
    this.name = 'ExpectationError'
  }
}

module.exports = ExpectationError

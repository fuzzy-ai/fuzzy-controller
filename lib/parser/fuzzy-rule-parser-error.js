// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

class FuzzyRuleParserError extends Error {
  constructor (message, source) {
    super(`${message} in '${source}'`)
    this.source = source
    this.name = 'FuzzyRuleParserError'
  }
}

module.exports = FuzzyRuleParserError

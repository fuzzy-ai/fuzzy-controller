// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const {add, sub, mul} = require('../careful-math')

const FuzzyExpression = require('./fuzzy-expression')

class OrExpression extends FuzzyExpression {
  constructor (left, right) {
    super()
    this.left = left
    this.right = right
  }

  evaluate (bindings) {
    const lv = this.left.evaluate(bindings)
    const rv = this.right.evaluate(bindings)

    const ov = sub(add(lv, rv), mul(lv, rv))

    return ov
  }

  validate (variables) {
    this.left.validate(variables)
    return this.right.validate(variables)
  }

  toString () {
    return `(${this.left}) OR (${this.right})`
  }

  toJSON () {
    return {
      type: 'or',
      left: this.left.toJSON(),
      right: this.right.toJSON()
    }
  }

  static fromJSON (json) {
    assert.equal(json.type, 'or')
    assert(_.isObject(json.left), 'left is required to be an object')
    const left = FuzzyExpression.fromJSON(json.left)
    assert(_.isObject(json.right), 'right is required to be an object')
    const right = FuzzyExpression.fromJSON(json.right)
    return new (this)(left, right)
  }
}

module.exports = OrExpression

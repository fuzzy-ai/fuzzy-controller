// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const FuzzyRuleParserError = require('./fuzzy-rule-parser-error')

class InvalidWeightError extends FuzzyRuleParserError {
  constructor (weight, source) {
    super(`Invalid weight ${weight}; must be between 0.0 and 1.0 inclusive`, source)
    this.weight = weight
    this.name = 'InvalidWeightError'
  }
}

module.exports = InvalidWeightError

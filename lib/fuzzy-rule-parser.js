// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')
const { EventEmitter } = require('events')

const _ = require('lodash')

const IsExpression = require('./parser/is-expression')
const AndExpression = require('./parser/and-expression')
const OrExpression = require('./parser/or-expression')
const NotExpression = require('./parser/not-expression')
const IfThenFuzzyRule = require('./parser/if-then-fuzzy-rule')
const IncreasesFuzzyRule = require('./parser/increases-fuzzy-rule')
const DecreasesFuzzyRule = require('./parser/decreases-fuzzy-rule')
const types = require('./parser/types')
const Token = require('./parser/token')
const FuzzyRuleParserError = require('./parser/fuzzy-rule-parser-error')
const ExpectationError = require('./parser/expectation-error')
const MismatchError = require('./parser/mismatch-error')
const InvalidWeightError = require('./parser/invalid-weight-error')
const [, geq, leq] = Array.from(require('./equalish'))

class FuzzyRuleParser extends EventEmitter {
  parse (source) {
    const current = function () {
      if (tokens.length > 0) {
        return tokens[0]
      } else {
        throw new FuzzyRuleParserError('Unexpected end of input', source)
      }
    }

    const currentType = function () {
      const token = current()
      return (token != null ? token.type : undefined)
    }

    const accept = () => tokens.shift()

    // These work almost the same, so we simplify using them

    const acceptKeyword = name =>
      function () {
        if (currentType() === types[name]) {
          return accept()
        } else {
          throw new ExpectationError(types[name], current(), source)
        }
      }

    const acceptIf = acceptKeyword('IF')
    const acceptIs = acceptKeyword('IS')
    const acceptThen = acceptKeyword('THEN')
    const acceptWith = acceptKeyword('WITH')
    const acceptIncreases = acceptKeyword('INCREASES')
    const acceptDecreases = acceptKeyword('DECREASES')
    const acceptEOL = acceptKeyword('EOL')

    const acceptIdentifier = function () {
      if (currentType() === types.IDENTIFIER) {
        const str = current()
        accept()
        return str
      } else {
        throw new ExpectationError(types.IDENTIFIER, current(), source)
      }
    }

    const acceptNumber = function () {
      if (currentType() === types.NUMBER) {
        let num
        const tok = current()
        try {
          num = parseFloat(tok.text)
        } catch (err) {
          throw new ExpectationError(types.NUMBER, current(), source)
        }
        accept()
        return num
      } else {
        throw new ExpectationError(types.NUMBER, current(), source)
      }
    }

    const acceptIsExpression = function () {
      const dimension = acceptIdentifier()
      acceptIs()
      const set = acceptIdentifier()
      return new IsExpression(dimension.text, set.text)
    }

    const acceptSimpleExpression = function () {
      let expr = null
      if (currentType() === types.OPEN) {
        accept()
        expr = acceptSimpleExpression()
        if (currentType() === types.CLOSE) {
          accept()
        } else {
          throw new FuzzyRuleParserError('Mismatched parentheses', source)
        }
      } else {
        expr = acceptIsExpression()
      }
      return expr
    }

    const acceptAtomicExpression = function () {
      let expr = null
      switch (currentType()) {
        case types.NOT:
          accept()
          expr = new NotExpression(acceptAtomicExpression())
          break
        case types.OPEN:
          accept()
          expr = acceptExpression()
          if (currentType() === types.CLOSE) {
            accept()
          } else {
            throw new MismatchError(source)
          }
          break
        case types.IDENTIFIER:
          expr = acceptIsExpression()
          break
        default:
          throw new FuzzyRuleParserError(`Expected an identifier, NOT, or ( but got ${current()}`, source)
      }
      return expr
    }

    const acceptExpression = function () {
      let expr
      let right
      const left = acceptAtomicExpression()

      switch (currentType()) {
        case types.AND:
          accept()
          right = acceptExpression()
          expr = new AndExpression(left, right)
          break
        case types.OR:
          accept()
          right = acceptExpression()
          expr = new OrExpression(left, right)
          break
        default:
          expr = left
      }

      return expr
    }

    const acceptIfThenFuzzyRule = function () {
      let weight
      acceptIf()
      const antecedent = acceptExpression()
      acceptThen()
      const consequent = acceptSimpleExpression()

      if (currentType() === types.WITH) {
        acceptWith()
        weight = acceptNumber()
      } else {
        weight = 1.0
      }

      if (!(geq(weight, 0.0) && leq(weight, 1.0))) {
        throw new InvalidWeightError(weight, source)
      }

      weight = _.clamp(weight, 0.0, 1.0)

      acceptEOL()

      return new IfThenFuzzyRule(antecedent, consequent, weight)
    }

    const acceptMappingRule = function () {
      let operator, weight
      const input = acceptIdentifier()
      if (currentType() === types.INCREASES) {
        operator = types.INCREASES
        acceptIncreases()
      } else if (currentType() === types.DECREASES) {
        operator = types.DECREASES
        acceptDecreases()
      } else {
        throw new FuzzyRuleParserError(`Expected INCREASES or DECREASES but got ${current()}`, source)
      }
      const output = acceptIdentifier()

      if (currentType() === types.WITH) {
        acceptWith()
        weight = acceptNumber()
      } else {
        weight = 1.0
      }

      if (!(geq(weight, 0.0) && leq(weight, 1.0))) {
        throw new InvalidWeightError(weight, source)
      }

      // Fix problems with rounding errors

      weight = _.clamp(weight, 0.0, 1.0)

      // accept multiple weights

      while (currentType() === types.NUMBER) {
        if (!_.isArray(weight)) {
          weight = [weight]
        }
        const next = acceptNumber()
        weight.push(next)
      }

      acceptEOL()

      if (operator === types.INCREASES) {
        return new IncreasesFuzzyRule(input.text, output.text, weight)
      } else if (operator === types.DECREASES) {
        return new DecreasesFuzzyRule(input.text, output.text, weight)
      } else {
        throw new Error(`Unexpected operator ${operator}`)
      }
    }

    const acceptRule = function () {
      let rule
      if (currentType() === types.IF) {
        rule = acceptIfThenFuzzyRule()
      } else if (currentType() === types.IDENTIFIER) {
        rule = acceptMappingRule()
      } else {
        throw new FuzzyRuleParserError(`Expected an identifier or IF but got ${current()}`, source)
      }

      return rule
    }

    const tokens = this.tokenize(source)

    assert.ok(_.isArray(tokens))

    const rule = acceptRule()

    return rule
  }

  // Yes this is janky.

  tokenize (source) {
    // Split on white space

    let tokens = source.split(/\s+/)

    // Separate out ( and ) as separate tokens

    tokens = _.map(tokens, (token) => {
      const opens = []
      const closes = []

      while (token[0] === '(') {
        opens.push('(')
        token = token.slice(1)
      }

      while (token[token.length - 1] === ')') {
        closes.push(')')
        token = token.slice(0, token.length - 1)
      }

      if ((opens.length > 0) || (closes.length > 0)) {
        return opens.concat([token], closes)
      } else {
        return token
      }
    })

    tokens = _.flatten(tokens)

    // separate out [ ] as separate token
    // Note: we handle ([]) but ignore [()]

    tokens = _.map(tokens, (token) => {
      if (token[0] === '[') {
        token = ['[', token.slice(1)]
      } else if (token[token.length - 1] === ']') {
        token = [token.slice(0, token.length - 1), ']']
      }
      return token
    })

    tokens = _.flatten(tokens)

    // For identifiers with multiple words

    let identifier = null
    let inIdentifier = false

    const old = tokens

    tokens = []

    for (const token of Array.from(old)) {
      if (token === '[') {
        inIdentifier = true
      } else if (token === ']') {
        if (!inIdentifier) {
          throw new FuzzyRuleParserError('Found closing square bracket with no opening bracket', source)
        } else if ((identifier == null)) {
          throw new FuzzyRuleParserError('Found closing square bracket with no identifier', source)
        } else {
          tokens.push(identifier)
          identifier = null
          inIdentifier = false
        }
      } else if (inIdentifier) {
        if (identifier != null) {
          identifier = `${identifier} ${token}`
        } else {
          identifier = token
        }
      } else {
        tokens.push(token)
      }
    }

    _.each(tokens, (token, i) => assert(_.isString(token), `Token ${i} (${token}) is not a string in '${JSON.stringify(tokens)}'`))

    // Replace keyword strings with numerical constants

    tokens = _.map(tokens, (token) => {
      const code = types[token.toUpperCase()]
      if (code && (code < types.MAX_KEYWORD)) {
        return new Token(code, token)
      } else {
        if (token === '(') {
          return new Token(types.OPEN, token)
        } else if (token === ')') {
          return new Token(types.CLOSE, token)
        // regex from http://stackoverflow.com/questions/13340717/json-numbers-regular-expression
        } else if (token.match(/^-?(?:0|[1-9]\d*)(?:\.\d+)?(?:[eE][+-]?\d+)?$/)) {
          return new Token(types.NUMBER, token)
        } else {
          return new Token(types.IDENTIFIER, token)
        }
      }
    })

    tokens.push(new Token(types.EOL))

    return tokens
  }
}

module.exports = FuzzyRuleParser

// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const assert = require('assert')
const _ = require('lodash')
const debug = require('debug')('fuzzy-controller:centroid')

const [eq] = Array.from(require('./equalish'))
const {add, sub, mul, div} = require('./careful-math')

const isPoint = pt => _.isArray(pt) && (pt.length === 2) && _.isFinite(pt[0]) && _.isFinite(pt[1])

const js = JSON.stringify

const triangleCentroid = function (a, b, c) {
  debug(`triangleCentroid(${js(a)}, ${js(b)}, ${js(c)})`)

  assert(isPoint(a), `Argument a to triangleCentroid is not a point: ${a}`)
  assert(isPoint(b), `Argument b to triangleCentroid is not a point: ${b}`)
  assert(isPoint(c), `Argument c to triangleCentroid is not a point: ${c}`)

  const ctr = [0, 0]

  ctr[0] = div(add(a[0], b[0], c[0]), 3)
  ctr[1] = div(add(a[1], b[1], c[1]), 3)

  assert(isPoint(ctr), `Result of triangleCentroid is not a point: ${b}`)

  debug(`triangleCentroid(${js(a)}, ${js(b)}, ${js(c)}) = ${js(ctr)}`)

  return ctr
}

const triangleArea = function (a, b, c) {
  let h, w
  debug(`triangleArea(${js(a)}, ${js(b)}, ${js(c)})`)

  assert(isPoint(a), `Argument a to triangleArea is not a point: ${a}`)
  assert(isPoint(b), `Argument b to triangleArea is not a point: ${b}`)
  assert(isPoint(c), `Argument c to triangleArea is not a point: ${c}`)

  if (eq(a[0], b[0]) && (a[1] > b[1])) { // downslope
    h = sub(a[1], b[1])
    w = sub(c[0], b[0])
  } else if (eq(b[0], c[0]) && (c[1] > b[1])) { // upslope
    h = sub(c[1], b[1])
    w = sub(b[0], a[0])
  }

  debug(`Height = ${h}`)
  debug(`Width = ${w}`)

  const area = div(mul(h, w), 2)

  assert(_.isFinite(area),
    `Result of triangleArea is not a finite number: ${area}`)

  debug(`triangleArea(${js(a)}, ${js(b)}, ${js(c)}) = ${area}`)

  return area
}

const rectangleCentroid = function (a, b) {
  debug(`rectangleCentroid(${js(a)}, ${js(b)})`)

  assert(isPoint(a), `Argument a to rectangleCentroid is not a point: ${a}`)
  assert(isPoint(b), `Argument b to rectangleCentroid is not a point: ${b}`)

  assert(eq(a[1], b[1]), `Rectangle height is not eq(): ${a[1]}, ${b[1]}`)

  const ctr = [0, 0]

  ctr[0] = div(add(a[0], b[0]), 2)
  ctr[1] = div(a[1], 2)

  debug(`rectangleCentroid(${js(a)}, ${js(b)}) = ${js(ctr)}`)

  assert(isPoint(ctr), `Result of rectangleCentroid is not a point: ${b}`)

  return ctr
}

const rectangleArea = function (a, b) {
  debug(`rectangleArea(${js(a)}, ${js(b)})`)

  assert(isPoint(a), `Argument a to rectangleArea is not a point: ${a}`)
  assert(isPoint(b), `Argument b to rectangleArea is not a point: ${b}`)

  assert(eq(a[1], b[1]), `Rectangle height is not eq(): ${a[1]}, ${b[1]}`)

  const [, h] = a
  const w = sub(b[0], a[0])

  debug(`Height = ${h}`)
  debug(`Width = ${w}`)

  const area = mul(h, w)

  assert(_.isFinite(area),
    `Result of rectangleArea is not a finite number: ${area}`)

  debug(`rectangleArea(${js(a)}, ${js(b)}) = ${area}`)

  return area
}

const centroid = function (points) {
  let wy
  debug(`points = ${JSON.stringify(points)}`)

  // Format: an array of points

  assert(_.isArray(points), 'Argument to centroid() is not array')

  // Each point is a 2-number array

  _.each(points, (point, i) => assert(isPoint(point), `${i} of args to centroid() not a point: ${point}`))

  // First and last point are on the X-axis

  assert(eq(points[0][1], 0),
    `First point not on X axis: ${JSON.stringify(points)}`)
  assert(eq(points[points.length - 1][1], 0),
    `Last point not on X axis: ${JSON.stringify(points)}`)

  const centroids = []
  const areas = []

  _.each(points, (point, i) => {
    if (i === (points.length - 1)) {
      return
    }
    const next = points[i + 1]
    if (point[0] === next[0]) { // vertical line

    } else if (eq(point[1], next[1])) { // horizontal line
      centroids.push(rectangleCentroid(point, next))
      return areas.push(rectangleArea(point, next))
    } else if (point[1] > next[1]) { // negative slope
      centroids.push(triangleCentroid(point, [point[0], next[1]], next))
      areas.push(triangleArea(point, [point[0], next[1]], next))
      if (next[1] > 0) {
        centroids.push(rectangleCentroid([point[0], next[1]], next))
        return areas.push(rectangleArea([point[0], next[1]], next))
      }
    } else if (point[1] < next[1]) { // positive slope
      centroids.push(triangleCentroid(point, [next[0], point[1]], next))
      areas.push(triangleArea(point, [next[0], point[1]], next))
      if (point[1] > 0) {
        centroids.push(rectangleCentroid(point, [next[0], point[1]]))
        return areas.push(rectangleArea(point, [next[0], point[1]]))
      }
    }
  })

  assert.equal(areas.length, centroids.length,
    `More areas ${areas.length} than centroids ${centroids.length}`)

  debug(`areas = ${js(areas)}`)

  const totalArea = add.apply(this, areas)

  debug(`totalArea = ${totalArea}`)

  let wx = (wy = 0)

  debug(`centroids = ${js(centroids)}`)

  _.each(centroids, (ctr, i) => {
    debug(`ctr[${i}] = ${js(ctr)}`)
    debug(`areas[${i}] = ${areas[i]}`)
    wx = add(wx, mul(ctr[0], areas[i]))
    wy = add(wy, mul(ctr[1], areas[i]))
    debug(`wx at ${i} is ${wx}`)
    return debug(`wy at ${i} is ${wy}`)
  })

  assert(_.isFinite(wx), `wx is not finite: ${wx}`)
  assert(_.isFinite(wy), `wy is not finite: ${wy}`)
  assert(_.isFinite(totalArea), `totalArea is not finite: ${totalArea}`)

  debug({wx, wy, totalArea})

  const ctr = [div(wx, totalArea), div(wy, totalArea)]

  debug(ctr)

  assert(isPoint(ctr),
    `Result of centroid(${JSON.stringify(points)}) is not a point: ${ctr}`)

  return ctr
}

module.exports = centroid

fuzzy-controller
----------------

NodeJS library to create fuzzy-logic-based controllers.

If you're not familiar with fuzzy logic, I recommend the Wikipedia
article as a starting point:
http://en.wikipedia.org/wiki/Fuzzy_control_system

Installation
============

Get it from [npm](https://npmjs.org/).

  npm install fuzzy-controller

Or add it to your [package.json](https://www.npmjs.org/doc/files/package.json.html).

Interface
=========

The module exposes a single class, so you can do something like this:

````javascript
var FuzzyController = require('fuzzy-controller');

var params = {}; // see constructor

var fc = new FuzzyController(params);

var results = fc.evaluate({temperature: 60});

setFanSpeed(results['fanSpeed']);
````

Constructor
-----------

The constructor is where you do most of the configuration for the controller.

The constructor takes a single JavaScript object as a parameter with the following (required) properties:

- **inputs**: an object. Each property is an object representing a "dimension",
  each of which has one or more properties representing fuzzy "sets". Each set in turn is an array of 2-4 numbers. 2 numbers represents a slope, 3 numbers a triangle, and 4 numbers a trapezoid. For example:

````javascript
inputs = {
  temperature: {
      cold: [50, 75]
      normal: [50, 75, 85, 100]
      hot: [85, 100]
  }
}
````

Note that the numbers represent the x-value of a point in the cartesian plain. This is so we don't have to keep writing `[50, 0]` and `[75, 1]` over and over. The first and last numbers of a trapezoid and a triangle are paired with zero, and the middle values are paired with 1. For slopes, we guess whether it's an upward or downward slope based on if it's the leftmost or rightmost slope in the group of sets.

- **outputs** Like inputs, an object for which each property is a dimension,    each property of which is a fuzzy set.

- **rules** An array of strings representing the rules for the fuzzy controller. The rules language is more or less copied from [FCL](http://en.wikipedia.org/wiki/Fuzzy_Control_Language) and works something like this:

```
IF <condition> THEN <result>
```

<condition> can either be an IS-expression, where an input is a member of a fuzzy set:

````
IF temperature IS hot THEN <result>
````

...or it's a fuzzy AND or OR of two IS expressions. You can nest these till the cows come home. Use parentheses, though.

````
IF (temperature IS hot) AND (humidity IS high) THEN <result>
````

The result is another IS expression, like this:

````
IF temperature IS hot THEN fanSpeed is high
````

You can add lots of rules.

Note that, because of the rules parsing, constructing a new `FuzzyController` is kind of expensive. You should probably construct once and evaluate several times.

Also, you can't mess around with the inputs, outputs or rules after construction. So if you need different values, make a new `FuzzyController`.

evaluate(args)
--------------

This is the part that actually does the fuzzy control. It takes these arguments:

- *args* An object where each property name is a fuzzy dimension from `inputs` with a numerical value. For example:

````javascript
var args = {temperature: 60};
````

All of the inputs that were defined when the controller was created must be defined and have a numerical value. `null` and `undefined` are not allowed.

The result is an object where each property name is a dimension and each value is a number.

````javascript
var crisp = fc.evaluate({temperature: 60});
console.log(crisp.fanSpeed); // 115.3
````

events
------

For debugging, the `FuzzyController` class is an [EventEmitter](http://nodejs.org/api/events.html#events_class_events_eventemitter).
It will emit some of the intermediate values in the evaluation process so you
can look at them or log them or debug them or whatever you like doing with
intermediate values.

If you are just trying to get your job done, *you don't need to mess around with
these events.*

- **fuzzified** When the arguments to `evaluate()` have been fuzzified. Includes
  two payloads: an object with the fuzzified values, and the `args` object
  itself. The fuzzified values object has a property for each fuzzy dimension,
  which in turn is an object with the membership of the argument in each fuzzy
  set for that dimension.

- **inferred** When the fuzzy rules have been applied, this event is emitted. It
  has three payloads: the fuzzy output (same style as the fuzzified input), the
  fuzzified input values that caused this output, and the original arguments.

- **clipped** These are the clipped polygons used for defuzzification of the
  output. It has as payload an object with each property naming a fuzzy set,
  each of which is an array of two-element arrays representing the
  polygon, and the name of the fuzzy dimension.

- **combined** This is the union of the clipped polygons. It has as a payload an
  array of point-like arrays representing the polygon, and the name of the
  fuzzy dimension.

- **centroid** This is the centroid of the combined polygon. It has a payload of
  a point-like array representing the centroid, plus the name of the fuzzy
  dimension.

All the payloads in these events are deep-cloned from the internal values, so
you can't use these events to modify the behavior of the controller.

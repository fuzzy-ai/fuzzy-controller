/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS205: Consider reworking code to avoid use of IIFEs
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')

const controllerBatch = require('./controllerbatch')

const TOLERANCE = 2

const inputSets = _.map(__range__(-10, 110, true), i => ({x: i}))

vows
  .describe('Basic structure for FuzzyController')
  .addBatch(controllerBatch({
    inputs: {
      x: {
        veryLow: [0, 25],
        low: [0, 25, 50],
        medium: [25, 50, 75],
        high: [50, 75, 100],
        veryHigh: [75, 100]
      }
    },
    outputs: {
      y: {
        veryLow: [0, 25],
        low: [0, 25, 50],
        medium: [25, 50, 75],
        high: [50, 75, 100],
        veryHigh: [75, 100]
      }
    },
    rules: [
      'x INCREASES y'
    ]
  },
  inputSets, {
    'it looks correct' (err, resultses, propses) {
      assert.ifError(err)
      assert.isArray(resultses)
      assert.isArray(propses)
      return (() => {
        const result = []
        for (let i = 0; i < inputSets.length; i++) {
          const inputSet = inputSets[i]
          const outputSet = resultses[i]
          if (inputSet.x < 0) {
            result.push(assert.inDelta(outputSet.y, 0, TOLERANCE))
          } else if (inputSet.x > 100) {
            result.push(assert.inDelta(outputSet.y, 100, TOLERANCE))
          } else {
            result.push(assert.inDelta(outputSet.y, inputSet.x, TOLERANCE))
          }
        }
        return result
      })()
    }
  }
  )).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}

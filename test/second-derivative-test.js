// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

vows
  .describe('Second derivative w/r/t parameters')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow WITH 0.5',
                'IF input1 IS low THEN output1 IS low WITH 0.5',
                'IF input1 IS medium THEN output1 IS medium WITH 0.5',
                'IF input1 IS high THEN output1 IS high WITH 0.5',
                'IF input1 IS veryHigh THEN output1 IS veryHigh WITH 0.5'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'it has a secondDerivative() method' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
          assert.isFunction(fc.secondDerivative)
        },
        'and we convert to an array': {
          topic (fc) {
            const { callback } = this
            try {
              const results = fc.toArray()
              callback(null, results)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it works' (err, results) {
            assert.ifError(err)
            assert.isArray(results)
          },
          'it looks correct' (err, results) {
            assert.ifError(err)
            assert.isArray(results)
            const expected = [75, 20, 20, 25, 20, 20, 50, 20, 20, 80, 20, 0, 20, 75, 20, 20, 25, 20, 20, 50, 20, 20, 80, 20, 0, 20, 0.5, 0.5, 0.5, 0.5, 0.5]
            assert.equal(results.length, expected.length, `Results not of expected length (${results.length} != ${expected.length})`)
            return Array.from(results).map((f, i) =>
              assert.equal(f, expected[i], `Incorrect value at index ${i}: ${f} != ${expected[i]}`))
          },
          'and we get the pairs of second derivatives at each index': {
            topic (arr, fc) {
              const { callback } = this
              const seconds = new Array(arr.length)
              for (let i = 0; i < arr.length; i++) {
                const firstValue = arr[i]
                seconds[i] = new Array(arr.length)
                for (let j = 0; j < arr.length; j++) {
                  const secondValue = arr[j]
                  try {
                    seconds[i][j] = fc.secondDerivative(i, j, firstValue, secondValue, {input1: 39})
                  } catch (err) {
                    callback(err)
                  }
                }
              }
              callback(null, seconds)
            },
            'it works' (err, seconds) {
              assert.ifError(err)
              assert.isArray(seconds)
              assert.equal(seconds.length, 31)
              return (() => {
                const result = []
                for (let i = 0; i < seconds.length; i++) {
                  const row = seconds[i]
                  assert.isArray(seconds)
                  result.push((() => {
                    const result1 = []
                    for (let j = 0; j < row.length; j++) {
                      const second = row[j]
                      assert.isObject(second)
                      result1.push(assert.isNumber(second.output1))
                    }
                    return result1
                  })())
                }
                return result
              })()
            }
          }
        }
      }
    }}).export(module)

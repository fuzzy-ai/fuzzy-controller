// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')

const polygonBatch = require('./polygonbatch')

vows
  .describe('Polygon from ROI controller')
  .addBatch(polygonBatch([
    [[55, 0], [65, 1], [75, 0]],
    [[65, 0], [75, 1], [85, 1], [95, 0]]
  ]))
  .export(module)

/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const toFromJSON = function (rule, expected) {
  const batch = {
    topic (parser) {
      const { callback } = this
      try {
        const frule = parser.parse(rule)
        callback(null, frule)
      } catch (err) {
        callback(err)
      }
      return undefined
    },
    'it works' (err, frule) {
      assert.ifError(err)
    },
    'and we convert it to JSON': {
      topic (frule) {
        const { callback } = this
        try {
          const actual = frule.toJSON()
          callback(null, actual)
        } catch (err) {
          callback(err)
        }
        return undefined
      },
      'it works' (err, actual) {
        assert.ifError(err)
        assert.isObject(actual)
      },
      'it looks correct' (err, actual) {
        assert.ifError(err)
        assert.deepEqual(actual, expected)
      },
      'and we convert it back to a rule': {
        topic (actual) {
          const { callback } = this
          try {
            const FuzzyRule = require('../lib/parser/fuzzy-rule')
            const newRule = FuzzyRule.fromJSON(actual)
            callback(null, newRule)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, newRule) {
          assert.ifError(err)
          assert.isObject(newRule)
        },
        'and we convert that to JSON': {
          topic (newRule) {
            const { callback } = this
            try {
              const newActual = newRule.toJSON()
              callback(null, newActual)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it works' (err, newActual) {
            assert.ifError(err)
            assert.isObject(newActual)
          },
          'it looks correct' (err, newActual) {
            assert.ifError(err)
            assert.deepEqual(newActual, expected)
          }
        }
      }
    }
  }
  return batch
}

vows
  .describe('fuzzy rules to JSON')
  .addBatch({
    'When we create a parser': {
      topic () {
        const { callback } = this
        try {
          const FuzzyRuleParser = require('../lib/fuzzy-rule-parser')
          const parser = new FuzzyRuleParser()
          callback(null, parser)
        } catch (err) {
          callback(err)
        }
        return undefined
      },
      'it works' (err, parser) {
        assert.ifError(err)
      },
      'and we parse an IF THEN rule':
        toFromJSON('IF temperature IS high THEN fanSpeed IS high', {
          type: 'if-then',
          antecedent: {
            type: 'is',
            dimension: 'temperature',
            set: 'high'
          },
          consequent: {
            type: 'is',
            dimension: 'fanSpeed',
            set: 'high'
          }
        }
        ),
      'and we parse an IF THEN rule with a NOT expression':
        toFromJSON('IF NOT (temperature IS cold) THEN (fanSpeed IS high)', {
          type: 'if-then',
          antecedent: {
            type: 'not',
            expression: {
              type: 'is',
              dimension: 'temperature',
              set: 'cold'
            }
          },
          consequent: {
            type: 'is',
            dimension: 'fanSpeed',
            set: 'high'
          }
        }
        ),
      'and we parse an IF THEN rule with a weight':
        toFromJSON('IF (temperature IS cold) THEN (fanSpeed IS slow) WITH 0.5', {
          type: 'if-then',
          antecedent: {
            type: 'is',
            dimension: 'temperature',
            set: 'cold'
          },
          consequent: {
            type: 'is',
            dimension: 'fanSpeed',
            set: 'slow'
          },
          weight: 0.5
        }
        ),
      'and we parse an IF THEN rule with an AND expression in the antecedent':
        toFromJSON('IF (temperature IS cold) AND (lubrication IS high) THEN fanSpeed IS slow', {
          type: 'if-then',
          antecedent: {
            type: 'and',
            left: {
              type: 'is',
              dimension: 'temperature',
              set: 'cold'
            },
            right: {
              type: 'is',
              dimension: 'lubrication',
              set: 'high'
            }
          },
          consequent: {
            type: 'is',
            dimension: 'fanSpeed',
            set: 'slow'
          }
        }
        ),
      'and we parse an IF THEN rule with a NOT AND expression in the antecedent':
        toFromJSON('IF NOT (temperature IS cold AND lubrication IS high) THEN fanSpeed IS slow', {
          type: 'if-then',
          antecedent: {
            type: 'not',
            expression: {
              type: 'and',
              left: {
                type: 'is',
                dimension: 'temperature',
                set: 'cold'
              },
              right: {
                type: 'is',
                dimension: 'lubrication',
                set: 'high'
              }
            }
          },
          consequent: {
            type: 'is',
            dimension: 'fanSpeed',
            set: 'slow'
          }
        }
        ),
      'and we parse an IF THEN rule with an OR expression in the antecedent':
        toFromJSON('IF (temperature IS cold) OR (battery IS low) THEN fanSpeed IS slow', {
          type: 'if-then',
          antecedent: {
            type: 'or',
            left: {
              type: 'is',
              dimension: 'temperature',
              set: 'cold'
            },
            right: {
              type: 'is',
              dimension: 'battery',
              set: 'low'
            }
          },
          consequent: {
            type: 'is',
            dimension: 'fanSpeed',
            set: 'slow'
          }
        }
        ),
      'and we parse an IF THEN rule with a NOT OR expression in the antecedent':
        toFromJSON('IF NOT (temperature IS cold OR battery IS low) THEN fanSpeed IS high', {
          type: 'if-then',
          antecedent: {
            type: 'not',
            expression: {
              type: 'or',
              left: {
                type: 'is',
                dimension: 'temperature',
                set: 'cold'
              },
              right: {
                type: 'is',
                dimension: 'battery',
                set: 'low'
              }
            }
          },
          consequent: {
            type: 'is',
            dimension: 'fanSpeed',
            set: 'high'
          }
        }
        ),
      'and we parse an IF THEN rule with an AND NOT expression in the antecedent':
        toFromJSON('IF temperature IS cold AND NOT lubrication IS low THEN fanSpeed IS high', {
          type: 'if-then',
          antecedent: {
            type: 'and',
            left: {
              type: 'is',
              dimension: 'temperature',
              set: 'cold'
            },
            right: {
              type: 'not',
              expression: {
                type: 'is',
                dimension: 'lubrication',
                set: 'low'
              }
            }
          },
          consequent: {
            type: 'is',
            dimension: 'fanSpeed',
            set: 'high'
          }
        }
        ),
      'and we parse an IF THEN rule with an OR NOT expression in the antecedent':
        toFromJSON('IF temperature IS cold OR NOT lubrication IS low THEN fanSpeed IS high', {
          type: 'if-then',
          antecedent: {
            type: 'or',
            left: {
              type: 'is',
              dimension: 'temperature',
              set: 'cold'
            },
            right: {
              type: 'not',
              expression: {
                type: 'is',
                dimension: 'lubrication',
                set: 'low'
              }
            }
          },
          consequent: {
            type: 'is',
            dimension: 'fanSpeed',
            set: 'high'
          }
        }
        ),
      'and we parse an INCREASES rule':
        toFromJSON('[number of likes] increases relevance', {
          type: 'increases',
          input: 'number of likes',
          output: 'relevance'
        }
        ),
      'and we parse an INCREASES rule with a weight':
        toFromJSON('[number of likes] increases relevance WITH 0.5', {
          type: 'increases',
          input: 'number of likes',
          output: 'relevance',
          weight: 0.5
        }
        ),
      'and we parse an INCREASES rule with multiple weights':
        toFromJSON('[number of likes] increases relevance WITH 0.25 0.5 1.0', {
          type: 'increases',
          input: 'number of likes',
          output: 'relevance',
          weight: [0.25, 0.5, 1.0]
        }),
      'and we parse a DECREASES rule':
        toFromJSON('[number of likes] decreases relevance', {
          type: 'decreases',
          input: 'number of likes',
          output: 'relevance'
        }
        ),
      'and we parse a DECREASES rule with a weight':
        toFromJSON('[number of likes] decreases relevance WITH 0.5', {
          type: 'decreases',
          input: 'number of likes',
          output: 'relevance',
          weight: 0.5
        }
        ),
      'and we parse a DECREASES rule with multiple weights':
        toFromJSON('[number of likes] decreases relevance WITH 0.25 0.5 1.0', {
          type: 'decreases',
          input: 'number of likes',
          output: 'relevance',
          weight: [0.25, 0.5, 1.0]
        })
    }})

  .export(module)

// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')

const polygonBatch = require('./polygonbatch')

vows
  .describe('Polygons from module test controller')
  .addBatch(polygonBatch([
    [[ 50, 0 ], [ 50, 0.6 ], [ 70, 0.6 ], [ 100, 0 ]],
    [[ 50, 0 ], [ 70, 0.4 ], [ 180, 0.4 ], [ 200, 0 ]]
  ]))
  .export(module)

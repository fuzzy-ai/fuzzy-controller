// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

vows
  .describe('variables with spaces in the name')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                'number of likes': {
                  'very low': [0, 1],
                  low: [1, 2, 3],
                  medium: [2, 3, 4],
                  high: [3, 4, 5],
                  'very high': [4, 5]
                },
                'number of retweets': {
                  'very low': [0, 1],
                  low: [1, 2, 3],
                  medium: [2, 3, 4],
                  high: [3, 4, 5],
                  'very high': [4, 5]
                },
                'age in hours': {
                  'very low': [0, 1],
                  low: [1, 2, 3],
                  medium: [2, 3, 4],
                  high: [3, 4, 5],
                  'very high': [4, 5]
                }
              },
              outputs: {
                'tweet relevance': {
                  'very low': [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  'very high': [80, 100]
                }
              },
              rules: [
                '[number of likes] INCREASES [tweet relevance]',
                '[number of retweets] INCREASES [tweet relevance]',
                'if [age in hours] is [very high] THEN [tweet relevance] is low'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we evaluate over the range': {
          topic (fc) {
            const { callback } = this
            const resultses = []
            let i = null
            let j = null
            try {
              let asc
              for (i = -1, asc = -1 <= 6; asc ? i <= 6 : i >= 6; asc ? i++ : i--) {
                let asc1
                for (j = -1, asc1 = -1 <= 6; asc1 ? j <= 6 : j >= 6; asc1 ? j++ : j--) {
                  for (let k = -1, asc2 = -1 <= 6; asc2 ? k <= 6 : k >= 6; asc2 ? k++ : k--) {
                    const results = fc.evaluate({'number of likes': i, 'number of retweets': j, 'age in hours': k})
                    resultses.push(results)
                  }
                }
              }
              callback(null, resultses)
            } catch (err) {
              callback(err, null)
            }
            return undefined
          },
          'it works' (err, resultses) {
            assert.ifError(err)
          }
        }
      }
    }}).export(module)

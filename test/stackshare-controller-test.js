/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const controllerBatch = require('./controllerbatch')

const ss = {
  rules: [
    'IF sharedStacks IS veryLow THEN relevance IS veryLow WITH 1.0',
    'IF sharedStacks IS low THEN relevance IS low WITH 1.0',
    'IF sharedStacks IS medium THEN relevance IS medium WITH 1.0',
    'IF sharedStacks IS high THEN relevance IS high WITH 1.0',
    'IF sharedStacks IS veryHigh THEN relevance IS veryHigh WITH 1.0',
    'IF sharedFans IS veryLow THEN relevance IS veryLow WITH 1.0',
    'IF sharedFans IS low THEN relevance IS low WITH 1.0',
    'IF sharedFans IS medium THEN relevance IS medium WITH 1.0',
    'IF sharedFans IS high THEN relevance IS high WITH 1.0',
    'IF sharedFans IS veryHigh THEN relevance IS veryHigh WITH 1.0',
    'IF integratedAB IS true THEN relevance IS veryHigh WITH 1.0',
    'IF integratedBA IS true THEN relevance IS veryHigh WITH 1.0'
  ],
  outputs: {
    relevance: {
      veryHigh: [
        75,
        100
      ],
      low: [
        0,
        25,
        50
      ],
      veryLow: [
        0,
        25
      ],
      high: [
        50,
        75,
        100
      ],
      medium: [
        25,
        50,
        75
      ]
    }
  },
  inputs: {
    integratedBA: {
      false: [
        0,
        0.5
      ],
      true: [
        0.5,
        1
      ]
    },
    integratedAB: {
      false: [
        0,
        0.5
      ],
      true: [
        0.5,
        1
      ]
    },
    sharedFans: {
      veryHigh: [
        2250,
        3000
      ],
      veryLow: [
        0,
        750
      ],
      low: [
        0,
        750,
        1500
      ],
      medium: [
        750,
        1500,
        2250
      ],
      high: [
        1500,
        2250,
        3000
      ]
    },
    sharedStacks: {
      veryLow: [
        0,
        750
      ],
      low: [
        0,
        750,
        1500
      ],
      high: [
        1500,
        2250,
        3000
      ],
      medium: [
        750,
        1500,
        2250
      ],
      veryHigh: [
        2250,
        3000
      ]
    }
  }
}

vows
  .describe('Problematic input from Stackshare')
  .addBatch(controllerBatch(
    ss,
    {sharedStacks: 800, integratedAB: 1, integratedBA: 1}, {
      'it looks right' (err, results, props) {
        assert.ifError(err)
        assert.isObject(results)
        assert.isNumber(results.relevance)
        assert.ok((results.relevance > 55))
      }
    }
  )).export(module)

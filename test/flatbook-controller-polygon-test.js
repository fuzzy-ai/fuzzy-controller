// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')

const polygonBatch = require('./polygonbatch')

vows
  .describe('Polygons from Flatbook controller')
  .addBatch(polygonBatch([
    [[50, 0],
      [50, 0.75],
      [65.625, 0.75],
      [81.25, 0.5],
      [112.5, 1],
      [162.5, 0.19999999999999996],
      [187.5, 0.2],
      [237.26452, 0.9962323200000001],
      [237.73548, 0.9962323200000001],
      [300, 0]],
    [[212.5, 0], [267.275, 0.626], [300, 0.626], [300, 0]]
  ]))
  .export(module)

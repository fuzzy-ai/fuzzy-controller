/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')
const debug = require('debug')('fuzzy-controller:controllerbatch')

const FuzzyController = require('../lib/fuzzy-controller')

const controllerBatch = function (controller, inputs, tests) {
  const batch = {
    'When we create the controller': {
      topic () {
        try {
          const fc = new FuzzyController(controller)
          this.callback(null, fc)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, fc) {
        assert.ifError(err)
        assert.isObject(fc)
      },
      'and we evaluate': {
        topic (fc) {
          const evaluate = function (inputSet) {
            fc.removeAllListeners()
            const props = {
              fuzzified: null,
              rules: [],
              inferred: null,
              clipped: {},
              combined: {},
              centroid: {}
            }
            fc.once('fuzzified', fuzzified => { props.fuzzified = fuzzified })
            fc.on('rule', (index, text) => props.rules.push(text))
            fc.once('inferred', inferred => { props.inferred = inferred })
            fc.on('clipped', (clipped, name) => { props.clipped[name] = clipped })
            fc.on('combined', (combined, name) => { props.combined[name] = combined })
            fc.on('centroid', (centroid, name) => { props.centroid[name] = centroid })
            const results = fc.evaluate(inputSet)
            fc.removeAllListeners()
            return [props, results]
          }

          try {
            let props, results
            if (_.isArray(inputs)) {
              const propses = []
              const resultses = []
              for (const input of Array.from(inputs)) {
                debug(input);
                [props, results] = Array.from(evaluate(input))
                debug(props)
                debug(results)
                propses.push(props)
                resultses.push(results)
              }
              this.callback(null, resultses, propses)
            } else {
              [props, results] = Array.from(evaluate(inputs))
              this.callback(null, results, props)
            }
          } catch (err) {
            debug(err)
            this.callback(err, null)
          }

          return undefined
        },

        'it works' (err, results, props) {
          assert.ifError(err)
        }
      }
    }
  }
  if (tests) {
    _.extend(batch['When we create the controller']['and we evaluate'], tests)
  }
  return batch
}

module.exports = controllerBatch

// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

vows
  .describe('Better handling for NaN')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'input1 INCREASES output1'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we evaluate with a NaN value': {
          topic (fc) {
            try {
              fc.evaluate({input1: NaN})
              this.callback(new Error('Unexpected success'))
            } catch (err) {
              this.callback(null, err.message)
            }
            return undefined
          },
          'it fails correctly' (err, message) {
            assert.ifError(err)
            assert.match(message, /input1 argument is not a number/)
          }
        }
      },
      'and we instantiate with NaN in the inputs': {
        topic (FuzzyController) {
          try {
            // eslint-disable-next-line no-unused-vars
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, NaN, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'input1 INCREASES output1'
              ]})
            this.callback(new Error('Unexpected success'))
          } catch (err) {
            this.callback(null, err.message)
          }
          return undefined
        },
        'it fails correctly' (err, message) {
          assert.ifError(err)
          assert.match(message, /must be 2-4 numbers/)
        }
      },
      'and we instantiate with NaN in the outputs': {
        topic (FuzzyController) {
          try {
            // eslint-disable-next-line no-unused-vars
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 20, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, NaN, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'input1 INCREASES output1'
              ]})
            this.callback(new Error('Unexpected success'))
          } catch (err) {
            this.callback(null, err.message)
          }
          return undefined
        },
        'it fails correctly' (err, message) {
          assert.ifError(err)
          assert.match(message, /must be 2-4 numbers/)
        }
      }
    }}).export(module)

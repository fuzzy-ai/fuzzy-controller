// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const testOutputSets = function (sets) {
  return {
    topic (FuzzyController) {
      try {
        const props = {
          inputs: {
            input1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          outputs: {
            output1: sets
          },
          rules: [
            'input1 INCREASES output1'
          ]
        }
        // eslint-disable-next-line no-unused-vars
        const fc = new FuzzyController(props)
        this.callback(new Error('Unexpected success'))
      } catch (err) {
        this.callback(null)
      }
      return undefined
    },
    'it fails correctly' (err) {
      assert.ifError(err)
    }
  }
}

const testInputSets = function (sets) {
  return {
    topic (FuzzyController) {
      try {
        const props = {
          inputs: {
            input1: sets
          },
          outputs: {
            output1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          rules: [
            'input1 INCREASES output1'
          ]
        }
        // eslint-disable-next-line no-unused-vars
        const fc = new FuzzyController(props)
        this.callback(new Error('Unexpected success'))
      } catch (err) {
        this.callback(null)
      }
      return undefined
    },
    'it fails correctly' (err) {
      assert.ifError(err)
    }
  }
}

vows
  .describe('Basic structure for FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we have an input with a vertical left slope':
        testInputSets({
          veryLow: [0, 0],
          low: [5, 45, 45],
          medium: [30, 50, 70],
          high: [55, 75, 95],
          veryHigh: [80, 100]}),
      'and we have an input with a vertical right slope':
        testInputSets({
          veryLow: [0, 20],
          low: [5, 45, 45],
          medium: [30, 50, 70],
          high: [55, 75, 95],
          veryHigh: [100, 100]}),
      'and we have an input with a zero-width triangle':
        testInputSets({
          veryLow: [0, 20],
          low: [5, 45, 45],
          medium: [50, 50, 50],
          high: [55, 75, 95],
          veryHigh: [100, 100]}),
      'and we have an input with a zero-width trapezoid':
        testInputSets({
          veryLow: [0, 20],
          low: [5, 45, 45],
          medium: [50, 50, 50, 50],
          high: [55, 75, 95],
          veryHigh: [100, 100]}),
      'and we have an output with a vertical left slope':
        testOutputSets({
          veryLow: [0, 0],
          low: [5, 45, 45],
          medium: [30, 50, 70],
          high: [55, 75, 95],
          veryHigh: [80, 100]}),
      'and we have an output with a vertical right slope':
        testOutputSets({
          veryLow: [0, 20],
          low: [5, 45, 45],
          medium: [30, 50, 70],
          high: [55, 75, 95],
          veryHigh: [100, 100]}),
      'and we have an output with a zero-width triangle':
        testOutputSets({
          veryLow: [0, 20],
          low: [5, 45, 45],
          medium: [50, 50, 50],
          high: [55, 75, 95],
          veryHigh: [100, 100]}),
      'and we have an output with a zero-width trapezoid':
        testOutputSets({
          veryLow: [0, 20],
          low: [5, 45, 45],
          medium: [50, 50, 50, 50],
          high: [55, 75, 95],
          veryHigh: [100, 100]})
    }})

  .export(module)

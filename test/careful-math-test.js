// careful-math-test.coffee -- Test that careful math module works
// Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const checkMethod = name =>
  function (err, obj) {
    assert.ifError(err)
    assert.isObject(obj)
    assert.isFunction(obj[name])
  }

const calcBatch = function (expected, calc, delta) {
  if (delta == null) { delta = 1e-6 }
  const batch = {
    topic (CarefulMath) {
      try {
        return this.callback(null, calc(CarefulMath))
      } catch (err) {
        return this.callback(err)
      }
    },
    'it works' (err, actual) {
      assert.ifError(err)
    },
    'it is correct' (err, actual) {
      assert.ifError(err)
      assert.inDelta(actual, expected, delta)
    }
  }
  return batch
}

vows.describe('Careful Math')
  .addBatch({
    'When we load the careful-math module': {
      topic () {
        this.callback(null, require('../lib/careful-math'))
        return undefined
      },
      'it works' (err, CarefulMath) {
        assert.ifError(err)
        assert.isObject(CarefulMath)
      },
      'it has an add() method': checkMethod('add'),
      'it has a sub() method': checkMethod('sub'),
      'it has a mul() method': checkMethod('mul'),
      'it has a div() method': checkMethod('div'),
      'and we add an integer and zero':
        calcBatch(1, m => m.add(1, 0)),
      'and we add an integer and an integer':
        calcBatch(3, m => m.add(1, 2)),
      'and we add three integers':
        calcBatch(6, m => m.add(1, 2, 3)),
      'and we subtract an integer from an integer':
        calcBatch(1, m => m.sub(3, 2)),
      'and we subtract zero from an integer':
        calcBatch(3, m => m.sub(3, 0)),
      'and we multiply two integers':
        calcBatch(12, m => m.mul(3, 4)),
      'and we multiply three integers':
        calcBatch(60, m => m.mul(3, 4, 5)),
      'and we multiply an integer by zero':
        calcBatch(0, m => m.mul(3, 0)),
      'and we multiply an integer by one':
        calcBatch(3, m => m.mul(3, 1)),
      'and we divide an integer by another integer':
        calcBatch(4, m => m.div(20, 5)),
      'and we divide an integer by one':
        calcBatch(5, m => m.div(5, 1))
    }}).export(module)

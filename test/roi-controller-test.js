// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')

const controllerBatch = require('./controllerbatch')

vows
  .describe('Problematic input from ROI')
  .addBatch(controllerBatch({
    inputs: {
      revenue: {
        veryHigh: [
          20000000000,
          25000000000
        ],
        high: [
          900000000,
          1000000000,
          20000000000,
          25000000000
        ],
        medium: [
          90000000,
          100000000,
          450000000,
          500000000
        ],
        low: [
          0,
          5000000,
          10000000,
          15000000
        ],
        mediumLow: [
          10000000,
          15000000,
          90000000,
          100000000
        ],
        mediumHigh: [
          450000000,
          500000000,
          900000000,
          1000000000
        ],
        veryLow: [
          0,
          5000000
        ]
      },
      numberOfEmployees: {
        veryHigh: [
          5000,
          5100
        ],
        high: [
          3000,
          3100,
          5000,
          5100
        ],
        low: [
          100,
          200,
          400,
          500
        ],
        medium: [
          900,
          1000,
          2000,
          2100
        ],
        veryLow: [
          0,
          200
        ],
        mediumHigh: [
          2000,
          2100,
          3000,
          3100
        ],
        mediumLow: [
          400,
          500,
          900,
          1000
        ]
      }
    },
    outputs: {
      roiIndex: {
        veryHigh: [
          85,
          100
        ],
        high: [
          65,
          75,
          85,
          95
        ],
        medium: [
          35,
          45,
          55,
          65
        ],
        low: [
          5,
          15,
          25,
          35
        ],
        mediumHigh: [
          55,
          65,
          75
        ],
        veryLow: [
          0,
          15
        ],
        mediumLow: [
          25,
          35,
          45
        ]
      }
    },
    rules: [
      'IF numberOfEmployees IS veryLow THEN roiIndex IS veryLow',
      'IF numberOfEmployees IS low THEN roiIndex IS low',
      'IF numberOfEmployees IS medium THEN roiIndex IS medium',
      'IF numberOfEmployees IS high THEN roiIndex IS high',
      'IF numberOfEmployees IS veryHigh THEN roiIndex IS veryHigh',
      'IF numberOfEmployees IS mediumLow THEN roiIndex IS mediumLow',
      'IF numberOfEmployees IS mediumHigh THEN roiIndex IS mediumHigh',
      'IF revenue IS veryLow THEN roiIndex IS veryLow',
      'IF revenue IS low THEN roiIndex IS low',
      'IF revenue IS mediumLow THEN roiIndex IS mediumLow',
      'IF revenue IS medium THEN roiIndex IS medium',
      'IF revenue IS mediumHigh THEN roiIndex IS mediumHigh',
      'IF revenue IS high THEN roiIndex IS high',
      'IF revenue IS veryHigh THEN roiIndex IS veryHigh'
    ]
  }, {
    revenue: 10000000000,
    numberOfEmployees: 2500
  }
  )).export(module)

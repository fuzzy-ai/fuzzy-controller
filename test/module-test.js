// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')
const semverRegex = require('semver-regex')

const LoopError = require('./loop-error')

vows
  .describe('Module structure for FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'it has a version stamp' (err, FuzzyController) {
        assert.ifError(err)
        assert.isString(FuzzyController.version)
        assert.match(FuzzyController.version, semverRegex())
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                temperature: {
                  cold: [50, 75],
                  normal: [50, 75, 85, 100],
                  hot: [85, 100]
                }
              },
              outputs: {
                fanSpeed: {
                  slow: [50, 100],
                  normal: [50, 100, 150, 200],
                  fast: [150, 200]
                }
              },
              rules: [
                'IF temperature IS cold THEN fanSpeed IS slow',
                'IF temperature IS normal THEN fanSpeed IS normal',
                'IF temperature IS hot THEN fanSpeed IS fast'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'it has an evaluate method' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
          assert.isFunction(fc.evaluate)
        },
        'and we evaluate': {
          topic (fc) {
            let f = null
            let o = null
            const c = {}
            let crisp = null
            const { callback } = this
            fc.once('fuzzified', fuzzified => { f = fuzzified })
            fc.once('inferred', output => { o = output })
            fc.once('clipped', (clipped, name) => { c[name] = clipped })
            try {
              crisp = fc.evaluate({temperature: 60})
              callback(null, f, o, c, crisp)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'fuzzified values work' (err, fuzzified, output, clipped, crisp) {
            assert.ifError(err)
            assert.isObject(fuzzified)
            assert.isObject(fuzzified['temperature'])
            assert.isNumber(fuzzified['temperature']['cold'])
            assert.equal(fuzzified['temperature']['cold'], 0.6)
            assert.isNumber(fuzzified['temperature']['normal'])
            assert.equal(fuzzified['temperature']['normal'], 0.4)
            assert.isNumber(fuzzified['temperature']['hot'])
            assert.equal(fuzzified['temperature']['hot'], 0)
          },
          'output values work' (err, fuzzified, output, clipped, crisp) {
            assert.ifError(err)
            assert.isObject(output)
            assert.isObject(output['fanSpeed'])
            assert.isNumber(output['fanSpeed']['slow'])
            assert.equal(output['fanSpeed']['slow'], 0.6)
            assert.isNumber(output['fanSpeed']['normal'])
            assert.equal(output['fanSpeed']['normal'], 0.4)
            assert.isNumber(output['fanSpeed']['fast'])
            assert.equal(output['fanSpeed']['fast'], 0)
          },
          'clipped values work' (err, fuzzified, output, clipped, crisp) {
            assert.ifError(err)
            assert.isObject(clipped)
            return _.each(clipped, (shapes, name) => {
              assert.isObject(shapes)
              return _.each(shapes, (shape, name) => {
                assert.isArray(shape)
                assert.lengthOf(shape, 4)
                return _.each(shape, (pt) => {
                  assert.isArray(pt)
                  assert.isNumber(pt[0])
                  assert.isNumber(pt[1])
                })
              })
            })
          },
          'crisp values work' (err, fuzzified, output, clipped, crisp) {
            assert.ifError(err)
            assert.isObject(crisp)
            assert.isNumber(crisp['fanSpeed'])
            assert.inDelta(crisp['fanSpeed'], 93, 2)
          },
          'and we evaluate a lot': {
            topic (fuzzified, output, clipped, crisp, fc) {
              const { callback } = this
              const resultses = []
              let f = null
              let o = null
              let c = {}
              try {
                _.each(__range__(40, 110, true), (num) => {
                  f = (o = null)
                  c = {}
                  fc.once('fuzzified', fuzzified => { f = fuzzified })
                  fc.once('inferred', output => { o = output })
                  fc.once('clipped', (clipped, name) => { c[name] = clipped })
                  return resultses.push(fc.evaluate({temperature: num}))
                })
                callback(null, resultses)
              } catch (err) {
                callback(new LoopError(err, null, f, o, c), null)
              }
              return undefined
            },
            'it works' (err, resultses) {
              assert.ifError(err)
              assert.isArray(resultses)
            },
            'values look correct' (err, resultses) {
              assert.ifError(err)
              assert.isArray(resultses)
              return _.each(resultses, (results) => {
                assert.isObject(results)
                assert.isNumber(results.fanSpeed)
                assert.greater(results.fanSpeed, 49)
                assert.lesser(results.fanSpeed, 201)
              })
            }
          }
        }
      }
    }}).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}

/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')
const debug = require('debug')('fuzzy-controller:slope-when-value-is-null-test')
const BOUNDARY = 100

const slopeTest = function (i, value, args, expected, delta) {
  if (delta == null) { delta = 0.01 }
  const batch = {
    topic (fc) {
      try {
        const slope = fc.slopeAt(i, value, args)
        this.callback(null, slope.y)
      } catch (err) {
        debug(`GOT AN ERROR: ${err}`)
        this.callback(err)
      }
      return undefined
    },
    'it looks correct' (err, slope) {
      debug({err, i, value, args, expected, delta, slope})
      assert.ifError(err)
      if (_.isNull(expected)) {
        assert.isNull(slope)
      } else if (_.isFinite(expected)) {
        assert.isNumber(slope)
        assert.inDelta(slope, expected, delta)
      }
    }
  }
  return batch
}

vows
  .describe('Slope when value is null or near null')
  .addBatch({
    'When we create a controller with some inputs that generate null output': {
      topic () {
        const initial = {
          inputs: {
            x: {
              veryLow: [0, BOUNDARY / 6],
              low: [0, BOUNDARY / 6, BOUNDARY / 3],
              high: [(2 * BOUNDARY) / 3, (5 * BOUNDARY) / 6, BOUNDARY],
              veryHigh: [(5 * BOUNDARY) / 6, BOUNDARY]
            }
          },
          outputs: {
            y: {
              veryLow: [0, BOUNDARY / 6],
              low: [0, BOUNDARY / 6, BOUNDARY / 3],
              high: [(2 * BOUNDARY) / 3, (5 * BOUNDARY) / 6, BOUNDARY],
              veryHigh: [(5 * BOUNDARY) / 6, BOUNDARY]
            }
          },
          rules: [
            'x INCREASES y WITH 0.5'
          ]
        }
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          const fc = new FuzzyController(initial)
          this.callback(null, fc)
        } catch (err) {
          this.callback(err)
        }
        return undefined
      },
      'it works' (err, fc) {
        assert.ifError(err)
      },
      'and we test that it generates null for the dead area': {
        topic (fc) {
          try {
            const results = fc.evaluate({x: BOUNDARY / 2})
            this.callback(null, results)
          } catch (err) {
            this.callback(err)
          }
          return undefined
        },
        'it works' (err, results) {
          assert.ifError(err)
          assert.isObject(results)
          assert.isNull(results.y)
        }
      },
      'and we test at a point at the edge of a null area':
        // FIXME: hacked in the value; not sure what it should be
        slopeTest(0, (5 * BOUNDARY) / 6, {x: (2 * BOUNDARY) / 3}, 0),
      'and we test at a point at the other edge of a null area':
        // FIXME: hacked in the value; not sure what it should be
        slopeTest(3, BOUNDARY / 6, {x: BOUNDARY / 3}, 0),
      'and we test at a point where all values are null':
        slopeTest(0, (5 * BOUNDARY) / 6, {x: BOUNDARY / 2}, null)
    }}).export(module)

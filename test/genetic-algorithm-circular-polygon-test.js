// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')

const polygonBatch = require('./polygonbatch')

vows
  .describe('Polygons from genetic algorithm controller')
  .addBatch(polygonBatch([
    [[1.4730775096331663, 0], [1.6738984865370372, 0.3349002040221804], [2.5122641269455337, 0.3349002040221804], [2.73358855696546, 0]],
    [[1.5170842886498028, 0], [1.7606661771058019, 0.5393655917409852], [2.176718668290387, 0.5393655917409852], [2.420300556746386, 0]]
  ]))
  .export(module)

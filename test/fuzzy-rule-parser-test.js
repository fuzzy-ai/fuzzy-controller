// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const _ = require('lodash')
const vows = require('perjury')
const { assert } = vows

const parseSucceed = function (src, weight) {
  if (weight == null) { weight = 1.0 }
  const batch = {
    topic (FuzzyRuleParser) {
      const { callback } = this
      const parser = new FuzzyRuleParser()
      try {
        const rule = parser.parse(src)
        callback(null, rule)
      } catch (err) {
        callback(err)
      }
      return undefined
    },
    'it works' (err, rule) {
      assert.ifError(err)
      assert.isObject(rule)
    },
    'and we examine the resulting rule': {
      topic (rule) {
        return rule
      },
      'the rule has an apply method' (err, rule) {
        assert.ifError(err)
        assert.isFunction(rule.apply)
      },
      'the rule has an antecedent' (err, rule) {
        assert.ifError(err)
        assert.isObject(rule.antecedent)
      },
      'the rule has a consequent' (err, rule) {
        assert.ifError(err)
        assert.isObject(rule.consequent)
      },
      'the rule has the right weight' (err, rule) {
        assert.ifError(err)
        assert.isNumber(rule.weight)
        assert.inDelta(rule.weight, weight, 0.001)
      }
    },
    'and we convert the resulting rule to text': {
      topic (rule) {
        const { callback } = this
        try {
          const str = rule.toString()
          callback(null, str)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, str) {
        assert.ifError(err)
        assert.isString(str)
      },
      'and we parse the resulting string again': {
        topic (str, rule, FuzzyRuleParser) {
          const { callback } = this
          const parser = new FuzzyRuleParser()
          try {
            const dupe = parser.parse(str)
            callback(null, rule, dupe)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, rule, dupe) {
          assert.ifError(err)
          assert.isObject(rule)
          assert.isObject(dupe)
        },
        'the results look correct' (err, rule, dupe) {
          assert.ifError(err)
          assert.deepEqual(dupe, rule)
        }
      }
    }
  }

  return batch
}

const parseMappedSucceed = function (src, weight) {
  if (weight == null) { weight = 1.0 }
  const batch = {
    topic (FuzzyRuleParser) {
      const { callback } = this
      const parser = new FuzzyRuleParser()
      try {
        const rule = parser.parse(src)
        callback(null, rule)
      } catch (err) {
        callback(err)
      }
      return undefined
    },
    'it works' (err, rule) {
      assert.ifError(err)
      assert.isObject(rule)
    },
    'and we examine the resulting rule': {
      topic (rule) {
        return rule
      },
      'the rule has an apply method' (err, rule) {
        assert.ifError(err)
        assert.isFunction(rule.apply)
      },
      'the rule has an input' (err, rule) {
        assert.ifError(err)
        assert.isString(rule.input)
      },
      'the rule has an output' (err, rule) {
        assert.ifError(err)
        assert.isString(rule.output)
      },
      'the rule has the right weight' (err, rule) {
        assert.ifError(err)
        if (_.isFinite(weight)) {
          assert.isNumber(rule.weight)
          assert.inDelta(rule.weight, weight, 0.001)
        } else if (_.isArray(weight)) {
          assert.isArray(rule.weight)
          return Array.from(weight).map((w, i) =>
            assert.inDelta(rule.weight[i], w, 0.001))
        }
      }
    }
  }
  return batch
}

const parseFail = function (src) {
  const batch = {
    topic (FuzzyRuleParser) {
      const { callback } = this
      const parser = new FuzzyRuleParser()
      try {
        parser.parse(src)
        callback(new Error('Unexpected success'))
      } catch (err) {
        callback(null, err)
      }
      return undefined
    },
    'the parser throws an error' (err, thrown) {
      assert.ifError(err)
    },
    'the thrown error looks correct' (err, thrown) {
      assert.ifError(err)
      assert.isObject(thrown)
      assert.instanceOf(thrown, require('../lib/fuzzy-controller').FuzzyRuleParserError)
      assert.equal(thrown.source, src)
    }
  }
  return batch
}

vows
  .describe('Fuzzy rule parser')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyRuleParser = require('../lib/fuzzy-rule-parser')
          this.callback(null, FuzzyRuleParser)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyRuleParser) {
        assert.ifError(err)
        assert.isFunction(FuzzyRuleParser)
      },
      'and we instantiate': {
        topic (FuzzyRuleParser) {
          try {
            const parser = new FuzzyRuleParser()
            this.callback(null, parser)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, parser) {
          assert.ifError(err)
          assert.isObject(parser)
        },
        'it has a parse method' (err, parser) {
          assert.ifError(err)
          assert.isObject(parser)
          assert.isFunction(parser.parse)
        }
      },
      'and we tokenize a string': {
        topic (FuzzyRuleParser) {
          const parser = new FuzzyRuleParser()
          const src = 'IF (temperature IS cold) OR (battery IS low) THEN fanSpeed IS slow'
          return parser.tokenize(src)
        },
        'tokens look correct' (err, tokens) {
          assert.ifError(err)
          assert.isArray(tokens)
          assert.lengthOf(tokens, 17)
          assert.deepEqual(tokens, [
            {type: 100, text: 'IF'},
            {type: 105, text: '('},
            {type: 200, text: 'temperature'},
            {type: 102, text: 'IS'},
            {type: 200, text: 'cold'},
            {type: 106, text: ')'},
            {type: 104, text: 'OR'},
            {type: 105, text: '('},
            {type: 200, text: 'battery'},
            {type: 102, text: 'IS'},
            {type: 200, text: 'low'},
            {type: 106, text: ')'},
            {type: 101, text: 'THEN'},
            {type: 200, text: 'fanSpeed'},
            {type: 102, text: 'IS'},
            {type: 200, text: 'slow'},
            {type: 400, text: '<EOL>'}
          ])
        }
      },
      'and we parse a string without an initial IF':
        parseFail('WHEN (temperature IS cold) THEN fanSpeed IS slow'),
      'and we parse a string with a bad antecedent expression':
        parseFail('IF (temperature > 95) THEN fanSpeed IS slow'),
      'and we parse a string with a missing THEN keyword':
        parseFail('IF (temperature IS cold) fanSpeed IS slow'),
      'and we parse a string with a bad consequent':
        parseFail('IF (temperature IS cold) THEN (fanSpeed => slow)'),
      'and we parse a string with a bad identifier':
        parseFail('IF (temperature IS cold) THEN (fanSpeed IS AND)'),
      'and we parse a string with a mismatched parenthesis':
        parseFail('IF (temperature IS cold AND (lubrication IS high) THEN fanSpeed IS slow'),
      'and we parse a string with a simple antecedent':
        parseSucceed('IF (temperature IS cold) THEN (fanSpeed IS slow)'),
      'and we parse a string with a NOT antecedent':
        parseSucceed('IF NOT (temperature IS cold) THEN (fanSpeed IS high)'),
      'and we parse a string with a NOT antecedent with no parentheses':
        parseSucceed('IF NOT temperature IS cold THEN (fanSpeed IS high)'),
      'and we parse a string with a weight':
        parseSucceed('IF (temperature IS cold) THEN (fanSpeed IS slow) WITH 0.5', 0.5),
      'and we parse with an AND expression':
        parseSucceed('IF (temperature IS cold) AND (lubrication IS high) THEN fanSpeed IS slow'),
      'and we parse with an AND expression and no parentheses':
        parseSucceed('IF temperature IS cold AND lubrication IS high THEN fanSpeed IS slow'),
      'and we parse with an AND expression and surrounding parentheses':
        parseSucceed('IF (temperature IS cold AND lubrication IS high) THEN fanSpeed IS slow'),
      'and we parse with a NOT AND expression':
        parseSucceed('IF NOT (temperature IS cold AND lubrication IS high) THEN fanSpeed IS slow'),
      'and we parse with a NOT AND expression without parentheses':
        parseSucceed('IF NOT temperature IS cold AND lubrication IS high THEN fanSpeed IS slow'),
      'and we parse with an OR expression':
        parseSucceed('IF (temperature IS cold) OR (battery IS low) THEN fanSpeed IS slow'),
      'and we parse with an OR expression and no parentheses':
        parseSucceed('IF temperature IS cold OR battery IS low THEN fanSpeed IS slow'),
      'and we parse with an OR expression and surrounding parentheses':
        parseSucceed('IF (temperature IS cold OR battery IS low) THEN fanSpeed IS slow'),
      'and we parse with a NOT OR expression':
        parseSucceed('IF NOT (temperature IS cold OR battery IS low) THEN fanSpeed IS high'),
      'and we parse with a NOT OR expression without parentheses':
        parseSucceed('IF NOT temperature IS cold OR battery IS low THEN fanSpeed IS high'),
      'and we parse with an AND NOT expression':
        parseSucceed('IF temperature IS cold AND NOT lubrication IS low THEN fanSpeed IS high'),
      'and we parse with an OR NOT expression':
        parseSucceed('IF temperature IS cold OR NOT lubrication IS low THEN fanSpeed IS high'),
      'and we parse a long complex string':
        parseSucceed(`IF (typeIsPassword IS true)
    AND ((idMatchesPassword IS true) OR (nameMatchesPassword IS true))
    AND (nameMatchesRejectList IS false)
    AND (idMatchesRejectList IS false)
THEN confidence IS stronglyPositive`
        ),
      'and we parse with a weight lower than 0.0':
        parseFail('IF (temperature IS cold) THEN (fanSpeed IS slow) WITH -0.5'),
      'and we parse with a weight higher than 1.0':
        parseFail('IF (temperature IS cold) THEN (fanSpeed IS slow) WITH 1.7'),
      'and we parse an INCREASES rule':
        parseMappedSucceed('temperature INCREASES fanSpeed'),
      'and we parse a DECREASES rule':
        parseMappedSucceed('pressure DECREASES fanSpeed'),
      'and we parse a rule with another verb in the middle':
        parseFail('lubrication INFLUENCES fanSpeed'),
      'and we parse an INCREASES rule with an input with spaces in it':
        parseMappedSucceed('[ambient temperature] INCREASES fanSpeed'),
      'and we parse an INCREASES rule with an output with spaces in it':
        parseMappedSucceed('temperature INCREASES [fan speed]'),
      'and we parse an INCREASES rule with both input and output with spaces in it':
        parseMappedSucceed('[ambient temperature] INCREASES [fan speed]'),
      'and we parse a DECREASES rule with an input with spaces in it':
        parseMappedSucceed('[ambient temperature] DECREASES fanSpeed'),
      'and we parse a DECREASES rule with an output with spaces in it':
        parseMappedSucceed('temperature DECREASES [fan speed]'),
      'and we parse a DECREASES rule with both input and output with spaces in it':
        parseMappedSucceed('[ambient temperature] DECREASES [fan speed]'),
      'and we parse an if-then rule with input with spaces in it':
        parseSucceed('IF [ambient temperature] IS high THEN fanSpeed IS low'),
      'and we parse an if-then rule with input with spaces in it and parentheses':
        parseSucceed('IF ([ambient temperature] IS high AND pressure IS low) THEN fanSpeed IS low'),
      'and we parse an if-then rule with output with spaces in it':
        parseSucceed('IF temperature IS high THEN [fan speed] IS low'),
      'and we parse an if-then rule with input and output with spaces in it':
        parseSucceed('IF [ambient temperature] IS high THEN [fan speed] IS low'),
      'and we parse an INCREASES rule with an input with spaces in it with unclosed brackets':
        parseFail('[ambient temperature INCREASES fanSpeed'),
      'and we parse an INCREASES rule with an input with spaces in it with mismatched brackets':
        parseFail('ambient temperature] INCREASES fanSpeed'),
      'and we parse an INCREASES rule with an ouptut with spaces in it with mismatched brackets':
        parseFail('temperature INCREASES fan speed]'),
      'and we parse an INCREASES rule with an ouptut with spaces in it with unclosed brackets':
        parseFail('temperature INCREASES [fan speed'),
      'and we parse an INCREASES rule with a weight':
        parseMappedSucceed('temperature INCREASES fanSpeed WITH 0.5', 0.5),
      'and we parse a DECREASES rule with a weight':
        parseMappedSucceed('temperature DECREASES fanSpeed WITH 0.5', 0.5),
      'and we parse an INCREASES rule with multiple weights':
        parseMappedSucceed('temperature INCREASES fanSpeed WITH 0.25 0.5 1.0', [0.25, 0.5, 1.0]),
      'and we parse a DECREASES rule with multiple weights':
        parseMappedSucceed('temperature DECREASES fanSpeed WITH 0.25 0.5 1.0', [0.25, 0.5, 1.0]),
      'and we parse a rule with an exponentially small weight':
        parseSucceed('IF [ambient temperature] IS high THEN [fan speed] IS low WITH 1e-7', 1e-7)
    }}).export(module)

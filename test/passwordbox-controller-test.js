/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const controllerBatch = require('./controllerbatch')

vows
  .describe('Better performance with a downward slope')
  .addBatch(controllerBatch({
    inputs: {
      typeIsText: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      idMatchesHitlist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      nameMatchesHitlist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      placeholderMatchesHitlist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      titleMatchesHitlist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      labelAttributeMatchesHitlist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      ariaLabelMatchesHitlist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      idMatchesBlocklist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      nameMatchesBlocklist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      placeholderMatchesBlocklist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      titleMatchesBlocklist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      labelAttributeMatchesBlocklist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      ariaLabelMatchesBlocklist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      labelMatchesLabelHitlist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      labelMatchesLabelBlocklist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      containerMatchesLabelHitlist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      },
      containerMatchesLabelBlocklist: {
        false: [
          -1,
          0,
          1
        ],
        true: [
          0,
          1,
          2
        ]
      }
    },
    outputs: {
      confidence: {
        overNegative: [
          -150,
          -100
        ],
        stronglyNegative: [
          -150,
          -100,
          -50
        ],
        negative: [
          -100,
          -50,
          0
        ],
        neutral: [
          -50,
          0,
          50
        ],
        positive: [
          0,
          50,
          100
        ],
        stronglyPositive: [
          50,
          100,
          150
        ],
        overPositive: [
          100,
          150
        ]
      }
    },
    rules: [
      'IF typeIsText IS true AND (idMatchesHitlist IS true OR nameMatchesHitlist IS true OR placeholderMatchesHitlist IS true OR titleMatchesHitlist IS true OR labelAttributeMatchesHitlist IS true OR ariaLabelMatchesHitlist IS true) AND not (idMatchesBlocklist IS true OR nameMatchesBlocklist IS true OR placeholderMatchesBlocklist IS true OR titleMatchesBlocklist IS true OR labelAttributeMatchesBlocklist IS true OR ariaLabelMatchesBlocklist IS true)  THEN confidence IS stronglyPositive',
      'IF typeIsText IS true AND labelMatchesLabelHitlist IS true AND labelMatchesLabelBlocklist IS false THEN confidence IS positive',
      'IF typeIsText IS true AND containerMatchesLabelHitlist IS true AND containerMatchesLabelBlocklist IS false THEN confidence IS positive'
    ]
  }, {
    typeIsText: 1,
    idMatchesHitlist: 1,
    nameMatchesHitlist: 1,
    placeholderMatchesHitlist: 0,
    titleMatchesHitlist: 0,
    labelAttributeMatchesHitlist: 0,
    ariaLabelMatchesHitlist: 0,
    idMatchesBlocklist: 0,
    nameMatchesBlocklist: 0,
    placeholderMatchesBlocklist: 0,
    titleMatchesBlocklist: 0,
    labelAttributeMatchesBlocklist: 0,
    ariaLabelMatchesBlocklist: 0,
    labelMatchesLabelHitlist: 0,
    labelMatchesLabelBlocklist: 0,
    containerMatchesLabelHitlist: 0,
    containerMatchesLabelBlocklist: 0
  }
    , {'the confidence is above zero' (err, results, props) {
    assert.ifError(err)
    assert.greater(results.confidence, 0)
  }
  }
  )).export(module)

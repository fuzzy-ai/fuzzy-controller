// Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const debug = require('debug')('fuzzy-controller:doutput-dinput-test')

vows
  .describe('Slope w/r/t input')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                },
                input2: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'input1 INCREASES output1'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'it has an inputSlope() method' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
          assert.isFunction(fc.inputSlope)
        },
        'and we check the slope at a point in input space': {
          topic (fc) {
            const { callback } = this
            try {
              const vals = __range__(-10, 110, true)
              const results = vals.map((val) => {
                const inputs = {input1: val, input2: 50}
                return fc.inputSlope('input1', 'output1', inputs)
              })
              callback(null, results)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it works' (err, results) {
            assert.ifError(err)
            assert.isArray(results)
            return debug(JSON.stringify(results))
          }
        }
      }
    }}).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}

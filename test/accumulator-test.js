// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const async = require('async')

const FuzzyController = require('../lib/fuzzy-controller')

const evaluate = function (fc, inputs, callback) {
  const props = {
    fuzzified: null,
    rules: [],
    inferred: null,
    clipped: {},
    combined: {},
    centroid: {}
  }
  try {
    fc.removeAllListeners()
    fc.once('fuzzified', (fuzzified) => {
      props.fuzzified = fuzzified
    })
    fc.on('rule', (index, text) => props.rules.push(text))
    fc.once('inferred', inferred => { props.inferred = inferred })
    fc.on('clipped', (clipped, name) => { props.clipped[name] = clipped })
    fc.on('combined', (combined, name) => { props.combined[name] = combined })
    fc.on('centroid', (centroid, name) => { props.centroid[name] = centroid })
    const results = fc.evaluate(inputs)
    fc.removeAllListeners()
    callback(null, results, props)
  } catch (err) {
    callback(err, null, null)
  }
}

const controller = {
  name: 'Accumulator test controller',
  inputs: {
    input1: {
      veryLow: [0, 1, 2],
      low: [1, 2, 3],
      medium: [2, 3, 4],
      high: [3, 4, 5],
      veryHigh: [4, 5, 6]
    },
    input2: {
      veryLow: [0, 1, 2],
      low: [1, 2, 3],
      medium: [2, 3, 4],
      high: [3, 4, 5],
      veryHigh: [4, 5, 6]
    },
    input3: {
      veryLow: [0, 1, 2],
      low: [1, 2, 3],
      medium: [2, 3, 4],
      high: [3, 4, 5],
      veryHigh: [4, 5, 6]
    }
  },
  outputs: {
    output1: {
      veryLow: [0, 1, 2],
      low: [1, 2, 3],
      medium: [2, 3, 4],
      high: [3, 4, 5],
      veryHigh: [4, 5, 6]
    }
  },
  rules: [
    'IF input1 IS veryLow THEN output1 IS veryLow',
    'IF input1 IS low THEN output1 IS low',
    'IF input1 IS medium THEN output1 IS medium',
    'IF input1 IS high THEN output1 IS high',
    'IF input1 IS veryHigh THEN output1 IS veryHigh',
    'IF input2 IS veryLow THEN output1 IS veryLow',
    'IF input2 IS low THEN output1 IS low',
    'IF input2 IS medium THEN output1 IS medium',
    'IF input2 IS high THEN output1 IS high',
    'IF input2 IS veryHigh THEN output1 IS veryHigh',
    'IF input3 IS veryLow THEN output1 IS veryLow',
    'IF input3 IS low THEN output1 IS low',
    'IF input3 IS medium THEN output1 IS medium',
    'IF input3 IS high THEN output1 IS high',
    'IF input3 IS veryHigh THEN output1 IS veryHigh'
  ]
}

vows
  .describe('Accumulator gives weight to multiple rules')
  .addBatch({
    'When we create the controller': {
      topic () {
        try {
          const fc = new FuzzyController(controller)
          this.callback(null, fc)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, fc) {
        assert.ifError(err)
        assert.isObject(fc)
      },
      'and we evaluate': {
        topic (fc) {
          let propsB, resultsB
          const { callback } = this
          const inputsA = {
            input1: 1,
            input2: 1,
            input3: 5
          }
          const inputsB = {
            input1: 1,
            input2: 2,
            input3: 5
          }
          let resultsA = (resultsB = null)
          let propsA = (propsB = null)
          async.waterfall([
            callback => evaluate(fc, inputsA, callback),
            function (results, props, callback) {
              resultsA = results
              propsA = props
              return evaluate(fc, inputsB, callback)
            }
          ], (err, results, props) => {
            if (err) {
              callback(err)
            } else {
              resultsB = results
              propsB = props
              callback(null, resultsA, propsA, resultsB, propsB)
            }
          })
          return undefined
        },
        'it works' (err, resultsA, propsA, resultsB, propsB) {
          assert.ifError(err)
        },
        'B > A' (err, resultsA, propsA, resultsB, propsB) {
          assert.ifError(err)
          assert.greater(resultsB.output1, resultsA.output1)
        }
      }
    }}).export(module)

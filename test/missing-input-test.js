// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const LoopError = require('./loop-error')

vows
  .describe('Missing input for FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                },
                input2: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow',
                'IF input1 IS low THEN output1 IS low',
                'IF input1 IS medium THEN output1 IS medium',
                'IF input1 IS high THEN output1 IS high',
                'IF input1 IS veryHigh THEN output1 IS veryHigh',
                'IF input2 IS veryLow THEN output1 IS veryLow',
                'IF input2 IS low THEN output1 IS low',
                'IF input2 IS medium THEN output1 IS medium',
                'IF input2 IS high THEN output1 IS high',
                'IF input2 IS veryHigh THEN output1 IS veryHigh'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we evaluate over the range': {
          topic (fc) {
            const { callback } = this
            let results = null
            let f = null
            let o = null
            let c = {}
            let i = null
            try {
              i = 37
              f = (o = null)
              c = {}
              fc.once('fuzzified', fuzzified => { f = fuzzified })
              fc.once('inferred', output => { o = output })
              fc.once('clipped', (clipped, name) => { c[name] = clipped })
              results = fc.evaluate({input1: i})
              callback(null, results)
            } catch (err) {
              callback(new LoopError(err, i, f, o, c), null)
            }
            return undefined
          },
          'it works' (err, results) {
            assert.ifError(err)
          }
        }
      }
    }}).export(module)

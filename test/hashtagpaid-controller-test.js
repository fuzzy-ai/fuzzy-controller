/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const controllerBatch = require('./controllerbatch')

vows
  .describe('Problematic input from #paid')
  .addBatch(controllerBatch({
    name: 'Instagram Relevance',
    inputs: {
      likesFollowersRatio: {
        veryLow: [
          15.184113562102013,
          32.22059898
        ],
        low: [
          19.06217693,
          36.051250633789785,
          62.73091684
        ],
        medium: [
          42.28764837,
          68.77559196254698,
          105.6666667
        ],
        high: [
          77.29605887,
          113.42326449168308,
          197.4080638
        ],
        veryHigh: [
          131.2044271,
          207.8726183320185
        ]
      },
      commentsFollowersRatio: {
        veryLow: [
          975.9559504682203,
          3030.637584
        ],
        low: [
          1427.671233,
          3489.336199407174,
          6813.518519
        ],
        medium: [
          4182.746479,
          7344.357169390971,
          14603.6129
        ],
        high: [
          8566.559633,
          15809.669491950228,
          30995
        ],
        veryHigh: [
          18934.99086,
          33455.00106032538
        ]
      },
      hashtagMatches: {
        veryLow: [
          0,
          1
        ],
        low: [
          0.5,
          1,
          2
        ],
        medium: [
          1,
          2,
          3
        ],
        high: [
          2,
          3,
          4
        ],
        veryHigh: [
          3,
          4
        ]
      },
      influencerLocation: {
        false: [
          0,
          0.5
        ],
        true: [
          0.5,
          1
        ]
      },
      audienceLocation: {
        false: [
          0,
          0.5
        ],
        true: [
          0.5,
          1
        ]
      },
      ageDemographics: {
        veryLow: [
          31.88472352339591,
          40.5
        ],
        low: [
          33.8,
          42.23358243426814,
          48.5
        ],
        medium: [
          43.6,
          49.91772740435593,
          54
        ],
        high: [
          50.8,
          54.84528720042558,
          61.9
        ],
        veryHigh: [
          56.4,
          63.23324921949143
        ]
      },
      genderDemographics: {
        veryLow: [
          11.841504852489388,
          21
        ],
        low: [
          13,
          22.455473308304644,
          34
        ],
        medium: [
          24,
          35.54477683156772,
          48
        ],
        high: [
          38,
          50.06076622699134,
          67
        ],
        veryHigh: [
          53,
          70.38430509150413
        ]
      },
      numberOfBids: {
        veryLow: [
          0,
          3
        ],
        low: [
          0.03804419406205953,
          3.5069446772241224,
          8
        ],
        medium: [
          4,
          8.087229742717202,
          14
        ],
        high: [
          9,
          14.903165446650158,
          27
        ],
        veryHigh: [
          16,
          29.1377165058841
        ]
      },
      numberOfPastCampaigns: {
        veryLow: [
          0,
          1
        ],
        low: [
          0.0001231924141766305,
          1.0056588952800232,
          2
        ],
        medium: [
          1,
          2.167516733608414,
          5
        ],
        high: [
          2,
          5.6282831639357145,
          12
        ],
        veryHigh: [
          6,
          13.016824099837986
        ]
      }
    },
    outputs: {
      relevance: {
        veryLow: [
          0,
          25
        ],
        low: [
          0,
          25,
          50
        ],
        medium: [
          25,
          50,
          75
        ],
        high: [
          50,
          75,
          100
        ],
        veryHigh: [
          75,
          100
        ]
      }
    },
    rules: [
      `\
IF likesFollowersRatio IS veryLow
THEN relevance IS veryHigh WITH 1.0\
`,
      `\
IF likesFollowersRatio IS low
THEN relevance IS high WITH 1.0\
`,
      `\
IF likesFollowersRatio IS medium
THEN relevance IS medium WITH 1.0\
`,
      `\
IF likesFollowersRatio IS high
THEN relevance IS low WITH 1.0\
`,
      `\
IF likesFollowersRatio IS veryHigh
THEN relevance IS veryLow WITH 1.0\
`,
      `\
IF commentsFollowersRatio IS veryLow
THEN relevance IS veryHigh WITH 0.5\
`,
      `\
IF commentsFollowersRatio IS low
THEN relevance IS high WITH 0.5\
`,
      `\
IF commentsFollowersRatio IS medium
THEN relevance IS medium WITH 0.5\
`,
      `\
IF commentsFollowersRatio IS high
THEN relevance IS low WITH 0.5\
`,
      `\
IF commentsFollowersRatio IS veryHigh
THEN relevance IS veryLow WITH 0.5\
`,
      `\
IF hashtagMatches IS veryLow
THEN relevance IS veryLow WITH 1.0\
`,
      `\
IF hashtagMatches IS low
THEN relevance IS low WITH 1.0\
`,
      `\
IF hashtagMatches IS medium
THEN relevance IS medium WITH 1.0\
`,
      `\
IF hashtagMatches IS high
THEN relevance IS high WITH 1.0\
`,
      `\
IF hashtagMatches IS veryHigh
THEN relevance IS veryHigh WITH 1.0\
`,
      `\
IF influencerLocation IS true
THEN relevance IS veryHigh WITH 0.5\
`,
      `\
IF influencerLocation IS false
THEN relevance IS veryLow WITH 0.5\
`,
      `\
IF audienceLocation IS true
THEN relevance IS veryHigh WITH 1.0\
`,
      `\
IF audienceLocation IS false
THEN relevance IS veryLow WITH 1.0\
`,
      `\
IF ageDemographics IS veryLow
THEN relevance IS veryLow WITH 1.0\
`,
      `\
IF ageDemographics IS low
THEN relevance IS low WITH 1.0\
`,
      `\
IF ageDemographics IS medium
THEN relevance IS medium WITH 1.0\
`,
      `\
IF ageDemographics IS high
THEN relevance IS high WITH 1.0\
`,
      `\
IF ageDemographics IS veryHigh
THEN relevance IS veryHigh WITH 1.0\
`,
      `\
IF genderDemographics IS veryLow
THEN relevance IS veryLow WITH 1.0\
`,
      `\
IF genderDemographics IS low
THEN relevance IS low WITH 1.0\
`,
      `\
IF genderDemographics IS medium
THEN relevance IS medium WITH 1.0\
`,
      `\
IF genderDemographics IS high
THEN relevance IS high WITH 1.0\
`,
      `\
IF genderDemographics IS veryHigh
THEN relevance IS veryHigh WITH 1.0\
`,
      `\
IF numberOfBids IS veryLow
THEN relevance IS veryLow WITH 0.75\
`,
      `\
IF numberOfBids IS low
THEN relevance IS low WITH 0.75\
`,
      `\
IF numberOfBids IS medium
THEN relevance IS medium WITH 0.75\
`,
      `\
IF numberOfBids IS high
THEN relevance IS high WITH 1.0\
`,
      `\
IF numberOfBids IS veryHigh
THEN relevance IS veryHigh WITH 1.0\
`,
      `\
IF numberOfPastCampaigns IS veryLow
THEN relevance IS veryLow WITH 0.75\
`,
      `\
IF numberOfPastCampaigns IS low
THEN relevance IS low WITH 0.75\
`,
      `\
IF numberOfPastCampaigns IS medium
THEN relevance IS medium WITH 1.0\
`,
      `\
IF numberOfPastCampaigns IS high
THEN relevance IS high WITH 1.0\
`,
      `\
IF numberOfPastCampaigns IS veryHigh
THEN relevance IS veryHigh WITH 1.0\
`
    ]
  }, {
    likesFollowersRatio: 30.41859358,
    commentsFollowersRatio: 2526.977401,
    hashtagMatches: 0,
    influencerLocation: 1,
    audienceLocation: 0,
    ageDemographics: 50,
    genderDemographics: 50,
    numberOfBids: 0,
    numberOfPastCampaigns: 0
  }
    , { 'it looks correct' (err, results, props) {
    assert.ifError(err)
  }
  }
  )).export(module)

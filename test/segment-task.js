/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const _ = require('lodash')

const stask = function (s1, s2, r) {
  const task = {
    topic (segment) {
      const { callback } = this
      try {
        const p = segment.intersect(s1, s2)
        callback(null, p)
      } catch (err) {
        callback(err, null)
      }
      return undefined
    },
    'it works' (err, pt) {
      assert.ifError(err)
    },
    'it looks right' (err, pt) {
      assert.ifError(err)
      // console.dir {s1: s1, s2: s2, r: r, pt: pt}
      if (_.isNull(r)) {
        assert.isNull(pt)
      } else if (_.isArray(r)) {
        assert.isArray(pt)
        assert.equal(r.length, pt.length)
        if (_.isFinite(r[0])) {
          assert.lengthOf(pt, 2)
          assert.isNumber(pt[0])
          assert.isNumber(pt[1])
          assert.inDelta(pt[0], r[0], 0.000001)
          assert.inDelta(pt[1], r[1], 0.000001)
        } else if (_.isArray(r[0])) {
          assert.isArray(pt[0])
          assert.lengthOf(pt[0], 2)
          assert.isArray(pt[1])
          assert.lengthOf(pt[1], 2)
          assert.inDelta(pt[0][0], r[0][0], 0.000001)
          assert.inDelta(pt[1][0], r[1][0], 0.000001)
          assert.inDelta(pt[0][1], r[0][1], 0.000001)
          assert.inDelta(pt[1][1], r[1][1], 0.000001)
        } else {
          throw new Error(`Unexpected reference value ${r}`)
        }
      } else {
        throw new Error(`Unexpected reference value ${r}`)
      }
    }
  }
  return task
}

module.exports = stask

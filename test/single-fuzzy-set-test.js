// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')

const LoopError = require('./loop-error')

vows
  .describe('Single fuzzy set for FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with a single input with a single fuzzy set': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  high: [0, 1, 2]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS high THEN output1 IS high'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we evaluate over the range': {
          topic (fc) {
            const { callback } = this
            const resultses = []
            let f = null
            let o = null
            let c = {}
            let i = null
            try {
              _.each([0, 1], (num) => {
                i = num
                f = (o = null)
                c = {}
                fc.once('fuzzified', fuzzified => { f = fuzzified })
                fc.once('inferred', output => { o = output })
                fc.once('clipped', (clipped, name) => { c[name] = clipped })
                const results = fc.evaluate({input1: num})
                return resultses.push(results)
              })
              callback(null, resultses)
            } catch (err) {
              callback(new LoopError(err, i, f, o, c), null)
            }
            return undefined
          },
          'it works' (err, resultses) {
            assert.ifError(err)
          },
          'one of the outputs is null' (err, resultses) {
            assert.ifError(err)
            assert.isObject(_.find(resultses, results => _.isNull(results.output1)))
          }
        }
      },
      'and we instantiate with an input with a single fuzzy set and another input': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                },
                input2: {
                  high: [0, 1, 2]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow',
                'IF input1 IS low THEN output1 IS low',
                'IF input1 IS medium THEN output1 IS medium',
                'IF input1 IS high THEN output1 IS high',
                'IF input1 IS veryHigh THEN output1 IS veryHigh',
                'IF input2 IS high THEN output1 IS high'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we evaluate over the range': {
          topic (fc) {
            const { callback } = this
            const resultses = []
            let f = null
            let o = null
            let c = {}
            let i = null
            let j = null
            try {
              _.each(__range__(-10, 110, true), (num) => {
                i = num
                return _.each([0, 1], (num) => {
                  j = num
                  f = (o = null)
                  c = {}
                  fc.once('fuzzified', fuzzified => { f = fuzzified })
                  fc.once('inferred', output => { o = output })
                  fc.once('clipped', (clipped, name) => { c[name] = clipped })
                  const results = fc.evaluate({input1: i, input2: j})
                  return resultses.push(results)
                })
              })
              callback(null, resultses)
            } catch (err) {
              callback(new LoopError(err, [i, j], f, o, c), null)
            }
            return undefined
          },
          'it works' (err, resultses) {
            assert.ifError(err)
          },
          'none of the outputs is null' (err, resultses) {
            assert.ifError(err)
            assert.ok(_.every(resultses, results => !_.isNull(results.output1)))
          }
        }
      }
    }}).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}

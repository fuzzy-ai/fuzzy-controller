// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')

const LoopError = require('./loop-error')

vows
  .describe('Correct results for weighted FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 5],
                  low: [0, 5, 10],
                  medium: [5, 10, 15],
                  high: [10, 15, 20],
                  veryHigh: [15, 20]
                },
                input2: {
                  veryLow: [0, 5],
                  low: [0, 5, 10],
                  medium: [5, 10, 15],
                  high: [10, 15, 20],
                  veryHigh: [15, 20]
                },
                input3: {
                  veryLow: [0, 5],
                  low: [0, 5, 10],
                  medium: [5, 10, 15],
                  high: [10, 15, 20],
                  veryHigh: [15, 20]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 5],
                  low: [0, 5, 10],
                  medium: [5, 10, 15],
                  high: [10, 15, 20],
                  veryHigh: [15, 20]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow',
                'IF input1 IS low THEN output1 IS low',
                'IF input1 IS medium THEN output1 IS medium',
                'IF input1 IS high THEN output1 IS high',
                'IF input1 IS veryHigh THEN output1 IS veryHigh',
                'IF input2 IS veryLow THEN output1 IS veryLow WITH 0.75',
                'IF input2 IS low THEN output1 IS low WITH 0.75',
                'IF input2 IS medium THEN output1 IS medium WITH 0.75',
                'IF input2 IS high THEN output1 IS high WITH 0.75',
                'IF input2 IS veryHigh THEN output1 IS veryHigh WITH 0.75',
                'IF input3 IS veryLow THEN output1 IS veryLow WITH 0.25',
                'IF input3 IS low THEN output1 IS low WITH 0.25',
                'IF input3 IS medium THEN output1 IS medium WITH 0.25',
                'IF input3 IS high THEN output1 IS high WITH 0.25',
                'IF input3 IS veryHigh THEN output1 IS veryHigh WITH 0.25'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we evaluate over the range': {
          topic (fc) {
            const { callback } = this
            const matrix = []
            let f = null
            let o = null
            let c = {}
            let i = null
            let j = null
            let k = null
            try {
              _.each(__range__(-1, 5, true), (fiv) => {
                const num = fiv * 5
                i = num
                matrix[i] = new Array(31)
                return _.each(__range__(-1, 5, true), (fiv2) => {
                  const num2 = fiv2 * 5
                  j = num2
                  matrix[i][j] = new Array(31)
                  return _.each(__range__(-1, 5, true), (fiv3) => {
                    const num3 = fiv3 * 5
                    k = num3
                    f = (o = null)
                    c = {}
                    fc.once('fuzzified', fuzzified => { f = fuzzified })
                    fc.once('inferred', output => { o = output })
                    fc.once('clipped', (clipped, name) => { c[name] = clipped })
                    const inputs = {input1: num, input2: num2, input3: num3}
                    const outputs = fc.evaluate(inputs)
                    matrix[i][j][k] = outputs.output1
                  })
                })
              })
              callback(null, matrix)
            } catch (err) {
              callback(new LoopError(err, [i, j, k], f, o, c), null)
            }
            return undefined
          },
          'it works' (err, matrix) {
            assert.ifError(err)
            assert.greater(matrix[15][10][5], matrix[10][15][5], `WITH modifier matters: ${matrix[10][15][5]} ${matrix[15][10][5]}`)
            assert.greater(matrix[10][15][5], matrix[5][15][10], `WITH modifier matters: ${matrix[10][15][5]} ${matrix[5][15][10]}`)
          }
        }
      }
    }}).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}

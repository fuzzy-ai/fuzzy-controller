// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')

const controllerBatch = require('./controllerbatch')

vows
  .describe('Problematic input from Flatbook')
  .addBatch(controllerBatch({
    inputs: {
      leadTime: {
        veryShort: [
          0,
          7
        ],
        short: [
          0,
          7,
          14
        ],
        medium: [
          7,
          14,
          30,
          37
        ],
        long: [
          30,
          37,
          60,
          100
        ],
        veryLong: [
          60,
          100
        ]
      },
      isPeakSeason: {
        false: [
          0,
          0.5
        ],
        true: [
          0.5,
          1
        ]
      },
      eventScore: {
        veryLow: [
          0,
          2.5
        ],
        low: [
          0,
          2.5,
          5
        ],
        medium: [
          2.5,
          5,
          7.5
        ],
        high: [
          5,
          7.5,
          10
        ],
        veryHigh: [
          7.5,
          10
        ]
      },
      cityOccupancy: {
        veryLow: [
          0,
          25
        ],
        low: [
          0,
          25,
          50
        ],
        medium: [
          25,
          50,
          75
        ],
        high: [
          50,
          75,
          100
        ],
        veryHigh: [
          75,
          100
        ]
      },
      cityOccupancyByBedroomCount: {
        veryLow: [
          0,
          25
        ],
        low: [
          0,
          25,
          50
        ],
        medium: [
          25,
          50,
          75
        ],
        high: [
          50,
          75,
          100
        ],
        veryHigh: [
          75,
          100
        ]
      },
      distFromCityCenter: {
        veryLow: [
          0,
          5
        ],
        low: [
          0,
          5,
          10
        ],
        medium: [
          5,
          10,
          15,
          20
        ],
        high: [
          15,
          20,
          40,
          45
        ],
        veryHigh: [
          40,
          45
        ]
      },
      bedroomCount: {
        veryLow: [
          1,
          2
        ],
        low: [
          1,
          2,
          3
        ],
        medium: [
          2,
          3,
          4,
          5
        ],
        high: [
          4,
          5,
          7,
          8
        ],
        veryHigh: [
          7,
          8
        ]
      },
      isBusinessFocused: {
        false: [
          0,
          0.5
        ],
        true: [
          0.5,
          1
        ]
      },
      hasAC: {
        false: [
          0,
          0.5
        ],
        true: [
          0.5,
          1
        ]
      },
      hasPool: {
        false: [
          0,
          0.5
        ],
        true: [
          0.5,
          1
        ]
      },
      isWeekend: {
        false: [
          0,
          0.5
        ],
        true: [
          0.5,
          1
        ]
      },
      avgTemp: {
        veryLow: [
          -20,
          -7.5
        ],
        low: [
          -20,
          -7.5,
          5
        ],
        medium: [
          -7.5,
          5,
          17.5
        ],
        high: [
          5,
          17.5,
          30
        ],
        veryHigh: [
          17.5,
          30
        ]
      },
      qualityScore: {
        veryLow: [
          0,
          2.5
        ],
        low: [
          0,
          2.5,
          5
        ],
        medium: [
          2.5,
          5,
          7.5
        ],
        high: [
          5,
          7.5,
          10
        ],
        veryHigh: [
          7.5,
          10
        ]
      }
    },
    outputs: {
      multiplier: {
        veryLow: [
          50,
          112.5
        ],
        low: [
          50,
          112.5,
          175
        ],
        medium: [
          112.5,
          175,
          237.5
        ],
        high: [
          175,
          237.5,
          300
        ],
        veryHigh: [
          212.5,
          300
        ]
      }
    },
    rules: [
      `\
IF leadTime IS veryShort
THEN multiplier IS veryHigh WITH 1.0\
`,
      `\
IF leadTime IS short
THEN multiplier IS high WITH 1.0\
`,
      `\
IF leadTime IS medium
THEN multiplier IS medium WITH 1.0\
`,
      `\
IF leadTime IS long
THEN multiplier IS low WITH 1.0\
`,
      `\
IF leadTime IS veryLong
THEN multiplier IS veryLow WITH 1.0\
`,
      `\
IF isPeakSeason IS true
THEN multiplier IS high WITH 1.0\
`,
      `\
IF isPeakSeason IS false
THEN multiplier IS low WITH 1.0\
`,
      `\
IF eventScore IS veryLow
THEN multiplier IS veryLow WITH 1.0\
`,
      `\
IF eventScore IS low
THEN multiplier IS low WITH 1.0\
`,
      `\
IF eventScore IS medium
THEN multiplier IS medium WITH 1.0\
`,
      `\
IF eventScore IS high
THEN multiplier IS high WITH 1.0\
`,
      `\
IF eventScore IS veryHigh
THEN multiplier IS veryHigh WITH 1.0\
`,
      `\
IF cityOccupancy IS veryLow
THEN multiplier IS veryLow WITH 0.75\
`,
      `\
IF cityOccupancy IS low
THEN multiplier IS low WITH 0.75\
`,
      `\
IF cityOccupancy IS medium
THEN multiplier IS medium WITH 0.75\
`,
      `\
IF cityOccupancy IS high
THEN multiplier IS high WITH 0.75\
`,
      `\
IF cityOccupancy IS veryHigh
THEN multiplier IS veryHigh WITH 0.75\
`,
      `\
IF cityOccupancyByBedroomCount IS veryLow
THEN multiplier IS veryLow WITH 1.0\
`,
      `\
IF cityOccupancyByBedroomCount IS low
THEN multiplier IS low WITH 1.0\
`,
      `\
IF cityOccupancyByBedroomCount IS medium
THEN multiplier IS medium WITH 1.0\
`,
      `\
IF cityOccupancyByBedroomCount IS high
THEN multiplier IS high WITH 1.0\
`,
      `\
IF cityOccupancyByBedroomCount IS veryHigh
THEN multiplier IS veryHigh WITH 1.0\
`,
      `\
IF distFromCityCenter IS veryLow AND leadTime IS veryShort
THEN multiplier IS veryHigh WITH 1.0\
`,
      `\
IF distFromCityCenter IS low AND leadTime IS veryShort
THEN multiplier IS veryHigh WITH 1.0\
`,
      `\
IF distFromCityCenter IS veryLow AND leadTime IS short
THEN multiplier IS high WITH 1.0\
`,
      `\
IF distFromCityCenter IS low AND leadTime IS short
THEN multiplier IS high WITH 1.0\
`,
      `\
IF distFromCityCenter IS veryLow AND leadTime IS veryShort AND cityOccupancy is veryLow
THEN multiplier IS veryHigh WITH 1.0\
`,
      `\
IF isWeekend IS true AND isBusinessFocused IS true
THEN multiplier IS medium WITH 1.0\
`,
      `\
IF isWeekend IS false AND isBusinessFocused IS true
THEN multiplier IS high WITH 1.0\
`,
      `\
IF qualityScore IS veryLow
THEN multiplier IS veryLow WITH 0.75\
`,
      `\
IF qualityScore IS low
THEN multiplier IS low WITH 0.75\
`,
      `\
IF qualityScore IS medium
THEN multiplier IS medium WITH 0.75\
`,
      `\
IF qualityScore IS high
THEN multiplier IS high WITH 0.75\
`,
      `\
IF qualityScore IS veryHigh
THEN multiplier IS veryHigh WITH 0.75\
`,
      `\
IF hasAC IS true AND avgTemp IS high
THEN multiplier IS high WITH 0.75\
`,
      `\
IF hasAC IS true AND avgTemp IS veryHigh
THEN multiplier IS veryHigh WITH 0.75\
`,
      `\
IF hasPool IS true AND avgTemp IS high
THEN multiplier IS high WITH 0.75\
`,
      `\
IF hasPool IS true AND avgTemp IS veryHigh
THEN multiplier IS veryHigh WITH 0.75\
`
    ]
  }, {
    leadTime: 90,
    isPeakSeason: 0,
    eventScore: 7,
    cityOccupancy: 80,
    cityOccupancyByBedroomCount: 80,
    distFromCityCenter: 3,
    bedroomCount: 2,
    isBusinessFocused: 0,
    hasAC: 1,
    hasPool: 1,
    isWeekend: 1,
    avgTemp: 12,
    qualityScore: 9
  }
  )).export(module)

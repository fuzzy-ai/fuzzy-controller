/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const _ = require('lodash')

const polygon = require('../lib/polygon')

const polygonBatch = function (polygons, length, tests) {
  if (!tests) {
    tests = length
    length = 1
  }
  const batch = {
    'When we union the polygons': {
      topic () {
        const { callback } = this
        try {
          const p = polygon.union(polygons[0], polygons[1])
          callback(null, p)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, shapes) {
        assert.ifError(err)
      },
      'it looks right' (err, shapes) {
        assert.ifError(err)
        assert.isArray(shapes)
        assert.ok(([1, 2].includes(shapes.length)), 'Shapes length one of 1, 2')
        assert.isArray(shapes[0])
      }
    }
  }
  if (tests) {
    _.extend(batch['When we union the polygons'], tests)
  }
  return batch
}

module.exports = polygonBatch

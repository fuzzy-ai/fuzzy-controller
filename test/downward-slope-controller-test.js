// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const controllerBatch = require('./controllerbatch')

vows
  .describe('Better performance with a downward slope')
  .addBatch(controllerBatch({
    inputs: {
      input1: {
        low: [
          0,
          50
        ],
        medium: [
          0,
          50,
          100
        ],
        high: [
          50,
          100
        ]
      }
    },
    outputs: {
      output1: {
        low: [
          0,
          50
        ],
        medium: [
          0,
          50,
          100
        ],
        high: [
          50,
          100
        ]
      }
    },
    rules: [
      'IF input1 IS low THEN output1 IS low',
      'IF input1 IS medium THEN output1 IS medium',
      'IF input1 IS high THEN output1 IS high'
    ]
  },
  {input1: 0}
    , {
    'the output is zero' (err, results) {
      assert.ifError(err)
      assert.inDelta(results.output1, 0, 0.001)
    }
  }
  )).export(module)

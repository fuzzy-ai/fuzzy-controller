/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS205: Consider reworking code to avoid use of IIFEs
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

let y, x

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')
const debug = require('debug')('fuzzy-controller:planar-fuzzy-controller-test')

const controllerBatch = require('./controllerbatch')

const TOLERANCE = 10

let inputSets = ((() => {
  const result = []
  for (y = -10; y <= 110; y += 5) {
    result.push((() => {
      const result1 = []
      for (x = -10; x <= 110; x += 5) {
        result1.push({x, y})
      }
      return result1
    })())
  }
  return result
})())
inputSets = _.flatten(inputSets)

vows
  .describe('Planar fuzzy controller')
  .addBatch(controllerBatch({
    inputs: {
      x: {
        veryLow: [0, 25],
        low: [0, 25, 50],
        medium: [25, 50, 75],
        high: [50, 75, 100],
        veryHigh: [75, 100]
      },
      y: {
        veryLow: [0, 25],
        low: [0, 25, 50],
        medium: [25, 50, 75],
        high: [50, 75, 100],
        veryHigh: [75, 100]
      }
    },
    outputs: {
      z: {
        veryLow: [0, 50],
        low: [0, 50, 100],
        medium: [50, 100, 150],
        high: [100, 150, 200],
        veryHigh: [150, 200]
      }
    },
    rules: [
      'x INCREASES z',
      'y INCREASES z'
    ]
  },
  inputSets, {
    'it looks correct' (err, resultses, propses) {
      assert.ifError(err)
      assert.isArray(resultses)
      assert.isArray(propses)
      return (() => {
        const result2 = []
        for (let i = 0; i < inputSets.length; i++) {
          const inputSet = inputSets[i]
          const outputSet = resultses[i]
          debug({x: inputSet.x, y: inputSet.y, z: outputSet.z})
          if (inputSet.x < 0) {
            if (inputSet.y < 0) {
              result2.push(assert.inDelta(outputSet.z, 0, TOLERANCE))
            } else if (inputSet.y > 100) {
              result2.push(assert.inDelta(outputSet.z, 100, TOLERANCE))
            } else {
              result2.push(assert.inDelta(outputSet.z, inputSet.y, TOLERANCE))
            }
          } else if (inputSet.x > 100) {
            if (inputSet.y < 0) {
              result2.push(assert.inDelta(outputSet.z, 100, TOLERANCE))
            } else if (inputSet.y > 100) {
              result2.push(assert.inDelta(outputSet.z, 200, TOLERANCE))
            } else {
              result2.push(assert.inDelta(outputSet.z, inputSet.y + 100, TOLERANCE))
            }
          } else {
            if (inputSet.y < 0) {
              result2.push(assert.inDelta(outputSet.z, inputSet.x, TOLERANCE))
            } else if (inputSet.y > 100) {
              result2.push(assert.inDelta(outputSet.z, inputSet.x + 100, TOLERANCE))
            } else {
              result2.push(assert.inDelta(outputSet.z, inputSet.x + inputSet.y, TOLERANCE))
            }
          }
        }
        return result2
      })()
    }
  }
  )).export(module)

// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

vows
  .describe('FuzzyController.fuzzyify() test')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'it has a fuzzify() method' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController.fuzzify)
      },
      'and we fuzzify a value': {
        topic (FuzzyController) {
          const { callback } = this
          const temperature = {
            cold: [50, 75],
            normal: [50, 75, 85, 100],
            hot: [85, 100]
          }
          try {
            const results = FuzzyController.fuzzify(60, temperature)
            callback(null, results)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, results) {
          assert.ifError(err)
          assert.isObject(results)
          assert.isNumber(results['cold'])
          assert.equal(results['cold'], 0.6)
          assert.isNumber(results['normal'])
          assert.equal(results['normal'], 0.4)
          assert.isNumber(results['hot'])
          assert.equal(results['hot'], 0)
        }
      }
    }}).export(module)

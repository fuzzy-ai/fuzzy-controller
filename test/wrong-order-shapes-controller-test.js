// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const controllerBatch = require('./controllerbatch')

vows
  .describe('Model where shapes get out of order')
  .addBatch(controllerBatch({
    inputs: {
      x1: {
        veryLow: [
          1.3188858559180936,
          1.9595322031527758
        ],
        low: [
          1.4605225287377834,
          2.105889627975036,
          2.7821985241025686
        ],
        medium: [
          2.252728442661464,
          2.920259448644164,
          3.5760297859087586
        ],
        high: [
          3.0680231153964996,
          3.719969829069865,
          4.472577128559351
        ],
        veryHigh: [
          3.890828136354685,
          4.638462965172217
        ]
      },
      x2: {
        veryLow: [
          1.4142372824632898,
          2.0532450284808874
        ],
        low: [
          1.560294329188764,
          2.2000492275092975,
          2.9300253465771675
        ],
        medium: [
          2.346976631321013,
          3.0859045953298416,
          3.7144492706283927
        ],
        high: [
          3.229278424754739,
          3.854047339644136,
          4.464500088244677
        ],
        veryHigh: [
          3.994194332510233,
          4.604582032590449
        ]
      }
    },
    outputs: {
      y: {
        veryLow: [
          1.5170032315332431,
          1.9682779127871046
        ],
        low: [
          1.4377214393108728,
          2.072348714993379,
          2.73358855696546
        ],
        medium: [
          2.2238421197947575,
          2.8803644134791018,
          3.751529973169173
        ],
        high: [
          3.0830476399899536,
          3.9645204439167756,
          5.5919758991518895
        ],
        veryHigh: [
          4.337863391939131,
          5.942038618612749
        ]
      }
    },
    rules: [
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS veryLow WITH 0.803',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS low WITH 0.100',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS medium WITH 0.378',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS high WITH 0.312',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS veryHigh WITH 0.342',
      'IF x1 IS veryLow AND x2 IS low THEN y IS veryLow WITH 0.743',
      'IF x1 IS veryLow AND x2 IS low THEN y IS low WITH 0.752',
      'IF x1 IS veryLow AND x2 IS low THEN y IS medium WITH 0.388',
      'IF x1 IS veryLow AND x2 IS low THEN y IS high WITH 0.813',
      'IF x1 IS veryLow AND x2 IS low THEN y IS veryHigh WITH 0.468',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS veryLow WITH 0.987',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS low WITH 0.756',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS medium WITH 0.394',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS high WITH 0.442',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS veryHigh WITH 0.923',
      'IF x1 IS veryLow AND x2 IS high THEN y IS veryLow WITH 0.937',
      'IF x1 IS veryLow AND x2 IS high THEN y IS low WITH 0.957',
      'IF x1 IS veryLow AND x2 IS high THEN y IS medium WITH 0.671',
      'IF x1 IS veryLow AND x2 IS high THEN y IS high WITH 0.217',
      'IF x1 IS veryLow AND x2 IS high THEN y IS veryHigh WITH 0.826',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS veryLow WITH 0.343',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS low WITH 0.796',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS medium WITH 0.449',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS high WITH 0.777',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS veryHigh WITH 0.276',
      'IF x1 IS low AND x2 IS veryLow THEN y IS veryLow WITH 0.842',
      'IF x1 IS low AND x2 IS veryLow THEN y IS low WITH 0.969',
      'IF x1 IS low AND x2 IS veryLow THEN y IS medium WITH 0.097',
      'IF x1 IS low AND x2 IS veryLow THEN y IS high WITH 0.684',
      'IF x1 IS low AND x2 IS veryLow THEN y IS veryHigh WITH 0.042',
      'IF x1 IS low AND x2 IS low THEN y IS veryLow WITH 0.605',
      'IF x1 IS low AND x2 IS low THEN y IS low WITH 0.993',
      'IF x1 IS low AND x2 IS low THEN y IS medium WITH 0.535',
      'IF x1 IS low AND x2 IS low THEN y IS high WITH 0.038',
      'IF x1 IS low AND x2 IS low THEN y IS veryHigh WITH 1.000',
      'IF x1 IS low AND x2 IS medium THEN y IS veryLow WITH 0.614',
      'IF x1 IS low AND x2 IS medium THEN y IS low WITH 0.388',
      'IF x1 IS low AND x2 IS medium THEN y IS medium WITH 0.321',
      'IF x1 IS low AND x2 IS medium THEN y IS high WITH 0.178',
      'IF x1 IS low AND x2 IS medium THEN y IS veryHigh WITH 0.688',
      'IF x1 IS low AND x2 IS high THEN y IS veryLow WITH 0.049',
      'IF x1 IS low AND x2 IS high THEN y IS low WITH 0.720',
      'IF x1 IS low AND x2 IS high THEN y IS medium WITH 0.624',
      'IF x1 IS low AND x2 IS high THEN y IS high WITH 0.058',
      'IF x1 IS low AND x2 IS high THEN y IS veryHigh WITH 0.874',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS veryLow WITH 0.374',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS low WITH 0.339',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS medium WITH 0.014',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS high WITH 0.045',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS veryHigh WITH 0.701',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS veryLow WITH 0.664',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS low WITH 0.458',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS medium WITH 0.369',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS high WITH 0.485',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS veryHigh WITH 0.159',
      'IF x1 IS medium AND x2 IS low THEN y IS veryLow WITH 0.362',
      'IF x1 IS medium AND x2 IS low THEN y IS low WITH 0.593',
      'IF x1 IS medium AND x2 IS low THEN y IS medium WITH 0.283',
      'IF x1 IS medium AND x2 IS low THEN y IS high WITH 0.706',
      'IF x1 IS medium AND x2 IS low THEN y IS veryHigh WITH 0.314',
      'IF x1 IS medium AND x2 IS medium THEN y IS veryLow WITH 0.662',
      'IF x1 IS medium AND x2 IS medium THEN y IS low WITH 0.317',
      'IF x1 IS medium AND x2 IS medium THEN y IS medium WITH 0.007',
      'IF x1 IS medium AND x2 IS medium THEN y IS high WITH 0.792',
      'IF x1 IS medium AND x2 IS medium THEN y IS veryHigh WITH 0.550',
      'IF x1 IS medium AND x2 IS high THEN y IS veryLow WITH 0.873',
      'IF x1 IS medium AND x2 IS high THEN y IS low WITH 0.168',
      'IF x1 IS medium AND x2 IS high THEN y IS medium WITH 0.534',
      'IF x1 IS medium AND x2 IS high THEN y IS high WITH 0.502',
      'IF x1 IS medium AND x2 IS high THEN y IS veryHigh WITH 0.112',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS veryLow WITH 0.361',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS low WITH 0.797',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS medium WITH 0.259',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS high WITH 0.197',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS veryHigh WITH 0.568',
      'IF x1 IS high AND x2 IS veryLow THEN y IS veryLow WITH 0.502',
      'IF x1 IS high AND x2 IS veryLow THEN y IS low WITH 0.893',
      'IF x1 IS high AND x2 IS veryLow THEN y IS medium WITH 0.087',
      'IF x1 IS high AND x2 IS veryLow THEN y IS high WITH 0.687',
      'IF x1 IS high AND x2 IS veryLow THEN y IS veryHigh WITH 0.544',
      'IF x1 IS high AND x2 IS low THEN y IS veryLow WITH 0.017',
      'IF x1 IS high AND x2 IS low THEN y IS low WITH 0.791',
      'IF x1 IS high AND x2 IS low THEN y IS medium WITH 0.834',
      'IF x1 IS high AND x2 IS low THEN y IS high WITH 0.862',
      'IF x1 IS high AND x2 IS low THEN y IS veryHigh WITH 0.293',
      'IF x1 IS high AND x2 IS medium THEN y IS veryLow WITH 0.572',
      'IF x1 IS high AND x2 IS medium THEN y IS low WITH 0.321',
      'IF x1 IS high AND x2 IS medium THEN y IS medium WITH 0.162',
      'IF x1 IS high AND x2 IS medium THEN y IS high WITH 0.896',
      'IF x1 IS high AND x2 IS medium THEN y IS veryHigh WITH 0.301',
      'IF x1 IS high AND x2 IS high THEN y IS veryLow WITH 0.615',
      'IF x1 IS high AND x2 IS high THEN y IS low WITH 0.901',
      'IF x1 IS high AND x2 IS high THEN y IS medium WITH 0.179',
      'IF x1 IS high AND x2 IS high THEN y IS high WITH 0.713',
      'IF x1 IS high AND x2 IS high THEN y IS veryHigh WITH 0.594',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS veryLow WITH 0.118',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS low WITH 0.257',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS medium WITH 0.407',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS high WITH 0.188',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS veryHigh WITH 0.457',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS veryLow WITH 0.303',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS low WITH 0.083',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS medium WITH 0.605',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS high WITH 0.822',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS veryHigh WITH 0.429',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS veryLow WITH 0.694',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS low WITH 0.512',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS medium WITH 0.671',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS high WITH 0.066',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS veryHigh WITH 0.047',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS veryLow WITH 0.383',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS low WITH 0.078',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS medium WITH 0.521',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS high WITH 0.003',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS veryHigh WITH 0.500',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS veryLow WITH 0.393',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS low WITH 0.746',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS medium WITH 0.337',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS high WITH 0.968',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS veryHigh WITH 0.725',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS veryLow WITH 0.940',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS low WITH 0.412',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS medium WITH 0.817',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS high WITH 0.787',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS veryHigh WITH 0.099'
    ]
  }, {
    x1: 4.675046239979565,
    x2: 3.831034764647484
  }
    , {
    'the output is OK' (err, output, props) {
      assert.ifError(err)
    }
  }
  )).export(module)

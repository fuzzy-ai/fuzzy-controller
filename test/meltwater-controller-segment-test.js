/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const stask = require('./segment-task')

vows
  .describe('Segments from Meltwater controller')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const segment = require('../lib/segment')
          callback(null, segment)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, segment) {
        assert.ifError(err)
        assert.isObject(segment)
      },
      'and we get the intersection of two line segments that do not cross':
        stask([[ 100, 0 ], [ 50, 0 ]], [[ 71.5, 0.13999999999999999 ], [ 74.99999966242284, 1.3503086440591183e-8 ]], null)
    }}).export(module)

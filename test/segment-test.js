/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const stask = require('./segment-task')

vows
  .describe('Segment')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const segment = require('../lib/segment')
          callback(null, segment)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, segment) {
        assert.ifError(err)
        assert.isObject(segment)
      },
      'it has an intersect member' (err, segment) {
        assert.ifError(err)
        assert.isFunction(segment.intersect)
      },
      'and we get the intersection of two parallel line segments':
        stask([[1, 2], [4, 5]], [[1, 3], [4, 6]], null),
      'and we get the intersection of a vertical line segment with a non-vertical one':
        stask([[3, 0], [3, 8]], [[1, 1], [5, 5]], [3, 3]),
      'and we get the intersection of a horizontal line segment with a non-horizontal one':
        stask([[0, 2], [9, 2]], [[1, 1], [5, 5]], [2, 2]),
      'and we get the intersection of a downward-sloped segment with an upward-sloped one':
        stask([[1, 5], [4, 2]], [[1, 1], [5, 5]], [3, 3]),
      'and we get the intersection of two overlapping colinear horizontal line segments':
        stask([[1, 0], [4, 0]], [[3, 0], [5, 0]], [[3, 0], [4, 0]]),
      'and we get the intersection of two overlapping colinear vertical line segments':
        stask([[1, 0], [1, 3]], [[1, 2], [1, 4]], [[1, 2], [1, 3]]),
      'and we get the intersection of two line segments that T together':
        stask([[6, 0.5], [7, 0]], [[11, 0], [5, 0]], [7, 0]),
      'and we get the intersection of a horizontal segment joined with another one as a T':
        stask([[0, 0], [46, 0]], [[33, 0.6], [45, 0]], [45, 0]),
      'and we get the intersection of two non-collinear vertical segments':
        stask([[50, 0], [50, 0.75]], [[300, 0.6], [300, 0]], null),
      'and we get the intersection of two other non-collinear vertical segments':
        stask([[50, 0], [50, 0.75]], [[300, 0.6], [300, 0]], null),
      'and we get the intersection of two line segments that touch at the first and first points':
        stask([[3, 0], [2, 2]], [[3, 0], [5, 0]], [3, 0]),
      'and we get the intersection of two line segments that touch at the first and second points':
        stask([[3, 0], [2, 2]], [[3, 5], [3, 0]], [3, 0]),
      'and we get the intersection of two line segments that touch at the second and first points':
        stask([[1, 0], [2, 2]], [[2, 2], [4, 2]], [2, 2]),
      'and we get the intersection of two other line segments that touch at the second and first points':
        stask([[5, 0], [1, 0]], [[1, 0], [0, 0]], [1, 0]),
      'and we get the intersection of two line segments that touch at the second and second points':
        stask([[1, 0], [2, 2]], [[1, 2], [2, 2]], [2, 2]),
      'and we get the intersection of two collinear line segments that start at the same point and go different distances':
        stask([[5, 0], [1, 0]], [[5, 0], [0, 0]], [[1, 0], [5, 0]]),
      'and we get the intersection of two collinear line segments that touch at one point':
        stask([[5, 0], [1, 0]], [[6, 0], [5, 0]], [5, 0])
    }})
  .export(module)

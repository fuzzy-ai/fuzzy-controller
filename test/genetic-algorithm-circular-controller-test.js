/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const controllerBatch = require('./controllerbatch')

vows
  .describe('Better performance with a downward slope')
  .addBatch(controllerBatch({
    inputs: {
      x1: {
        veryLow: [
          1.3456719481107333,
          2.0322892693802714
        ],
        low: [
          1.487212624400854,
          2.189791245021694,
          2.8657917892560363
        ],
        medium: [
          2.3423758298158646,
          3.0142461813254458,
          3.646324096247554
        ],
        high: [
          3.1587912682443857,
          3.7859829127012135,
          4.497542763128877
        ],
        veryHigh: [
          3.9502562284469604,
          4.659208803154695
        ]
      },
      x2: {
        veryLow: [
          1.4113154268714876,
          2.049736392684281
        ],
        low: [
          1.55851028021425,
          2.195912302437299,
          2.916892006061971
        ],
        medium: [
          2.346976631321013,
          3.0833802003707276,
          3.7144492706283927
        ],
        high: [
          3.229278424754739,
          3.8543104897203944,
          4.468343997374177
        ],
        veryHigh: [
          3.994194332510233,
          4.605697166152024
        ]
      }
    },
    outputs: {
      y: {
        veryLow: [
          1.5170842886498028,
          1.9686924226980944
        ],
        low: [
          1.4730775096331663,
          2.0727217454144338,
          2.73358855696546
        ],
        medium: [
          2.226574399528671,
          2.8812014970069186,
          3.7633096238395307
        ],
        high: [
          3.087970569337602,
          3.965390574870316,
          5.5919758991518895
        ],
        veryHigh: [
          4.337863391939131,
          5.942814955303774
        ]
      }
    },
    rules: [
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS veryLow WITH 0.148',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS low WITH 0.388',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS medium WITH 0.406',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS high WITH 0.412',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS veryHigh WITH 0.855',
      'IF x1 IS veryLow AND x2 IS low THEN y IS veryLow WITH 0.437',
      'IF x1 IS veryLow AND x2 IS low THEN y IS low WITH 0.470',
      'IF x1 IS veryLow AND x2 IS low THEN y IS medium WITH 0.223',
      'IF x1 IS veryLow AND x2 IS low THEN y IS high WITH 0.423',
      'IF x1 IS veryLow AND x2 IS low THEN y IS veryHigh WITH 0.852',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS veryLow WITH 0.935',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS low WITH 0.098',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS medium WITH 0.717',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS high WITH 0.959',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS veryHigh WITH 0.838',
      'IF x1 IS veryLow AND x2 IS high THEN y IS veryLow WITH 0.202',
      'IF x1 IS veryLow AND x2 IS high THEN y IS low WITH 0.685',
      'IF x1 IS veryLow AND x2 IS high THEN y IS medium WITH 0.201',
      'IF x1 IS veryLow AND x2 IS high THEN y IS high WITH 0.939',
      'IF x1 IS veryLow AND x2 IS high THEN y IS veryHigh WITH 0.406',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS veryLow WITH 0.373',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS low WITH 1.000',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS medium WITH 0.487',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS high WITH 0.235',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS veryHigh WITH 0.866',
      'IF x1 IS low AND x2 IS veryLow THEN y IS veryLow WITH 0.238',
      'IF x1 IS low AND x2 IS veryLow THEN y IS low WITH 0.116',
      'IF x1 IS low AND x2 IS veryLow THEN y IS medium WITH 0.874',
      'IF x1 IS low AND x2 IS veryLow THEN y IS high WITH 0.471',
      'IF x1 IS low AND x2 IS veryLow THEN y IS veryHigh WITH 0.200',
      'IF x1 IS low AND x2 IS low THEN y IS veryLow WITH 0.863',
      'IF x1 IS low AND x2 IS low THEN y IS low WITH 0.602',
      'IF x1 IS low AND x2 IS low THEN y IS medium WITH 0.021',
      'IF x1 IS low AND x2 IS low THEN y IS high WITH 0.249',
      'IF x1 IS low AND x2 IS low THEN y IS veryHigh WITH 0.994',
      'IF x1 IS low AND x2 IS medium THEN y IS veryLow WITH 0.760',
      'IF x1 IS low AND x2 IS medium THEN y IS low WITH 0.061',
      'IF x1 IS low AND x2 IS medium THEN y IS medium WITH 0.892',
      'IF x1 IS low AND x2 IS medium THEN y IS high WITH 0.709',
      'IF x1 IS low AND x2 IS medium THEN y IS veryHigh WITH 0.650',
      'IF x1 IS low AND x2 IS high THEN y IS veryLow WITH 0.186',
      'IF x1 IS low AND x2 IS high THEN y IS low WITH 0.091',
      'IF x1 IS low AND x2 IS high THEN y IS medium WITH 0.885',
      'IF x1 IS low AND x2 IS high THEN y IS high WITH 0.566',
      'IF x1 IS low AND x2 IS high THEN y IS veryHigh WITH 0.343',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS veryLow WITH 0.537',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS low WITH 0.979',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS medium WITH 0.732',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS high WITH 0.672',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS veryHigh WITH 0.180',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS veryLow WITH 0.826',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS low WITH 0.713',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS medium WITH 0.630',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS high WITH 0.071',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS veryHigh WITH 0.503',
      'IF x1 IS medium AND x2 IS low THEN y IS veryLow WITH 0.278',
      'IF x1 IS medium AND x2 IS low THEN y IS low WITH 0.685',
      'IF x1 IS medium AND x2 IS low THEN y IS medium WITH 0.931',
      'IF x1 IS medium AND x2 IS low THEN y IS high WITH 0.200',
      'IF x1 IS medium AND x2 IS low THEN y IS veryHigh WITH 0.584',
      'IF x1 IS medium AND x2 IS medium THEN y IS veryLow WITH 0.343',
      'IF x1 IS medium AND x2 IS medium THEN y IS low WITH 0.102',
      'IF x1 IS medium AND x2 IS medium THEN y IS medium WITH 0.098',
      'IF x1 IS medium AND x2 IS medium THEN y IS high WITH 0.527',
      'IF x1 IS medium AND x2 IS medium THEN y IS veryHigh WITH 0.569',
      'IF x1 IS medium AND x2 IS high THEN y IS veryLow WITH 0.967',
      'IF x1 IS medium AND x2 IS high THEN y IS low WITH 0.125',
      'IF x1 IS medium AND x2 IS high THEN y IS medium WITH 0.546',
      'IF x1 IS medium AND x2 IS high THEN y IS high WITH 0.164',
      'IF x1 IS medium AND x2 IS high THEN y IS veryHigh WITH 0.597',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS veryLow WITH 0.662',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS low WITH 0.044',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS medium WITH 0.041',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS high WITH 0.194',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS veryHigh WITH 0.327',
      'IF x1 IS high AND x2 IS veryLow THEN y IS veryLow WITH 0.053',
      'IF x1 IS high AND x2 IS veryLow THEN y IS low WITH 0.567',
      'IF x1 IS high AND x2 IS veryLow THEN y IS medium WITH 0.030',
      'IF x1 IS high AND x2 IS veryLow THEN y IS high WITH 0.182',
      'IF x1 IS high AND x2 IS veryLow THEN y IS veryHigh WITH 0.041',
      'IF x1 IS high AND x2 IS low THEN y IS veryLow WITH 0.138',
      'IF x1 IS high AND x2 IS low THEN y IS low WITH 0.139',
      'IF x1 IS high AND x2 IS low THEN y IS medium WITH 0.212',
      'IF x1 IS high AND x2 IS low THEN y IS high WITH 0.116',
      'IF x1 IS high AND x2 IS low THEN y IS veryHigh WITH 0.164',
      'IF x1 IS high AND x2 IS medium THEN y IS veryLow WITH 0.535',
      'IF x1 IS high AND x2 IS medium THEN y IS low WITH 0.028',
      'IF x1 IS high AND x2 IS medium THEN y IS medium WITH 0.416',
      'IF x1 IS high AND x2 IS medium THEN y IS high WITH 0.087',
      'IF x1 IS high AND x2 IS medium THEN y IS veryHigh WITH 0.525',
      'IF x1 IS high AND x2 IS high THEN y IS veryLow WITH 0.430',
      'IF x1 IS high AND x2 IS high THEN y IS low WITH 0.452',
      'IF x1 IS high AND x2 IS high THEN y IS medium WITH 0.220',
      'IF x1 IS high AND x2 IS high THEN y IS high WITH 0.475',
      'IF x1 IS high AND x2 IS high THEN y IS veryHigh WITH 0.500',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS veryLow WITH 0.587',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS low WITH 0.057',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS medium WITH 0.829',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS high WITH 0.109',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS veryHigh WITH 0.752',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS veryLow WITH 0.599',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS low WITH 0.343',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS medium WITH 0.500',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS high WITH 0.817',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS veryHigh WITH 0.656',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS veryLow WITH 0.036',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS low WITH 0.264',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS medium WITH 0.033',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS high WITH 0.357',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS veryHigh WITH 0.345',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS veryLow WITH 0.643',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS low WITH 0.645',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS medium WITH 0.713',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS high WITH 0.651',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS veryHigh WITH 0.935',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS veryLow WITH 0.612',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS low WITH 0.380',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS medium WITH 0.002',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS high WITH 0.722',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS veryHigh WITH 0.312',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS veryLow WITH 0.084',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS low WITH 0.943',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS medium WITH 0.896',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS high WITH 0.031',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS veryHigh WITH 0.501'
    ]
  }, {
    x1: 4.595055698417127,
    x2: 3.8349352637305856
  }
    , {
    'it works' (err, results) {
      assert.ifError(err)
    }
  }
  )).export(module)

// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const polygonBatch = require('./polygonbatch')

vows
  .describe('One trapezoid inside the other')
  .addBatch(polygonBatch([
    [[0, 0], [2, 2], [4, 2], [6, 0]],
    [[1, 0], [2, 1], [4, 1], [5, 0]]
  ], {
    'it looks correct' (err, results) {
      assert.ifError(err)
      assert.isArray(results)
      assert.lengthOf(results, 1)
      assert.deepEqual(results[0], [[0, 0], [2, 2], [4, 2], [6, 0]])
    }
  }))
  .export(module)

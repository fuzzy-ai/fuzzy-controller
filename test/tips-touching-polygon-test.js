/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const polygonBatch = require('./polygonbatch')

vows
  .describe('Two triangles barely touching')
  .addBatch(polygonBatch([
    [[0, 0], [1, 1], [2, 0]],
    [[2, 0], [3, 1], [4, 0]]
  ], {
    'it looks correct' (err, results) {
      assert.ifError(err)
      assert.isArray(results)
      assert.lengthOf(results, 2)
      assert.deepEqual(results[0], [[0, 0], [1, 1], [2, 0]])
      assert.deepEqual(results[1], [[2, 0], [3, 1], [4, 0]])
    }
  }))
  .export(module)

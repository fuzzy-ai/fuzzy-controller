// Copyright 2014-2017 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

class LoopError extends Error {
  constructor (wrapped, num, fuzzified, output, clipped) {
    super(`${wrapped.message} (num: ${num} fuzzified: ${JSON.stringify(fuzzified)} output: ${JSON.stringify(output)} clipped: ${JSON.stringify(clipped)})`)
    this.wrapped = wrapped
    this.num = num
    this.fuzzified = fuzzified
    this.output = output
    this.clipped = clipped
  }
}

module.exports = LoopError

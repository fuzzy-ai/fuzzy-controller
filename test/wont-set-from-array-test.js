// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

vows
  .describe('Error setting to-from array')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate a controller': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                x: {
                  veryLow: [
                    0,
                    25
                  ],
                  low: [
                    0,
                    25,
                    50
                  ],
                  medium: [
                    25,
                    50,
                    75
                  ],
                  high: [
                    5.50258647427497,
                    30.50258647427497,
                    34.14382797470326
                  ],
                  veryHigh: [
                    25.868528596401084,
                    27.285422326780232
                  ]
                }
              },
              outputs: {
                y: {
                  veryLow: [
                    0,
                    25
                  ],
                  low: [
                    0,
                    25,
                    50
                  ],
                  medium: [
                    25,
                    50,
                    75
                  ],
                  high: [
                    119.65284223798295,
                    119.75284223798295,
                    145.26518879177348
                  ],
                  veryHigh: [
                    117.28875221485293,
                    221.93642428384055
                  ]
                }
              },
              rules: [
                'x INCREASES y WITH 0.5 0.5 0.5 0 1'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we convert from an array': {
          topic (fc) {
            const { callback } = this
            try {
              const arr = [ 30.50258647427497,
                25,
                3.6412415004282863,
                25,
                25,
                25,
                50,
                25,
                25,
                25.868528596401084,
                1.4168937303791473,
                0,
                25,
                119.75284223798295,
                0.0988903178785751,
                25.512346553790536,
                25,
                25,
                25,
                50,
                25,
                25,
                117.28875221485293,
                104.64767206898762,
                0,
                25,
                0.5,
                0.5,
                0.5,
                0,
                1 ]
              fc.fromArray(arr)
              const results = fc.toArray()
              callback(null, results)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it works' (err, results) {
            assert.ifError(err)
            assert.isArray(results)
          },
          'it looks correct' (err, results) {
            assert.ifError(err)
            assert.isArray(results)
            assert.inDelta(results[14], 0.0988903178785751, 0.001)
          }
        }
      }
    }}).export(module)

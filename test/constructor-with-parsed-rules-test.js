// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')

const LoopError = require('./loop-error')

const testWithArguments = function (instantiator) {
  return {
    topic (FuzzyController) {
      try {
        const fc = instantiator(FuzzyController)
        this.callback(null, fc)
      } catch (err) {
        this.callback(err, null)
      }
      return undefined
    },
    'it works' (err, fc) {
      assert.ifError(err)
      assert.isObject(fc)
    },
    'and we evaluate over the range': {
      topic (fc) {
        const { callback } = this
        const resultses = []
        let f = null
        let o = null
        let c = {}
        let i = null
        try {
          _.each(__range__(-10, 110, true), (num) => {
            i = num
            f = (o = null)
            c = {}
            fc.once('fuzzified', fuzzified => { f = fuzzified })
            fc.once('inferred', output => { o = output })
            fc.once('clipped', (clipped, name) => { c[name] = clipped })
            const results = fc.evaluate({input1: num})
            return resultses.push(results)
          })
          callback(null, resultses)
        } catch (err) {
          callback(new LoopError(err, i, f, o, c), null)
        }
        return undefined
      },
      'it works' (err, resultses) {
        assert.ifError(err)
      }
    }
  }
}

vows
  .describe('Basic structure for FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with an object with a parsed_rules property':
        testWithArguments((FuzzyController) => {
          const args = {
            inputs: {
              input1: {
                veryLow: [0, 20],
                low: [5, 25, 45],
                medium: [30, 50, 70],
                high: [55, 75, 95],
                veryHigh: [80, 100]
              }
            },
            outputs: {
              output1: {
                veryLow: [0, 20],
                low: [5, 25, 45],
                medium: [30, 50, 70],
                high: [55, 75, 95],
                veryHigh: [80, 100]
              }
            },
            parsed_rules: [
              {
                type: 'increases',
                input: 'input1',
                output: 'output1'
              }
            ]
          }
          return new FuzzyController(args)
        }),
      'and we instantiate with an object with a parsed_rules and a rules property':
        testWithArguments((FuzzyController) => {
          const args = {
            inputs: {
              input1: {
                veryLow: [0, 20],
                low: [5, 25, 45],
                medium: [30, 50, 70],
                high: [55, 75, 95],
                veryHigh: [80, 100]
              }
            },
            outputs: {
              output1: {
                veryLow: [0, 20],
                low: [5, 25, 45],
                medium: [30, 50, 70],
                high: [55, 75, 95],
                veryHigh: [80, 100]
              }
            },
            rules: [
              'input1 INCREASES output1'
            ],
            parsed_rules: [
              {
                type: 'increases',
                input: 'input1',
                output: 'output1'
              }
            ]
          }
          return new FuzzyController(args)
        }),
      'and we instantiate with positional arguments with no rules and parsed_rules':
        testWithArguments((FuzzyController) => {
          const inputs = {
            input1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          }
          const outputs = {
            output1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          }
          const parsedRules = [
            {
              type: 'increases',
              input: 'input1',
              output: 'output1'
            }
          ]
          return new FuzzyController(inputs, outputs, null, parsedRules)
        }),

      'and we instantiate with positional arguments with rules and parsed_rules':
        testWithArguments((FuzzyController) => {
          const inputs = {
            input1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          }
          const outputs = {
            output1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          }
          const rules = [
            'input1 INCREASES output1'
          ]
          const parsedRules = [
            {
              type: 'increases',
              input: 'input1',
              output: 'output1'
            }
          ]
          return new FuzzyController(inputs, outputs, rules, parsedRules)
        })
    }}).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}

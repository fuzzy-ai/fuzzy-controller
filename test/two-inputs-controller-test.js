// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')

const LoopError = require('./loop-error')

vows
  .describe('Two inputs with triangles in a FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                },
                input2: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow',
                'IF input1 IS low THEN output1 IS low',
                'IF input1 IS medium THEN output1 IS medium',
                'IF input1 IS high THEN output1 IS high',
                'IF input1 IS veryHigh THEN output1 IS veryHigh',
                'IF input2 IS veryLow THEN output1 IS veryLow',
                'IF input2 IS low THEN output1 IS low',
                'IF input2 IS medium THEN output1 IS medium',
                'IF input2 IS high THEN output1 IS high',
                'IF input2 IS veryHigh THEN output1 IS veryHigh'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we evaluate over both ranges': {
          topic (fc) {
            const { callback } = this
            const resultses = []
            let f = null
            let o = null
            let c = {}
            let pair = null
            try {
              _.each(__range__(-1, 11, true), (dec1) => {
                const num1 = dec1 * 10
                return _.each(__range__(-1, 11, true), (dec2) => {
                  const num2 = dec2 * 10
                  pair = [num1, num2]
                  f = (o = null)
                  c = {}
                  fc.once('fuzzified', fuzzified => { f = fuzzified })
                  fc.once('inferred', output => { o = output })
                  fc.once('clipped', (clipped, name) => { c[name] = clipped })
                  const results = fc.evaluate({input1: num1, input2: num2})
                  resultses.push(results)
                })
              })
              callback(null, resultses)
            } catch (err) {
              callback(new LoopError(err, pair, f, o, c), null)
            }
            return undefined
          },
          'it works' (err, resultses) {
            assert.ifError(err)
          }
        }
      }
    }}).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}

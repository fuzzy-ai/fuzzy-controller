// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

vows
  .describe('Polygon from basic fuzzy controller')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const polygon = require('../lib/polygon')
          callback(null, polygon)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, polygon) {
        assert.ifError(err)
        assert.isObject(polygon)
      },
      'and we get the union of two trapezoids from the basic test': {
        topic (polygon) {
          const { callback } = this
          // It's a trap!
          const trap1 = [[0, 0], [0, 0.15000000000000002], [17, 0.15000000000000002], [20, 0]]
          // It's a trap!
          const trap2 = [[5, 0], [17, 0.6], [33, 0.6], [45, 0]]
          try {
            const p = polygon.union(trap1, trap2)
            callback(null, p)
          } catch (err) {
            callback(err, null)
          }
          return undefined
        },
        'it works' (err, shapes) {
          assert.ifError(err)
        },
        'it looks right' (err, shapes) {
          assert.ifError(err)
          assert.isArray(shapes)
          assert.lengthOf(shapes, 1)
          assert.isArray(shapes[0])
        }
      },
      'and we get a union that causes a circular list in the basic test': {
        topic (polygon) {
          const { callback } = this
          // It's a trap!
          const trap1 = [[5, 0], [17, 0.6], [33, 0.6], [45, 0]]
          // It's a trap!
          const trap2 = [[30, 0], [33, 0.15], [67, 0.15], [70, 0]]
          try {
            const p = polygon.union(trap1, trap2)
            callback(null, p)
          } catch (err) {
            callback(err, null)
          }
          return undefined
        },
        'it works' (err, shapes) {
          assert.ifError(err)
        },
        'it looks right' (err, shapes) {
          assert.ifError(err)
          assert.isArray(shapes)
          assert.lengthOf(shapes, 1)
          assert.isArray(shapes[0])
        }
      }
    }})
  .export(module)

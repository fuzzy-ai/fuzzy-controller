// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')

const polygonBatch = require('./polygonbatch')

vows
  .describe('Polygons from Meltwater controller')
  .addBatch(polygonBatch([
    [
      [-25, 0],
      [0, 3.549999999999999],
      [22.183098591549296, 0.4],
      [40, 0.4],
      [46.50000000000001, 0.13999999999999999],
      [71.5, 0.13999999999999999],
      [75, 0]
    ],
    [
      [50, 0],
      [50.00000033757716, 1.3503086440591183e-8],
      [99.99999966242284, 1.3503086440591183e-8],
      [100, 0]
    ]
  ]))
  .export(module)

// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const LoopError = require('./loop-error')

vows
  .describe('Two inputs with triangles in a FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                },
                input2: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow',
                'IF input1 IS low THEN output1 IS low',
                'IF input1 IS medium THEN output1 IS medium',
                'IF input1 IS high THEN output1 IS high',
                'IF input1 IS veryHigh THEN output1 IS veryHigh',
                'IF input2 IS veryLow THEN output1 IS veryLow',
                'IF input2 IS low THEN output1 IS low',
                'IF input2 IS medium THEN output1 IS medium',
                'IF input2 IS high THEN output1 IS high',
                'IF input2 IS veryHigh THEN output1 IS veryHigh'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we evaluate on opposite ends': {
          topic (fc) {
            const { callback } = this
            let f = null
            let o = null
            const c = {}
            const cb = {}
            const pair = null
            try {
              fc.once('fuzzified', fuzzified => { f = fuzzified })
              fc.once('inferred', output => { o = output })
              fc.once('clipped', (clipped, name) => { c[name] = clipped })
              fc.once('combined', (combined, name) => { cb[name] = combined })
              const results = fc.evaluate({input1: 10, input2: 90})
              callback(null, {fuzzified: f, output: o, clipped: c, combined: cb, results})
            } catch (err) {
              callback(new LoopError(err, pair, f, o, c), null)
            }
            return undefined
          },
          'it works' (err, results) {
            assert.ifError(err)
            assert.inDelta(50, results.results.output1, 1)
          }
        }
      }
    }}).export(module)

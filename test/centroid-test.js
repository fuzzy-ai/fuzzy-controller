/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const debug = require('debug')('fuzzy-controller:centroid-test')

const TOLERANCE = 0.01

const js = JSON.stringify

const centroidBatch = function (points, expected) {
  return {
    topic (centroid) {
      const { callback } = this
      try {
        const results = centroid(points)
        debug(`centroid(${js(points)}) = ${js(results)}`)
        callback(null, results)
      } catch (err) {
        debug(`centroid(${js(points)}) caused error ${js(err)}`)
        callback(err)
      }
      return undefined
    },
    'it works' (err, results) {
      assert.ifError(err)
      assert.isArray(results)
      assert.lengthOf(results, 2)
      assert.isNumber(results[0])
      assert.isNumber(results[1])
      assert.epsilon(results[0], expected[0], TOLERANCE)
      assert.epsilon(results[1], expected[1], TOLERANCE)
    }
  }
}

vows
  .describe('Centroid')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const centroid = require('../lib/centroid')
          callback(null, centroid)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, centroid) {
        assert.ifError(err)
        assert.isFunction(centroid)
      },
      'and we call the centroid function on a rectangle':
        centroidBatch([[0, 0], [0, 2], [2, 2], [2, 0]], [1, 1]),
      'and we call the centroid function on a downward slope':
        centroidBatch([[0, 0], [0, 1], [1, 0]], [1 / 3, 1 / 3]),
      'and we call the centroid function on an upward slope':
        centroidBatch([[0, 0], [1, 1], [1, 0]], [2 / 3, 1 / 3]),
      'and we call the centroid function on an elevated upward slope':
        centroidBatch([[1, 0], [1, 1], [2, 2], [2, 0]],
          [((1.5 * 1) + ((5 / 3) * 0.5)) / 1.5, ((0.5 * 1) + ((4 / 3) * 0.5)) / 1.5]),
      'and we call the centroid function on an elevated downward slope':
        centroidBatch([[1, 0], [1, 2], [2, 1], [2, 0]],
          [((1.5 * 1) + ((4 / 3) * 0.5)) / 1.5, ((0.5 * 1) + ((4 / 3) * 0.5)) / 1.5]),
      'and we call the centroid function on a trapezoid':
        centroidBatch([[0, 0], [1, 1], [2, 1], [3, 0]],
          [(((2 / 3) * 0.5) + (1.5 * 1) + ((7 / 3) * 0.5)) / 2, (((1 / 3) * 0.5) + (0.5 * 1) + ((1 / 3) * 0.5)) / 2]),
      'and we call the centroid function on a combination of two trapezoids':
        centroidBatch([[ 0, 0 ], [ 1, 1 / 3 ], [ 5, 1 / 3 ], [ 5.25, 0.25 ], [ 14, 0.25 ], [ 16, 0 ]], [7.28, 0.14]),
      'and we call the centroid function on a combination from the slope test':
        centroidBatch([[0, 0], [6.25, 0.25], [43.75, 0.25], [43.750125, 0.249995], [68.750125, 0.249995], [75, 0]], [37.5, 0.12])
    }})
  .export(module)

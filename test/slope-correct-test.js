/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')
const debug = require('debug')('fuzzy-controller:slope-correct-test')

const BOUNDARY = 100
const js = JSON.stringify

vows
  .describe('Slope correctness')
  .addBatch({
    'When we generate a slope manually and with the slopeAt() method for a set parameter': {
      topic () {
        const initial = {
          inputs: {
            x: {
              veryLow: [0, BOUNDARY / 4],
              low: [0, BOUNDARY / 4, BOUNDARY / 2],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          outputs: {
            y: {
              veryLow: [0, BOUNDARY / 4],
              low: [0, BOUNDARY / 4, BOUNDARY / 2],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          rules: [
            'x INCREASES y WITH 0.5'
          ]
        }

        const initial1 = {
          inputs: {
            x: {
              veryLow: [0, BOUNDARY / 4],
              low: [-0.0005, 24.9995, 49.9995],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          outputs: {
            y: {
              veryLow: [0, BOUNDARY / 4],
              low: [0, BOUNDARY / 4, BOUNDARY / 2],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          rules: [
            'x INCREASES y WITH 0.5'
          ]
        }

        const initial2 = {
          inputs: {
            x: {
              veryLow: [0, BOUNDARY / 4],
              low: [0.0005, 25.0005, 50.0005],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          outputs: {
            y: {
              veryLow: [0, BOUNDARY / 4],
              low: [0, BOUNDARY / 4, BOUNDARY / 2],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          rules: [
            'x INCREASES y WITH 0.5'
          ]
        }

        try {
          const FuzzyController = require('../lib/fuzzy-controller')

          const fc = new FuzzyController(initial)
          const fc1 = new FuzzyController(initial1)
          const fc2 = new FuzzyController(initial2)

          const slope = fc.slopeAt(3, 25, {x: 30})

          const output1 = fc1.evaluate({x: 30})
          const output2 = fc2.evaluate({x: 30})

          this.callback(null, slope, output1, output2)
        } catch (err) {
          this.callback(err)
        }

        return undefined
      },

      'it works' (err, slope, output1, output2) {
        assert.ifError(err)
      },

      'the slope looks roughly correct' (err, slope, output1, output2) {
        assert.ifError(err)
        assert.inDelta(slope.y, (output2.y - output1.y) / 0.001, 0.001)
      }
    }}).addBatch({
    'When we generate a slope manually and with the slopeAt() method for a rule parameter': {
      topic () {
        let value
        const initial = {
          inputs: {
            x: {
              veryLow: [0, BOUNDARY / 4],
              low: [0, BOUNDARY / 4, BOUNDARY / 2],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          outputs: {
            y: {
              veryLow: [0, BOUNDARY / 4],
              low: [0, BOUNDARY / 4, BOUNDARY / 2],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          rules: [
            'x INCREASES y WITH 0.5'
          ]
        }

        const delta1 = {
          'rules[0]': 'x INCREASES y WITH 0.5 0.5 0.49999 0.5 0.5'
        }

        const delta2 = {
          'rules[0]': 'x INCREASES y WITH 0.5 0.5 0.50001 0.5 0.5'
        }

        const initial1 = _.cloneDeep(initial)

        for (const path in delta1) {
          value = delta1[path]
          _.set(initial1, path, value)
        }

        const initial2 = _.cloneDeep(initial)

        for (const path in delta2) {
          value = delta2[path]
          _.set(initial2, path, value)
        }

        try {
          const FuzzyController = require('../lib/fuzzy-controller')

          const fc = new FuzzyController(initial)
          const fc1 = new FuzzyController(initial1)
          const fc2 = new FuzzyController(initial2)

          const slope = fc.slopeAt(28, 0.5, {x: (3 * BOUNDARY) / 8})

          const output1 = fc1.evaluate({x: (3 * BOUNDARY) / 8})
          const output2 = fc2.evaluate({x: (3 * BOUNDARY) / 8})

          this.callback(null, slope, output1, output2)
        } catch (err) {
          this.callback(err)
        }

        return undefined
      },

      'it works' (err, slope, output1, output2) {
        assert.ifError(err)
      },

      'the slope looks roughly correct' (err, slope, output1, output2) {
        assert.ifError(err)
        debug(`slope = ${js(slope)}, output1 = ${js(output1)}, output2 = ${js(output2)}`)
        assert.inDelta(slope.y, (output2.y - output1.y) / 0.00002, 0.1)
      }
    }}).export(module)

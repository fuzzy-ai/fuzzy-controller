/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

vows
  .describe('Polygon')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const polygon = require('../lib/polygon')
          callback(null, polygon)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, polygon) {
        assert.ifError(err)
        assert.isObject(polygon)
      },
      'it has a union member' (err, polygon) {
        assert.ifError(err)
        assert.isFunction(polygon.union)
      },
      'and we get the union of a triangle and a rectangle': {
        topic (polygon) {
          const { callback } = this
          const triangle = [[1, 2], [2, 4], [3, 2]]
          const rectangle = [[2, 1], [2, 3], [4, 3], [4, 1]]
          try {
            const p = polygon.union(rectangle, triangle)
            callback(null, p)
          } catch (err) {
            callback(err, null)
          }
          return undefined
        },
        'it works' (err, shapes) {
          assert.ifError(err)
        },
        'it looks right' (err, shapes) {
          assert.ifError(err)
          assert.isArray(shapes)
          assert.lengthOf(shapes, 1)
          assert.isArray(shapes[0])
          assert.lengthOf(shapes[0], 7)
          assert.deepEqual(shapes[0], [[1, 2], [2, 4], [2.5, 3], [4, 3], [4, 1], [2, 1], [2, 2]])
        }
      },
      'and we get the union of two trapezoids on the X-axis': {
        topic (polygon) {
          const { callback } = this
          // It's a trap!
          const trap1 = [[1, 0], [3, 1], [5, 1], [7, 0]]
          const trap2 = [[5, 0], [7, 1], [9, 1], [11, 0]]
          try {
            const p = polygon.union(trap1, trap2)
            callback(null, p)
          } catch (err) {
            callback(err, null)
          }
          return undefined
        },
        'it works' (err, shapes) {
          assert.ifError(err)
        },
        'it looks right' (err, shapes) {
          assert.ifError(err)
          assert.isArray(shapes)
          assert.lengthOf(shapes, 1)
          assert.isArray(shapes[0])
          assert.deepEqual(shapes[0], [[1, 0], [3, 1], [5, 1], [6, 0.5], [7, 1], [9, 1], [11, 0]])
        }
      },
      'and we get the union of a triangle and a trapezoid from the module test': {
        topic (polygon) {
          const { callback } = this
          // It's a trap!
          const trap = [[50, 0], [83.33333333333334, 0.6666666666666667], [166.66666666666666, 0.6666666666666667], [200, 0]]
          // It's a tri!
          const tri = [[150, 0], [166.66666666666666, 0.3333333333333333], [200, 0.3333333333333333], [200, 0]]
          try {
            const p = polygon.union(trap, tri)
            callback(null, p)
          } catch (err) {
            callback(err, null)
          }
          return undefined
        },
        'it works' (err, shapes) {
          assert.ifError(err)
        },
        'it looks right' (err, shapes) {
          assert.ifError(err)
          assert.isArray(shapes)
          assert.lengthOf(shapes, 1)
          assert.isArray(shapes[0])
        }
      },
      'and we get the union of two triangles weirdly defined from the social example': {
        topic (polygon) {
          const { callback } = this
          const tri1 = [[30, 0], [50, 1], [50, 1], [70, 0]]
          const tri2 = [[55, 0], [75, 1], [75, 1], [95, 0]]
          try {
            // eslint-disable-next-line no-unused-vars
            const p = polygon.union(tri1, tri2)
            callback(new Error('Unexpected success'))
          } catch (err) {
            callback(null)
          }
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        }
      }
    }}).export(module)

// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const segment = require('../lib/segment')

const s1 = [
  [ 1.6214465335487536, 0 ],
  [ 1.7955576366795136, 0.3850561396619225 ]
]

const s2 = [
  [ 1.7476309316439673, 0.4909723746423459 ],
  [ 1.969794345806172, 0 ]
]

vows
  .describe('Weird segment intersection')
  .addBatch({
    'When we intersect two problematic points': {
      topic () {
        const { callback } = this
        try {
          const p = segment.intersect(s1, s2)
          callback(null, p)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, pt) {
        assert.ifError(err)
        assert.isArray(pt)
        assert.isNumber(pt[0])
        assert.isNumber(pt[1])
      },
      'it looks correct' (err, pt) {
        assert.ifError(err)
        assert(pt[0] >= s1[0][0])
        assert(pt[0] <= s1[1][0])
        assert(pt[1] >= s1[0][1])
        assert(pt[1] <= s1[1][1])
        assert(pt[0] >= s2[0][0])
        assert(pt[0] <= s2[1][0])
        assert(pt[1] <= s2[0][1])
        assert(pt[1] >= s2[1][1])
      }
    }})
  .export(module)

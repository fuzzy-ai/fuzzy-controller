/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

vows
  .describe('Serialization for FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                temperature: {
                  cold: [50, 75],
                  normal: [50, 75, 85, 100],
                  hot: [85, 100]
                }
              },
              outputs: {
                fanSpeed: {
                  slow: [50, 100],
                  normal: [50, 100, 150, 200],
                  fast: [150, 200]
                }
              },
              rules: [
                'IF temperature IS cold THEN fanSpeed IS slow',
                'IF temperature IS normal THEN fanSpeed IS normal',
                'IF temperature IS hot THEN fanSpeed IS fast'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we serialize to JSON': {
          topic (fc) {
            const { callback } = this
            try {
              const str = JSON.stringify(fc)
              callback(null, str)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it works' (err, str) {
            assert.ifError(err)
            assert.isString(str)
          },
          'and we reconstitute the object': {
            topic (json, orig, FuzzyController) {
              const { callback } = this
              try {
                const fc = new FuzzyController(JSON.parse(json))
                callback(null, fc)
              } catch (err) {
                callback(err)
              }
              return undefined
            },
            'it works' (err, fc) {
              assert.ifError(err)
              assert.isObject(fc)
            },
            'and we evaluate': {
              topic (fc) {
                const { callback } = this
                let crisp = null
                try {
                  crisp = fc.evaluate({temperature: 60})
                  callback(null, crisp)
                } catch (err) {
                  callback(err)
                }
                return undefined
              },
              'we get the correct output' (err, crisp) {
                assert.ifError(err)
                assert.isObject(crisp)
                assert.isNumber(crisp['fanSpeed'])
                assert.inDelta(crisp['fanSpeed'], 93, 2)
              }
            }
          }
        }
      }
    }}).export(module)

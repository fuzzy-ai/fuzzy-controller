// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')

const toFromBatch = function (initial, expected, delta, newexpected, path, value) {
  const batch = {
    topic (FuzzyController) {
      try {
        const fc = new FuzzyController(initial)
        this.callback(null, fc)
      } catch (err) {
        this.callback(err, null)
      }
      return undefined
    },
    'it works' (err, fc) {
      assert.ifError(err)
      assert.isObject(fc)
    },
    'it has a toArray() method' (err, fc) {
      assert.ifError(err)
      assert.isObject(fc)
      assert.isFunction(fc.toArray)
    },
    'it has a fromArray() method' (err, fc) {
      assert.ifError(err)
      assert.isObject(fc)
      assert.isFunction(fc.fromArray)
    },
    'and we convert to an array': {
      topic (fc) {
        const { callback } = this
        try {
          const results = fc.toArray()
          callback(null, results)
        } catch (err) {
          callback(err)
        }
        return undefined
      },
      'it works' (err, results) {
        assert.ifError(err)
        assert.isArray(results)
      },
      'it looks correct' (err, results) {
        assert.ifError(err)
        assert.isArray(results)
        assert.equal(results.length, expected.length, `Results not of expected length (${results.length} != ${expected.length})`)
        return Array.from(results).map((f, i) =>
          assert.inDelta(f, expected[i], 0.0001, `Incorrect value at index ${i}: ${f} != ${expected[i]}`))
      },
      'and we convert from an array': {
        topic (arr, fc) {
          const { callback } = this
          try {
            for (const key in delta) {
              value = delta[key]
              arr[parseInt(key, 10)] = value
            }
            fc.fromArray(arr)
            callback(null, fc)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
        },
        // FIXME: check that values were changed
        'and we convert to an array again': {
          topic (fc) {
            const { callback } = this
            try {
              const arr = fc.toArray()
              callback(null, arr)
            } catch (err) {
              callback(err)
            }
            return undefined
          },
          'it looks correct' (err, results) {
            assert.ifError(err)
            assert.isArray(results)
            assert.equal(results.length, newexpected.length, `Results not of expected length (${results.length} != ${expected.length})`)
            return Array.from(results).map((f, i) =>
              assert.inDelta(f, newexpected[i], 0.0001, `Incorrect value at index ${i}: ${f} != ${newexpected[i]}`))
          }
        }
      }
    }
  }

  if (path) {
    batch['and we convert to an array']['and we convert from an array']['and we convert to JSON'] = {
      topic (fc) {
        try {
          const json = fc.toJSON()
          this.callback(null, json)
        } catch (err) {
          this.callback(err)
        }
        return undefined
      },
      'it looks correct' (err, json) {
        assert.ifError(err)
        assert.inDelta(_.get(json, path), value, 0.01)
      }
    }
  }
  return batch
}

const UNMODIFIED = [75, 20, 20, 25, 20, 20, 50, 20, 20, 80, 20, 0, 20, 75, 20, 20, 25, 20, 20, 50, 20, 20, 80, 20, 0, 20, 0.5, 0.5, 0.5, 0.5, 0.5]
const MODIFIED = [75, 20, 20, 25, 20, 20, 55, 20, 20, 80, 20, 0, 20, 75, 20, 20, 25, 20, 20, 50, 20, 20, 80, 20, 0, 20, 0.5, 0.5, 0.5, 0.5, 0.75]
const WITHILLEGAL = [75, 20, 0.0001, 25, 20, 20, 55, 20, 20, 80, 20, 0, 20, 75, 20, 20, 25, 20, 20, 50, 20, 20, 80, 20, 0, 20, 0.0077, 0.0077, 0.0077, 0.0, 1.0]

vows
  .describe('toArray() and fromArray() test')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with IF THEN rules':
        toFromBatch({
          inputs: {
            input1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          outputs: {
            output1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          rules: [
            'IF input1 IS veryLow THEN output1 IS veryLow WITH 0.5',
            'IF input1 IS low THEN output1 IS low WITH 0.5',
            'IF input1 IS medium THEN output1 IS medium WITH 0.5',
            'IF input1 IS high THEN output1 IS high WITH 0.5',
            'IF input1 IS veryHigh THEN output1 IS veryHigh WITH 0.5'
          ]
        },
        UNMODIFIED,
        {6: 55, 30: 0.75},
        MODIFIED,
        'parsed_rules[4].weight',
        0.75),
      'and we instantiate with an INCREASES rule':
        toFromBatch({
          inputs: {
            input1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          outputs: {
            output1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          rules: [
            'input1 INCREASES output1 WITH 0.5'
          ]
        },
        UNMODIFIED,
        {6: 55, 30: 0.75},
        MODIFIED,
        'parsed_rules[0].weight[4]',
        0.75),
      'and we instantiate with a DECREASES rule':
        toFromBatch({
          inputs: {
            input1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          outputs: {
            output1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          rules: [
            'input1 DECREASES output1 WITH 0.5'
          ]
        },
        UNMODIFIED,
        {6: 55, 30: 0.75},
        MODIFIED,
        'parsed_rules[0].weight[4]',
        0.75),
      'and we instantiate a controller':
        toFromBatch({
          inputs: {
            input1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          outputs: {
            output1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          rules: [
            'input1 DECREASES output1 WITH 0.5'
          ]
        },
        UNMODIFIED,
        {2: -75, 6: 55, 29: -0.9, 30: 180.3},
        WITHILLEGAL),
      'and we instantiate a controller with IF-THEN rules':
        toFromBatch({
          inputs: {
            input1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          outputs: {
            output1: {
              veryLow: [0, 20],
              low: [5, 25, 45],
              medium: [30, 50, 70],
              high: [55, 75, 95],
              veryHigh: [80, 100]
            }
          },
          rules: [
            'IF input1 IS veryLow THEN output1 IS veryLow WITH 0.5',
            'IF input1 IS low THEN output1 IS low WITH 0.5',
            'IF input1 IS medium THEN output1 IS medium WITH 0.5',
            'IF input1 IS high THEN output1 IS high WITH 0.5',
            'IF input1 IS veryHigh THEN output1 IS veryHigh WITH 0.5'
          ]
        },
        UNMODIFIED,
        {2: -75, 6: 55, 29: -0.9, 30: 180.3},
        WITHILLEGAL),
      'and we compare values returned between two versions of a controller': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'input1 INCREASES output1 WITH 0.5'
              ]})
            const arr = fc.toArray()
            const first = fc.evaluate({input1: 85})
            arr[arr.length - 1] = 0.75
            fc.fromArray(arr)
            const second = fc.evaluate({input1: 85})
            this.callback(null, first, second)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, first, second) {
          assert.ifError(err)
        },
        'the second is higher than the first' (err, first, second) {
          assert.ifError(err)
          assert.isObject(first)
          assert.isObject(second)
          assert.greater(second.output1, first.output1)
        }
      }
    }}).export(module)

// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const controllerBatch = require('./controllerbatch')

vows
  .describe('Better performance with a downward slope')
  .addBatch(controllerBatch({
    inputs: {
      input1: {
        veryLow: [0, 25],
        low: [0, 25, 50],
        medium: [25, 50, 75],
        high: [50, 75, 100],
        veryHigh: [75, 100]
      },
      input2: {
        veryLow: [0, 25],
        low: [0, 25, 50],
        medium: [25, 50, 75],
        high: [50, 75, 100],
        veryHigh: [75, 100]
      }
    },
    outputs: {
      output1: {
        veryLow: [0, 25],
        low: [0, 25, 50],
        medium: [25, 50, 75],
        high: [50, 75, 100],
        veryHigh: [75, 100]
      }
    },
    rules: [
      'input1 INCREASES output1 WITH 0.5',
      'input2 DECREASES output1 WITH 0.5'
    ]
  },
  {input1: 50}
    , {
    'the output is a number' (err, results) {
      assert.ifError(err)
      assert.isNumber(results.output1)
    }
  }
  )).export(module)

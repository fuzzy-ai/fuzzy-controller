/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const controllerBatch = require('./controllerbatch')

vows
  .describe('A controller with no rules works')
  .addBatch(controllerBatch({
    inputs: {
      input1: {
        low: [
          0,
          50
        ],
        medium: [
          0,
          50,
          100
        ],
        high: [
          50,
          100
        ]
      }
    },
    outputs: {
      output1: {
        low: [
          0,
          50
        ],
        medium: [
          0,
          50,
          100
        ],
        high: [
          50,
          100
        ]
      }
    },
    rules: [
    ]
  },
  {input1: 0}
    , {
    'the output is null' (err, results) {
      assert.ifError(err)
      assert.isNull(results.output1)
    }
  }
  )).export(module)

// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')

const controllerBatch = require('./controllerbatch')

vows
  .describe('Model where shapes get circular')
  .addBatch(controllerBatch({
    inputs: {
      x1: {
        veryLow: [
          1.345286829050488,
          2.0322892693802714
        ],
        low: [
          1.487212624400854,
          2.1885652987392787,
          2.8657917892560363
        ],
        medium: [
          2.3423758298158646,
          3.012825943485426,
          3.646324096247554
        ],
        high: [
          3.1587912682443857,
          3.7849111081296734,
          4.497542763128877
        ],
        veryHigh: [
          3.9502562284469604,
          4.658885963552872
        ]
      },
      x2: {
        veryLow: [
          1.4117276529024032,
          2.049736392684281
        ],
        low: [
          1.55851028021425,
          2.196743935173042,
          2.916892006061971
        ],
        medium: [
          2.346976631321013,
          3.084461251776269,
          3.7144492706283927
        ],
        high: [
          3.229278424754739,
          3.8551309327947654,
          4.468343997374177
        ],
        veryHigh: [
          3.994194332510233,
          4.606015178535882
        ]
      }
    },
    outputs: {
      y: {
        veryLow: [
          1.5172975748339925,
          1.969794345806172
        ],
        low: [
          1.6214465335487536,
          2.073617230745346,
          2.73358855696546
        ],
        medium: [
          2.226574399528671,
          2.8828322036376495,
          3.7633096238395307
        ],
        high: [
          3.0902115445405864,
          3.966825575042815,
          5.5919758991518895
        ],
        veryHigh: [
          4.337863391939131,
          5.943845878100661
        ]
      }
    },
    rules: [
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS veryLow WITH 0.482',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS low WITH 0.955',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS medium WITH 0.127',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS high WITH 0.492',
      'IF x1 IS veryLow AND x2 IS veryLow THEN y IS veryHigh WITH 0.992',
      'IF x1 IS veryLow AND x2 IS low THEN y IS veryLow WITH 0.755',
      'IF x1 IS veryLow AND x2 IS low THEN y IS low WITH 0.734',
      'IF x1 IS veryLow AND x2 IS low THEN y IS medium WITH 0.255',
      'IF x1 IS veryLow AND x2 IS low THEN y IS high WITH 0.160',
      'IF x1 IS veryLow AND x2 IS low THEN y IS veryHigh WITH 0.726',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS veryLow WITH 0.708',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS low WITH 0.286',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS medium WITH 0.649',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS high WITH 0.254',
      'IF x1 IS veryLow AND x2 IS medium THEN y IS veryHigh WITH 0.151',
      'IF x1 IS veryLow AND x2 IS high THEN y IS veryLow WITH 0.429',
      'IF x1 IS veryLow AND x2 IS high THEN y IS low WITH 0.694',
      'IF x1 IS veryLow AND x2 IS high THEN y IS medium WITH 0.311',
      'IF x1 IS veryLow AND x2 IS high THEN y IS high WITH 0.175',
      'IF x1 IS veryLow AND x2 IS high THEN y IS veryHigh WITH 0.093',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS veryLow WITH 0.439',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS low WITH 0.862',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS medium WITH 0.985',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS high WITH 0.113',
      'IF x1 IS veryLow AND x2 IS veryHigh THEN y IS veryHigh WITH 0.729',
      'IF x1 IS low AND x2 IS veryLow THEN y IS veryLow WITH 0.117',
      'IF x1 IS low AND x2 IS veryLow THEN y IS low WITH 0.392',
      'IF x1 IS low AND x2 IS veryLow THEN y IS medium WITH 0.606',
      'IF x1 IS low AND x2 IS veryLow THEN y IS high WITH 0.771',
      'IF x1 IS low AND x2 IS veryLow THEN y IS veryHigh WITH 0.080',
      'IF x1 IS low AND x2 IS low THEN y IS veryLow WITH 0.826',
      'IF x1 IS low AND x2 IS low THEN y IS low WITH 0.339',
      'IF x1 IS low AND x2 IS low THEN y IS medium WITH 0.055',
      'IF x1 IS low AND x2 IS low THEN y IS high WITH 0.069',
      'IF x1 IS low AND x2 IS low THEN y IS veryHigh WITH 0.627',
      'IF x1 IS low AND x2 IS medium THEN y IS veryLow WITH 0.990',
      'IF x1 IS low AND x2 IS medium THEN y IS low WITH 0.469',
      'IF x1 IS low AND x2 IS medium THEN y IS medium WITH 0.471',
      'IF x1 IS low AND x2 IS medium THEN y IS high WITH 0.017',
      'IF x1 IS low AND x2 IS medium THEN y IS veryHigh WITH 0.098',
      'IF x1 IS low AND x2 IS high THEN y IS veryLow WITH 0.715',
      'IF x1 IS low AND x2 IS high THEN y IS low WITH 0.581',
      'IF x1 IS low AND x2 IS high THEN y IS medium WITH 0.831',
      'IF x1 IS low AND x2 IS high THEN y IS high WITH 0.045',
      'IF x1 IS low AND x2 IS high THEN y IS veryHigh WITH 0.262',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS veryLow WITH 0.338',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS low WITH 0.158',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS medium WITH 0.688',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS high WITH 0.049',
      'IF x1 IS low AND x2 IS veryHigh THEN y IS veryHigh WITH 0.057',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS veryLow WITH 0.881',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS low WITH 0.866',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS medium WITH 0.574',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS high WITH 0.520',
      'IF x1 IS medium AND x2 IS veryLow THEN y IS veryHigh WITH 0.419',
      'IF x1 IS medium AND x2 IS low THEN y IS veryLow WITH 0.690',
      'IF x1 IS medium AND x2 IS low THEN y IS low WITH 0.448',
      'IF x1 IS medium AND x2 IS low THEN y IS medium WITH 0.301',
      'IF x1 IS medium AND x2 IS low THEN y IS high WITH 0.574',
      'IF x1 IS medium AND x2 IS low THEN y IS veryHigh WITH 0.421',
      'IF x1 IS medium AND x2 IS medium THEN y IS veryLow WITH 0.134',
      'IF x1 IS medium AND x2 IS medium THEN y IS low WITH 0.198',
      'IF x1 IS medium AND x2 IS medium THEN y IS medium WITH 0.496',
      'IF x1 IS medium AND x2 IS medium THEN y IS high WITH 0.381',
      'IF x1 IS medium AND x2 IS medium THEN y IS veryHigh WITH 0.941',
      'IF x1 IS medium AND x2 IS high THEN y IS veryLow WITH 0.609',
      'IF x1 IS medium AND x2 IS high THEN y IS low WITH 0.623',
      'IF x1 IS medium AND x2 IS high THEN y IS medium WITH 0.245',
      'IF x1 IS medium AND x2 IS high THEN y IS high WITH 0.551',
      'IF x1 IS medium AND x2 IS high THEN y IS veryHigh WITH 0.016',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS veryLow WITH 0.915',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS low WITH 0.041',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS medium WITH 0.236',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS high WITH 0.321',
      'IF x1 IS medium AND x2 IS veryHigh THEN y IS veryHigh WITH 0.357',
      'IF x1 IS high AND x2 IS veryLow THEN y IS veryLow WITH 0.473',
      'IF x1 IS high AND x2 IS veryLow THEN y IS low WITH 0.515',
      'IF x1 IS high AND x2 IS veryLow THEN y IS medium WITH 0.142',
      'IF x1 IS high AND x2 IS veryLow THEN y IS high WITH 0.400',
      'IF x1 IS high AND x2 IS veryLow THEN y IS veryHigh WITH 0.231',
      'IF x1 IS high AND x2 IS low THEN y IS veryLow WITH 0.303',
      'IF x1 IS high AND x2 IS low THEN y IS low WITH 0.682',
      'IF x1 IS high AND x2 IS low THEN y IS medium WITH 0.689',
      'IF x1 IS high AND x2 IS low THEN y IS high WITH 0.409',
      'IF x1 IS high AND x2 IS low THEN y IS veryHigh WITH 0.190',
      'IF x1 IS high AND x2 IS medium THEN y IS veryLow WITH 0.493',
      'IF x1 IS high AND x2 IS medium THEN y IS low WITH 0.793',
      'IF x1 IS high AND x2 IS medium THEN y IS medium WITH 0.632',
      'IF x1 IS high AND x2 IS medium THEN y IS high WITH 0.879',
      'IF x1 IS high AND x2 IS medium THEN y IS veryHigh WITH 0.524',
      'IF x1 IS high AND x2 IS high THEN y IS veryLow WITH 0.170',
      'IF x1 IS high AND x2 IS high THEN y IS low WITH 0.929',
      'IF x1 IS high AND x2 IS high THEN y IS medium WITH 0.004',
      'IF x1 IS high AND x2 IS high THEN y IS high WITH 0.080',
      'IF x1 IS high AND x2 IS high THEN y IS veryHigh WITH 0.010',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS veryLow WITH 0.823',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS low WITH 0.661',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS medium WITH 0.485',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS high WITH 0.231',
      'IF x1 IS high AND x2 IS veryHigh THEN y IS veryHigh WITH 0.152',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS veryLow WITH 0.267',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS low WITH 0.363',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS medium WITH 0.860',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS high WITH 0.452',
      'IF x1 IS veryHigh AND x2 IS veryLow THEN y IS veryHigh WITH 0.374',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS veryLow WITH 0.838',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS low WITH 0.116',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS medium WITH 0.905',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS high WITH 0.131',
      'IF x1 IS veryHigh AND x2 IS low THEN y IS veryHigh WITH 0.451',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS veryLow WITH 0.349',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS low WITH 0.740',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS medium WITH 0.571',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS high WITH 0.321',
      'IF x1 IS veryHigh AND x2 IS medium THEN y IS veryHigh WITH 0.351',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS veryLow WITH 0.582',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS low WITH 0.415',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS medium WITH 0.888',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS high WITH 0.609',
      'IF x1 IS veryHigh AND x2 IS high THEN y IS veryHigh WITH 0.111',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS veryLow WITH 0.844',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS low WITH 0.553',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS medium WITH 0.175',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS high WITH 0.773',
      'IF x1 IS veryHigh AND x2 IS veryHigh THEN y IS veryHigh WITH 0.481'
    ]
  }, {
    x1: 2.9077741317451,
    x2: 1.879171336069703
  }
  )).export(module)

/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const BOUNDARY = 100

vows
  .describe('Slope near zero for rules')
  .addBatch({
    'When we generate a slope manually and with the slopeAt() method': {
      topic () {
        const initial = {
          inputs: {
            x: {
              veryLow: [0, BOUNDARY / 4],
              low: [0, BOUNDARY / 4, BOUNDARY / 2],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          outputs: {
            y: {
              veryLow: [0, BOUNDARY / 4],
              low: [0, BOUNDARY / 4, BOUNDARY / 2],
              medium: [BOUNDARY / 4, BOUNDARY / 2, (3 * BOUNDARY) / 4],
              high: [BOUNDARY / 2, (3 * BOUNDARY) / 4, BOUNDARY],
              veryHigh: [(3 * BOUNDARY) / 4, BOUNDARY]
            }
          },
          rules: [
            'x INCREASES y WITH 0.5 0.5 0.5 0.5 0.5'
          ]
        }

        try {
          const FuzzyController = require('../lib/fuzzy-controller')

          const fc = new FuzzyController(initial)

          const slope = fc.slopeAt(28, 0.0, {x: BOUNDARY / 2})

          this.callback(null, slope)
        } catch (err) {
          this.callback(err)
        }

        return undefined
      },

      'it works' (err, slope) {
        assert.ifError(err)
      },

      'the slope looks roughly correct' (err, slope) {
        assert.ifError(err)
        assert.lesser(slope.y, 100.0)
      }
    }}).export(module)

// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

vows
  .describe('Invalid rules for FuzzyController')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with a rule referring to a non-existent input': {
        topic (FuzzyController) {
          try {
            // eslint-disable-next-line no-unused-vars
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow',
                'IF input1 IS low THEN output1 IS low',
                'IF input1 IS medium THEN output1 IS medium',
                'IF input1 IS high THEN output1 IS high',
                'IF input2 IS veryHigh THEN output1 IS veryHigh'
              ]})
            this.callback(new Error('Unexpected success'))
          } catch (err) {
            this.callback(null)
          }
          return undefined
        },
        'it fails correctly' (err) {
          assert.ifError(err)
        }
      },
      'and we instantiate with a rule referring to a non-existent output': {
        topic (FuzzyController) {
          try {
            // eslint-disable-next-line no-unused-vars
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow',
                'IF input1 IS low THEN output1 IS low',
                'IF input1 IS medium THEN output1 IS medium',
                'IF input1 IS high THEN output1 IS high',
                'IF input1 IS veryHigh THEN output2 IS veryHigh'
              ]})
            this.callback(new Error('Unexpected success'))
          } catch (err) {
            this.callback(null)
          }
          return undefined
        },
        'it fails correctly' (err) {
          assert.ifError(err)
        }
      },
      'and we instantiate with a rule referring to a non-existent input set': {
        topic (FuzzyController) {
          try {
            // eslint-disable-next-line no-unused-vars
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow',
                'IF input1 IS low THEN output1 IS low',
                'IF input1 IS medium THEN output1 IS medium',
                'IF input1 IS high THEN output1 IS high',
                'IF input1 IS veryVeryHigh THEN output1 IS veryHigh'
              ]})
            this.callback(new Error('Unexpected success'))
          } catch (err) {
            this.callback(null)
          }
          return undefined
        },
        'it fails correctly' (err) {
          assert.ifError(err)
        }
      },
      'and we instantiate with a rule referring to a non-existent output set': {
        topic (FuzzyController) {
          try {
            // eslint-disable-next-line no-unused-vars
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'IF input1 IS veryLow THEN output1 IS veryLow',
                'IF input1 IS low THEN output1 IS low',
                'IF input1 IS medium THEN output1 IS medium',
                'IF input1 IS high THEN output1 IS high',
                'IF input1 IS veryHigh THEN output1 IS veryVeryHigh'
              ]})
            this.callback(new Error('Unexpected success'))
          } catch (err) {
            this.callback(null)
          }
          return undefined
        },
        'it fails correctly' (err) {
          assert.ifError(err)
        }
      }
    }}).export(module)

// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
//
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const _ = require('lodash')

vows
  .describe('FuzzyController::addRule')
  .addBatch({
    'When we import the module': {
      topic () {
        try {
          const FuzzyController = require('../lib/fuzzy-controller')
          this.callback(null, FuzzyController)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, FuzzyController) {
        assert.ifError(err)
        assert.isFunction(FuzzyController)
      },
      'and we instantiate with arguments': {
        topic (FuzzyController) {
          try {
            const fc = new FuzzyController({
              inputs: {
                input1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                },
                input2: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              outputs: {
                output1: {
                  veryLow: [0, 20],
                  low: [5, 25, 45],
                  medium: [30, 50, 70],
                  high: [55, 75, 95],
                  veryHigh: [80, 100]
                }
              },
              rules: [
                'input1 INCREASES output1'
              ]})
            this.callback(null, fc)
          } catch (err) {
            this.callback(err, null)
          }
          return undefined
        },
        'it works' (err, fc) {
          assert.ifError(err)
          assert.isObject(fc)
        },
        'and we evaluate over the ranges': {
          topic (fc) {
            const { callback } = this
            const resultses = []
            try {
              _.each(__range__(-10, 110, true), (num) => {
                const results = fc.evaluate({input1: 50, input2: num})
                return resultses.push(results)
              })
              callback(null, resultses)
            } catch (err) {
              callback(err, null)
            }
            return undefined
          },
          'it works' (err, resultses) {
            assert.ifError(err)
          },
          'all values are the same' (err, resultses) {
            assert.ifError(err)
            return Array.from(resultses).map((results) =>
              assert.equal(results.output1, resultses[0].output1))
          },
          'and we add a rule': {
            topic (resultses, fc) {
              try {
                fc.addRule('input2 INCREASES output1')
                this.callback(null)
              } catch (err) {
                this.callback(err)
              }
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            },
            'and we get JSON': {
              topic (resultses, fc) {
                try {
                  const json = fc.toJSON()
                  this.callback(null, json)
                } catch (err) {
                  this.callback(err)
                }
                return undefined
              },
              'it works' (err, json) {
                assert.ifError(err)
              },
              'it includes the new rule' (err, json) {
                assert.ifError(err)
                assert.notEqual(json.rules.indexOf('input2 INCREASES output1'), -1)
                const pm = _.filter(json.parsed_rules, pr => (pr.type === 'increases') && (pr.input === 'input2') && (pr.output === 'output1'))
                assert.isArray(pm)
                assert.lengthOf(pm, 1)
              },
              'it includes the old rule' (err, json) {
                assert.ifError(err)
                assert.notEqual(json.rules.indexOf('input1 INCREASES output1'), -1)
                const pm = _.filter(json.parsed_rules, pr => (pr.type === 'increases') && (pr.input === 'input1') && (pr.output === 'output1'))
                assert.isArray(pm)
                assert.lengthOf(pm, 1)
              }
            },
            'and we evaluate over the range': {
              topic (resultses, fc) {
                const { callback } = this
                resultses = []
                try {
                  _.each(__range__(-10, 110, true), (num) => {
                    const results = fc.evaluate({input1: 50, input2: num})
                    return resultses.push(results)
                  })
                  callback(null, resultses)
                } catch (err) {
                  callback(err, null)
                }
                return undefined
              },
              'it works' (err, resultses) {
                assert.ifError(err)
              },
              'some values are different' (err, resultses) {
                assert.ifError(err)
                assert.ok(_.some(resultses.slice(1), results => results.output1 !== resultses[0].output1)
                )
              }
            }
          }
        }
      }
    }}).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}
